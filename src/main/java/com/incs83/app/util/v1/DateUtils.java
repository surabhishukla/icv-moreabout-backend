/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is the utility class for Date.
 * <br><br>{@link #getCurrentDate()} this method is used for getting current date.
 * <br><br>{@link #isStaleDate(Long)} this method is used for knowing that whether date2 is greater than or not.
 * <br><br>{@link #getCurrentTimeStampAsString()} It is used to get current timestamp.
 * <br><br>{@link #parseDatetoYYYYMMDD(Date)} It is used to parse the date in yyyy-MM-dd pattern.
 * <br><br>{@link #parseDatetoMMDDYYYY(Date)} It is used to parse the date in MM-dd-yyyy pattern.
 */
public final class DateUtils {

    /**
     * This method is used for getting current date.
     * @return current date of Date type
     */
    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * This method is used for knowing that whether date2 is greater than or not.
     * @param date2 it is of Long type
     * @return true or false
     */
    public static boolean isStaleDate(Long date2) {
        return new Date().getTime() > date2;
    }

    /**
     * This method is used to get current timestamp.
     * @return current timestamp of String type
     */
    public static String getCurrentTimeStampAsString() {
        return (String.valueOf(new Date().getTime()));
    }

    /**
     * It is used to parse the date in yyyy-MM-dd pattern.
     * @param date it is the object of Date
     * @return date in parsed format which of String type
     */
    public static String parseDatetoYYYYMMDD(Date date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").format(date).toString();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * It is used to parse the date in MM-dd-yyyy pattern.
     * @param date it is the object of Date
     * @return date in parsed format which of String type
     */
    public static String parseDatetoMMDDYYYY(Date date) {
        try {
            return new SimpleDateFormat("MM-dd-yyyy").format(date).toString();
        } catch (Exception e) {
            return null;
        }
    }

}
