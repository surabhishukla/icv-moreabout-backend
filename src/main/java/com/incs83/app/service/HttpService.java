/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.incs83.app.constants.CommonConstants;
import com.incs83.app.handler.exceptions.ApiException;
import com.incs83.app.handler.response.ApiResponseCode;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import static com.incs83.app.constants.CommonConstants.EMPTY_STRING;

/**
 * This class helps in performing operations like post, delete, get etc... on the server.
 * <br><br>{@link #doGetByte(String, HashMap, HashMap)} this method is used get the information from server in the form of array of bytes.
 * <br><br>{@link #doGet(String, HashMap, HashMap)} this method  is used to query or to get some information from the server.
 * <br><br>{@link #doPost(StringEntity, String, HashMap)} this method is used to sending information to the server.
 * <br><br>{@link #doDelete(String, HashMap)} This method allows the client to remove anything from the server.
 */
@Service
public class HttpService {
    private static final Logger LOG = LogManager.getLogger();

    /**
     * This method is used get the information from server in the form of array of bytes.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc....
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     * @return resp of type {byte[]}.
     */
    public byte[] doGetByte(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        byte[] resp;
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams)) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }
            HttpGet httpGet = new HttpGet(url);
            headerInfo.forEach(httpGet::addHeader);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            resp = EntityUtils.toByteArray(httpResponse.getEntity());
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            } else {
                LOG.error("Error ::: Response ");
                throw new ApiException(ApiResponseCode.ERROR);
            }
        } catch (Exception e) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error Closing HTTP HttConnection...");
            }
        }
        return resp;
    }

    /**
     * This method  is used to query or to get some information from the server.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc...
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doGet(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String resp;
        try {
            StringBuffer params = new StringBuffer(CommonConstants.EMPTY_STRING);
            if (Objects.nonNull(queryParams)) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }
            HttpGet httpGet = new HttpGet(url);
            headerInfo.forEach(httpGet::addHeader);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            resp = EntityUtils.toString(httpResponse.getEntity());
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            } else {
                LOG.error("Error ::: Response ");
                throw new ApiException(ApiResponseCode.ERROR);
            }
        } catch (Exception e) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error Closing HTTP HttConnection...", e);
            }
        }
        return resp;
    }

    /**
     * This method is used to sending information to the server.
     * @param params it is of StringEntity type.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc...
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPost(StringEntity params, String url, HashMap<String, String> headerInfo) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String resp = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            headerInfo.forEach(httpPost::addHeader);
            httpPost.setEntity(params);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            if (!(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK || httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED || httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT)) {
                LOG.error("Error ::: Response , httpResponse " + httpResponse);
                throw new ApiException(ApiResponseCode.ERROR);
            }

            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_NO_CONTENT)
                resp = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error Closing HTTP HttConnection...");
                throw new ApiException(ApiResponseCode.ERROR);
            }
        }
        return resp;
    }

    /**
     * This method allows the client to remove anything from the server.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc...
     * @throws Exception this method might throw exception
     * @return response of type {boolean}.
     */
    public boolean doDelete(String url, HashMap<String, String> headerInfo) throws Exception {
        boolean response;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpResponse httpResponse;
        HttpDelete httpDelete = new HttpDelete(url);
        headerInfo.forEach(httpDelete::addHeader);
        httpResponse = httpClient.execute(httpDelete);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT || httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            response = true;
        } else {
            LOG.error("Error Invoking Delete Call");
            throw new ApiException(ApiResponseCode.BAD_REQUEST);
        }
        return response;
    }
}
