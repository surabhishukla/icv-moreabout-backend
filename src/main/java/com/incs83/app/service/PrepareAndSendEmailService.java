/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.service;

import com.incs83.app.common.EmailWithAttachmentRequest;
import com.incs83.app.common.MobileMessageRequest;
import com.incs83.app.common.PrepareEmailRequest;
import com.incs83.app.config.ApplicationConsulService;
import com.incs83.app.constants.HttpStatus;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.mt.DataAccessService;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.request.EmailSendRequest;
import com.incs83.app.util.v1.CommonUtils;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.incs83.app.constants.ApplicationConstants.*;
import static com.incs83.app.constants.CommonConstants.*;

/**
 * This class is used to prepare the email and once the email got prepared, sending the email.
 * <br><br>{@link #prepareAndSendEmail(PrepareEmailRequest)} This method is used to prepare data for email sending by validating the input given by user
 * <br><br>{@link #prepareAndSendEmail(EmailWithAttachmentRequest)} This method is used to prepare and send the email once got prepared.
 * <br><br>{@link #replaceVariables(String text, String name)} this method  is used to replace the pre existing fields with provided value.
 */
@Service
public class PrepareAndSendEmailService {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private ApplicationConsulService applicationConsulService;
    @Autowired
    private AmazonSesService amazonSesService;
    @Autowired
    private DataAccessService dataAccessService;
    @Autowired
    private AmazonSNSPublisherService amazonSNSPublisherService;

    /**
     * This method is used to prepare data for email sending by validating the input given by user.
     * @param prepareEmailRequest(templateName,emailData,addressTo,replacementData,userMobile)
     *                            templateName it is the name of the template which is going to be used to send the email.
     *                            emailData it is the actual message written in the email.
     *                            addressTo it is the address of the receiver.
     *                            replacementData it is the replacement data.
     *                            userMobile it contains the mobile number of the sender.
     * @throws Exception this method might throw exception
     */

    public void prepareAndSendEmail(PrepareEmailRequest prepareEmailRequest) throws Exception {
        try {
            List<EmailSendRequest.DestinationModel> destinationModelList = new ArrayList();
            prepareEmailRequest.getEmailData().forEach(emailData -> {
                if (ValidationUtils.nonNullOrEmpty(emailData.getReplacementData()) && ValidationUtils.nonNullOrEmpty(emailData.getAddressTo())) {
                    EmailSendRequest.DestinationModel destinationModel = new EmailSendRequest.DestinationModel();
                    destinationModel.setToAddresses(Arrays.asList(emailData.getAddressTo()));
                    destinationModel.setReplacementTemplateData(com.mongodb.util.JSON.serialize(emailData.getReplacementData()));
                    destinationModelList.add(destinationModel);
                    try {
                        if (ValidationUtils.nonNullOrEmpty(emailData.getUserMobile())) {
                            if (CommonUtils.isIndianMobile(emailData.getUserMobile()) && applicationConsulService.getEnableSmsForIndia() ||
                                    (!CommonUtils.isIndianMobile(emailData.getUserMobile()))) {
                                String findTextMessage = TEMPLATE_NAME_BASED_MESSAGE.get(prepareEmailRequest.getTemplateName());
                                MobileMessageRequest request = new MobileMessageRequest();
                                request.setMobileNumber(emailData.getUserMobile());
                                if (ValidationUtils.nonNullOrEmpty(findTextMessage)) {
                                    if (findTextMessage.contains("##")) {
                                        for (String variable : TEMPLATE_VARIABLES.get(prepareEmailRequest.getTemplateName())) {
                                            findTextMessage = findTextMessage.replaceAll(variable, emailData.getReplacementData().get(variable.substring(2)));
                                        }
                                    }
                                    request.setMessage(findTextMessage);
                                    amazonSNSPublisherService.sendMessage(request);
                                }
                            }
                        }
                    } catch (Exception e) {
                        LOG.error(LOGGER_PREFIX + "ERROR OCCURRED WHILE PREPARING SMS" + LOGGER_SUFFIX, e);
                    }

                } else {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Please input data and email with their respective values");
                }
            });
            EmailSendRequest emailSendRequest = new EmailSendRequest();
            emailSendRequest.setSource(applicationConsulService.getFromEmail());
            emailSendRequest.setTemplateName(prepareEmailRequest.getTemplateName());
            emailSendRequest.setDefaultTemplateData(DEFAULT_TEMPLATE);
            emailSendRequest.setDestinations(destinationModelList);
            amazonSesService.sendTemplatedEmail(emailSendRequest);
        } catch (Exception e) {
            LOG.error(LOGGER_PREFIX + "ERROR OCCURRED WHILE SENDING EMAIL" + LOGGER_SUFFIX);
            throw e;
        }
    }

    HashMap<String, String> TEMPLATE_NAME_BASED_MESSAGE = new HashMap() {{
        put("RegistrationForTrialSignUp", REGISTRATION_TRIAL_SIGNUP_MESSAGE);
        put("RegistrationForPaidSignUp", REGISTRAION_PAID_SIGNUP_MESSAGE);
        put("TrialExpired", TRIAL_EXPIRED_MESSAGE);
        put("AccountClosureReminder", ACCOUNT_CLOSURE_REMINDER_MESSAGE);
        put("AccountClosure", ACCOUNT_CLOSURE_MESSAGE);
        put("ForgotPasswordMail", FORGOT_PASSWORD_MESSAGE);
        put("ChangePasswordNotification", CHANGE_PASSWORD_MESSAGE);
        put("PaymentSuccessful", PAYMENT_SUCCESSFUL_MESSAGE);
        put("UpgradeSuccessful", UPGRADE_SUCCESSFUL_MESSAGE);
        put("PaymentFailed", PAYMENT_FAILED_MESSAGE);
        put("CancelSubscriptionAndCloseAccount", CANCEL_SUBSCRIPTION_MESSAGE);
        put("RefundInitiated", REFUND_INITIATED_MESSAGE);
        put("PolicyEmail", POLICY_EMAIL_MESSAGE);
    }};

    HashMap<String, List<String>> TEMPLATE_VARIABLES = new HashMap() {{
        put("RegistrationForPaidSignUp", Arrays.asList("##PRODUCT_NAME", "##PLAN_NAME", "##VALIDITY", "##AMOUNT"));
        put("PaymentSuccessful", Arrays.asList("##AMOUNT", "##PRODUCT_NAME", "##PLAN", "##TRANSACTION_NUMBER"));
        put("UpgradeSuccessful", Arrays.asList("##PRODUCT_NAME", "##PLAN_NAME", "##AMOUNT"));
        put("PaymentFailed", Arrays.asList("##AMOUNT", "##PRODUCT_NAME", "##PLAN"));
        put("RefundInitiated", Arrays.asList("##TRANSACTION_REF_NO"));
        put("PolicyEmail", Arrays.asList("##CONNECTOR_NAME"));
    }};

    /**
     * This method is used to prepare and send the email once got prepared.
     * @param request(filePath,recipientEmail,connectorName,html_part,text_part,subject)
     *                            filePath it is the path of the file.
     *                            recipientEmail it is the email of receiver.
     *                            connectorName it is the name of the connector.
     *                            html_part it consist of html.
     *                            text_part it is the place where actual message will be written.
     *                            subject it is the subject of the email.
     */
    public void prepareAndSendEmail(EmailWithAttachmentRequest request) {
        String query = "select html_part,text_part,subject from email_template where template_name =:name";
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", MQTT_TEMPLATE);
        try {
            SqlCurrentTenantIdentifier._tenantIdentifierSql.set(M83_MASTER_DB);
            List<Object[]> emailTemplate = dataAccessService.readNative(query, params);
            if (ValidationUtils.nonNullOrEmpty(emailTemplate)) {
                String htmlPart = (String) emailTemplate.get(0)[0];
                String textPart = (String) emailTemplate.get(0)[1];
                String subject = (String) emailTemplate.get(0)[2];
                htmlPart = replaceVariables(htmlPart, request.getConnectorName());
                textPart = replaceVariables(textPart, request.getConnectorName());
                request.setHtml_part(htmlPart);
                request.setText_part(textPart);
                request.setSubject(subject);
                amazonSesService.sendWithAttachment(request);
            } else {
                request.setSubject("Data");
                request.setText_part("iot83");
                request.setHtml_part(DEFAULT_TEMPLATE);
                amazonSesService.sendWithAttachment(request);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * This method is used to replace the pre existing fields with provided value.
     * @param text it is the format of text.
     * @param name it is the name of connector
     * @return the variables with updated value.
     */
    private String replaceVariables(String text, String name) {

        return text
                .replace("{FIRST_NAME}", ExecutionContext.get().getUsercontext().getName())
                .replace("{CONNECTOR_NAME}", name)
                .replace("{DOMAIN_NAME}", MQTT_URL.replace("__baseUrl__", applicationConsulService.getBaseUrl().split("\\.")[1]));
    }
}
