/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.tokenFactory;

import java.util.List;

public class UserContext {
    private final String username;
    private final String id;
    private final String roleId;
    private final String issuer;
    private final String name;
    private final Boolean isVerified;
    private final String roleName;
    private final List<Authority> authorities;


    public UserContext(String id, String username, String tenantId, String issuer, String roleId, List<String> groupIds, String name, boolean isVerified, boolean isLicenseExpired, Boolean internalUser, String roleName, List<Authority> authorities, List<String> projectIds, String currentProjectId, Boolean defaultAccountAdmin, String tenantType) {
        this.username = username;
        this.id = id;
        this.roleId = roleId;
        this.issuer = issuer;
        this.name = name;
        this.isVerified = isVerified;
        this.roleName = roleName;
        this.authorities = authorities;
    }


    public static UserContext create(String id, String username, String tenantId, String issuer, String roleId, List<String> groupIds, String name, Boolean isVerified, Boolean isLicenseExpired, Boolean internalUser, String roleName, List<Authority> authorities, List<String> projectIds, String currentProjectId, Boolean defaultAccountAdmin, String tenantType) {
        if (username.isEmpty()) throw new IllegalArgumentException("Username is blank: " + username);
        return new UserContext(id, username, tenantId, issuer, roleId, groupIds, name, isVerified, isLicenseExpired, internalUser, roleName, authorities, projectIds, currentProjectId, defaultAccountAdmin, tenantType);
    }

    public String getUsername() {
        return username;
    }

    public String getId() {
        return id;
    }

    public String getRoleId() {
        return roleId;
    }


    public String getIssuer() {
        return issuer;
    }

    public String getName() {
        return name;
    }

    public Boolean getVerified() {
        return isVerified;
    }

    public String getRoleName() {
        return roleName;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }


    @Override
    public String toString() {
        return "UserContext{" +
                "username='" + username + '\'' +
                ", id='" + id + '\'' +
                ", roleId='" + roleId + '\'' +
                ", issuer='" + issuer + '\'' +
                ", name='" + name + '\'' +
                ", isVerified=" + isVerified +
                '}';
    }
}
