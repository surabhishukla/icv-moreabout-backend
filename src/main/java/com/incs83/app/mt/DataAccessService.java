/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.mt;

import com.incs83.app.dao.Page;
import com.incs83.app.dao.Repository;
import com.incs83.app.request.PaginatedRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * This class is designed to provide methods for mysql that were used throughout the project.
 * <br><br>{@link #read(Class type)} This method is used to fetching of data from database.
 * <br><br>{@link #read(Class type, PaginatedRequest paginatedRequest)} This method is used to fetching of data from database.
 * <br><br>{@link #read(Class type, String resourceId)} This method is used to fetching of data from database.
 * <br><br>{@link #read(Class, String, HashMap)} This method is used to fetching of data from database.
 * <br><br>{@link #read(Class, String, HashMap, PaginatedRequest)} This method is used to fetching of data from database.
 * <br><br>{@link #findById(Class type, String resourceId)} This method is used to fetching of data from database..
 * <br><br>{@link #deleteById(Class type, String resourceId)} This method is used to delete data from database.
 * <br><br>{@link #delete(String, HashMap)} This method is used to delete data from database.
 * <br><br>{@link #deleteUpdateNative(String, HashMap)} This method is used to delete data from database.
 * <br><br>{@link #create(Class, List)} This method is used to create or save data in database.
 * <br><br>{@link #update(Class, String, HashMap)} This method is used to update the data in database.
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class DataAccessService<T> {

    @Autowired
    private Repository repository;

    private static final Logger LOG = LogManager.getLogger();


    /**
     * This method is used to fetching of data from database.
     *
     * @param type it's a class on the basis of which the data is fetched from database.
     * @return result of type list.
     * @throws Exception this method might throw exception
     */
    public Iterable<T> read(Class type) throws Exception {
        Iterable<T> result = repository.read(type);
        return Objects.isNull(result) ? new ArrayList<>() : result;
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param type             it's a class on the basis of which the data is fetched from database.
     * @param paginatedRequest (offset,max,order,sortBy,tableName)
     *                         offset it consist of 0 as a value.
     *                         max it consist of 10 as a value.
     *                         order it is of order type whose value can be one of these ASC and DSC.
     *                         sortBy it provides condition on which sorting is done.
     *                         tableName it is used for table name.
     * @return result of type list.
     * @throws Exception this method might throw exception
     */
    public Iterable<T> read(Class type, PaginatedRequest paginatedRequest) throws Exception {
        Iterable<T> result =  repository.read(type, paginatedRequest);
        return Objects.isNull(result) ? new ArrayList<>() : result;
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param type       it's a class on the basis of which the data is fetched from database.
     * @param resourceId it's a id of resource.
     * @return result of class type.
     * @throws Exception this method might throw exception
     */
    public T read(Class type, String resourceId) throws Exception {
        T result =  (T) repository.read(type, resourceId);
        return Objects.isNull(result) ? null : result;
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param type   it's a class on the basis of which the data is fetched from database.
     * @param query  it's a hql query required for fetching the data from database.
     * @param params these are parameters required in query.
     * @return result of type list.
     * @throws Exception this method might throw exception
     */
    public Iterable<T> read(Class type, String query, HashMap<String, Object> params) throws Exception {
        Iterable<T> result =  repository.read(query, params);
        return Objects.isNull(result) ? new ArrayList<>() : result;
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param type             it's a class on the basis of which the data is fetched from database.
     * @param query            it's a hql query required for fetching the data from database.
     * @param params           these are parameters required in query.
     * @param paginatedRequest (offset,max,order,sortBy,tableName)
     *                         offset it consist of 0 as a value.
     *                         max it consist of 10 as a value.
     *                         order it is of order type whose value can be one of these ASC and DSC.
     *                         sortBy it provides condition on which sorting is done.
     *                         tableName it is used for table name.
     * @return result of class type.
     * @throws Exception this method might throw exception
     */
    public Page<T> read(Class type, String query, HashMap<String, Object> params, PaginatedRequest paginatedRequest) throws Exception {
        Page<T> result = (Page<T>) repository.read(query, params, paginatedRequest);
        return Objects.isNull(result) ? null : result;
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param type             it's a class on the basis of which the data is fetched from database.
     * @param queries          these are hql queries required for fetching the data from database.
     * @param params           these are parameters required in query.
     * @param paginatedRequest (offset,max,order,sortBy,tableName)
     *                         offset it consist of 0 as a value.
     *                         max it consist of 10 as a value.
     *                         order it is of order type whose value can be one of these ASC and DSC.
     *                         sortBy it provides condition on which sorting is done.
     *                         tableName it is used for table name.
     * @return result of class type.
     * @throws Exception this method might throw exception
     */
    public Page<T> read(Class type, HashMap<String, Object> params, PaginatedRequest paginatedRequest, String... queries) throws Exception {
        Object result = repository.read(params, paginatedRequest, queries);
        return Objects.isNull(result) ? null : (Page<T>) result;
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param type       it's a class on the basis of which the data is fetched from database.
     * @param resourceId it's a id of resource.
     * @return result of class type.
     * @throws Exception this method might throw exception
     */
    public T findById(Class type, String resourceId) throws Exception {
        T result = (T) repository.read(type, resourceId);
        return result;
    }

    /**
     * This method is used to delete data from database.
     *
     * @param type       it's a class on the basis of which the data is fetched from database which is going to be delete.
     * @param resourceId it's a id of resource.
     * @throws Exception this method might throw exception
     */
    public void deleteById(Class type, String resourceId) throws Exception {
        repository.delete(type, resourceId);
    }

    /**
     * This method is used to delete data from database.
     *
     * @param query  it's a hql query required for fetching the data from database which is going to be delete.
     * @param params these are parameters required in query.
     * @throws Exception this method might throw exception
     */
    public void delete(String query, HashMap<String, Object> params) throws Exception {
        repository.delete(query, params);
    }

    /**
     * This method is used to delete data from database.
     *
     * @param query  it's a sql query required for fetching the data from database which is going to be delete.
     * @param params these are parameters required in query.
     * @throws Exception this method might throw exception
     */
    public void deleteUpdateNative(String query, HashMap<String, Object> params) throws Exception {
        repository.deleteUpdateNative(query, params);
    }

    /**
     * This method is used to fetching of data from database.
     *
     * @param query  it's a sql query required for fetching the data from database.
     * @param params these are parameters required in query.
     * @return it returns list of data of type {List} on the basis of the query.
     * @throws Exception this method might throw exception
     */
    public List<?> readNative(String query, HashMap<String, Object> params) throws Exception {
        return repository.readNative(query, params);
    }

    /**
     * This method is used to create or save data in database.
     *
     * @param type     it's of class type
     * @param resource it is T type  object which consist of some data to be saved in database.
     * @return true .
     * @throws Exception this method might throw exception
     */
    public boolean create(@NotNull Class type, @NotNull T resource) throws Exception {
        return Objects.nonNull(repository.create(resource));
    }

    /**
     * This method is used to create or save data in database.
     *
     * @param type      it's of class type
     * @param resources it is List type objects which consist of some data to be saved in database..
     * @return true .
     * @throws Exception this method might throw exception
     */
    public boolean create(@NotNull Class type, @NotNull List<T> resources) throws Exception {
        return Objects.nonNull(repository.create(resources));
    }

    /**
     * This method is used to update the data in database.
     *
     * @param type     it's of class type
     * @param resource it is T type  object which consist of some data to be updated in database.
     * @return result of T type.
     * @throws Exception this method might throw exception
     */
    public T update(Class type, T resource) throws Exception {
        return (T) repository.update(resource);
    }

    /**
     * This method is used to update the data in database.
     *
     * @param type   it's of class type
     * @param query  it's a hql query required for updating the data in database.
     * @param params these are parameters required in query.
     * @throws Exception this method might throw exception
     */
    public void update(Class type, String query, HashMap<String, Object> params) throws Exception {
        repository.update(query, params);
    }

    /**
     * This method is used to update the data in database.
     *
     * @param type     it's of class type
     * @param resource it is T type  object which consist of some data to be updated in database.
     * @throws Exception this method might throw exception
     */
    public void merge(Class type, T resource) throws Exception {
        repository.merge(resource);
    }
}
