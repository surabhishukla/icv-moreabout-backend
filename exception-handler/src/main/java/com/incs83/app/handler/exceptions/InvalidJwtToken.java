/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.handler.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * This class is used to throw exception when the token of the signed-in authority has expired
 *  or when the authentication ofr the user is failed.
 */
public final class InvalidJwtToken extends AuthenticationException {
    private static final long serialVersionUID = -294671188037098603L;

    public InvalidJwtToken(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidJwtToken(String msg) {
        super(msg);
    }
}
