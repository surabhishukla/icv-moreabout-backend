/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.constants;

public interface ApplicationConstants {
    //    List<MqttClient> mqttConnectionList = Collections.synchronizedList(new ArrayList());
    String S3_CDN_PATH = "cdn/assests/";
    String S3_DAP_PATH = "dap/raw/";
    String SLASH = "/";
    String UNDERSCORE = "_";
    String EMPTY_STRING = "";
    String _ID = "_id";
    String SYSTEM_ADMIN = "4bd8799aeb5446af871fac0a11523aa4";
    String SYSTEM_GENERATED = "System Generated";
    String NOT_BLANK = " can not be blank";
    String ACCOUNT_ADMIN_USERID = "a7565419736f465eb8_account_admin";
    String API_GATEWAY_CALL = "Faas APIs";
    String AUDIT_LOGS = "auditLogs";
    String FORWARD_SLASH = "/";
    String USER_LOGIN = "Login";
    String HTTP_API_PREFIX = "api/v1/connector/";
    String HTTP = "HTTP";
    String SYSTEM_DATA_SOURCE = "SYSTEM_dataSource";
    String HTTP_PREFIX = "HTTP_";
    String API_KEY = "x-api-key";
    String DEFAULT_TEMPLATE = "{ \"first_name\":\" name \"}";
    String LOGGER_PREFIX = "############  ";
    String LOGGER_SUFFIX = "  ############";
    String S3_DOWNLOAD_FILE_PATH ="/home/ubuntu/file";

    String FORGOT_PASSWORD_MESSAGE = " Hi\n" + "As per your request, the reset password link (valid for 15 mins) has been sent on your registered email address. \n" + "We recommend you to choose a strong password.\n" + "\n" + "IoT83";
    String CHANGE_PASSWORD_MESSAGE = "Hi\n" + "You have successfully, changed your password. Please do not share your password with any one.\n" + "\n" + "IoT83";
    String REGISTRATION_TRIAL_SIGNUP_MESSAGE = "Hey,\n" + "Welcome to your free trial of the Method83 Platform. \n" + "Explore our intuitive tool sets and visual studios.\n" + "Trial is valid for 5 days.\n" + "\n" + "IoT83";
    String REGISTRAION_PAID_SIGNUP_MESSAGE = "Thank you for signing up !\n " + "Subscription and plan details.\n" + "Product Name ##PRODUCT_NAME\n" + "Plan name: ##PLAN_NAME\n" + "Billing Cycle ##VALIDITY days\n" + "Amount $ ##AMOUNT\n" + "\n" + "IOT83";
    String TRIAL_EXPIRED_MESSAGE = "Hi\n" + "\n" + "Trial Expired !\n" + "\n" + "If you wish to continue using Method83, please visit the “My Subscriptions Page” on the portal and choose a plan that fits your need.\n" + "\n" + "IoT83";
    String ACCOUNT_CLOSURE_REMINDER_MESSAGE = "Hi\n" + "\n" + "Your trial has already expired.\n" + "If you do not want to continue further, we thank you for your interest in Method83. We will close your account after 2 days.\n" + "\n" + "IoT83";
    String ACCOUNT_CLOSURE_MESSAGE = "Hi\n" + "We have closed your account. In case you wish to come back anytime later, we would be happy to have you back.\n" + "\n" + " IoT83";
    String PAYMENT_SUCCESSFUL_MESSAGE = "Hi\n" + "\n" + "We have successfully received your payment of $ ##AMOUNT towards ##PRODUCT_NAME for ##PLAN category\n" + "Your transaction reference number is ##TRANSACTION_NUMBER\n" + "\n" + "IoT83";
    String UPGRADE_SUCCESSFUL_MESSAGE = "Hi\n" + "We have successfully upgraded your package.\n" + "Upgraded Product : ##PRODUCT_NAME\n" + "Upgraded Plan : ##PLAN_NAME\n" + "You will be charged an amount of ##AMOUNT from next billing cycle.\n" + "\n" + "IoT83";
    String PAYMENT_FAILED_MESSAGE = "Hi\n" + "\n" + "Your payment of $ ##AMOUNT towards ##PRODUCT_NAME for ##PLAN category has FAILED!\n" + "\n" + "IoT83";
    String CANCEL_SUBSCRIPTION_MESSAGE = "Hi\n" + "We are sorry that you have decided to close your account on Method83. It was our pleasure to have you on board.\n" + "\n" + "IoT83";
    String REFUND_INITIATED_MESSAGE = "Hi\n" + " Due to a system error you were double charged for your last payment, a refund process has been initiated and soon will be processed.\n" + "Your transaction reference number is ##TRANSACTION_REF_NO\n" + "\n" + "IoT83";
    String POLICY_EMAIL_MESSAGE = "Hi \n" + "As per your created rules, on the platform for your Device ##CONNECTOR_NAME, there has been observed some alarms / anomalies that need attention.\n" + "Please check your email for more info\n" + "Please take necessary action.\n" + "\n" + "IoT83";

}
