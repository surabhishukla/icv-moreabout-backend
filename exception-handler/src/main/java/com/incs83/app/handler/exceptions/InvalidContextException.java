/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.handler.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * This class is used to throw exception when the tenant name in url is invalid.
 * <br><br>{@link #InvalidContextException(String, Throwable)} It is the constructor of InvalidContextException class
 * <br><br>{@link #InvalidContextException(String)} It is the constructor of InvalidContextException class
 */
public final class InvalidContextException extends AuthenticationException {

    private static final long serialVersionUID = -2438650860206589111L;

    /**
     * It is the constructor of InvalidContextException class
     * @param msg it is the exception method
     * @param t it is the exception
     */
    public InvalidContextException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * It is the constructor of InvalidContextException class
     * @param msg it is the exception method
     */
    public InvalidContextException(String msg) {
        super(msg);
    }
}
