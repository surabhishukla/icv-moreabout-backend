/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.service;

import org.apache.http.client.methods.HttpPost;
import org.springframework.stereotype.Service;

/**
 * This class is used when we need to pass the payload while performing delete operation.
 * <br><br>{@link #getMethod()} This method is used to get the method.
 */
@Service
public class HttpDeleteWithBody extends HttpPost {
    public HttpDeleteWithBody(){
        super();
    }

    public HttpDeleteWithBody(String uri) {
        super(uri);
    }

    /**
     * This method is used to get the method.
     * @return the method {String}.
     */
    @Override
    public String getMethod() {
        return "DELETE";
    }
}
