/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.dao;

import com.incs83.app.constants.MySQLConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Service;

import java.util.Properties;

import static com.incs83.app.constants.MySQLConstants.*;

/**
 * This class is used for inserting the various required fields to establish the connection with database using hibernate and also to close the hibernate session.
 * <br><br>{@link #closeSilently(Session)} This method is used for closing the hibernate session.
 * <br><br>{@link #sessionFactory()} This method is used for inserting various required fields needed for connecting with database.
 */
@Service
public final class HibernateService {

    public static final Properties multiTenantProperties = new Properties();
    private static final Logger LOG = LogManager.getLogger("com");
    @Autowired
    private MySqlConsulService mySqldevConsulService;

    /**
     * This method is used for closing the hibernate session
     * @param session it represents the database connection so that we can perform crud operation in database.
     *                Once the session terminated we will no longer have connection with database.
     */
    public static void closeSilently(Session session) {
        try {
            if (session != null)
                session.close();
            session = null;
        } catch (HibernateException e) {
            LOG.error("#### Error while closing hibernate session #####");
        }
    }

    /**
     * This method is used for inserting various fields needed for creating the connection of hibernate with database.
     * @return sessionFactory of type {LocalSessionFactoryBean} once all fields got set up.
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        Properties properties = new Properties();
        properties.put(MySQLConstants.HIBERNATE_DIALECT, mySqldevConsulService.getHibernateDialect());
        properties.put(MySQLConstants.HIBERNATE_SHOW_SQL, mySqldevConsulService.getHibernateShowSql());
        properties.put(MySQLConstants.HIBERNATE_FORMAT_SQL, mySqldevConsulService.getHibernateFormatSql());
        if (Boolean.parseBoolean(mySqldevConsulService.getHibernateGenerateMLTables())) {
            properties.put(MySQLConstants.HIBERNATE_DDL_MODE, mySqldevConsulService.getHibernateHbm2ddlAuto());
        }

        properties.put(MySQLConstants.HIBERNATE_MT_CONNECTION_PROVIDER, mySqldevConsulService.getHibernateMtConnectionProvider());
        properties.put(HIBERNATE_MT_TENANT_IDENTIFIER, mySqldevConsulService.getHibernateMtTenantIdentifier());
        properties.put(MySQLConstants.HIBERNATE_MT, mySqldevConsulService.getHibernateMultiTenancy());

        properties.put(HIBERNATE_CP_PROVIDER_CLASS, mySqldevConsulService.getHibernateConnectionPoolClass());

        properties.put(MySQLConstants.HIBERNATE_CP_MIN_SIZE, mySqldevConsulService.getHibernatePoolMinConnections());
        properties.put(MySQLConstants.HIBERNATE_CP_MAX_SIZE, mySqldevConsulService.getHibernatePoolMaxConnections());
        properties.put(MySQLConstants.HIBERNATE_CP_TIMEOUT, mySqldevConsulService.getHibernatePoolTimeout());
        properties.put(MySQLConstants.HIBERNATE_CP_MAX_STATEMENTS, mySqldevConsulService.getHibernatePoolMaxStatements());
        properties.put(MySQLConstants.HIBERNATE_ALLOW_MERGE_ENTITY, mySqldevConsulService.getHibernateAllowMerge());
        properties.put(HIBERNATE_CONNECTION_DRIVER, mySqldevConsulService.getHibernateConnectionDriverClass());
        properties.put(MySQLConstants.HIBERNATE_JDBC_URL, mySqldevConsulService.getHibernateMtUrl().split(DATABASE)[0]);
        multiTenantProperties.put(MySQLConstants.JDBC_URL, mySqldevConsulService.getHibernateConnectionUrl());
        multiTenantProperties.put(MySQLConstants.MT_JDBC_URL, mySqldevConsulService.getHibernateMtUrl());
        multiTenantProperties.put(MySQLConstants.DB_USERNAME, mySqldevConsulService.getHibernateConnectionUsername());
        multiTenantProperties.put(MySQLConstants.DB_PASSWORD, mySqldevConsulService.getHibernateConnectionPassword());

        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setPackagesToScan(mySqldevConsulService.getHibernatePackagesScan());
        sessionFactory.setHibernateProperties(properties);
        return sessionFactory;
    }


}


