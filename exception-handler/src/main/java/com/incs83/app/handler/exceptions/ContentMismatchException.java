/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 * This class is used to throw exception when there is mismatch of content.
 * <br><br>{@link #code}  is used to give Api response code when throwing ContentMismatchException Exception.
 * <br><br>{@link #message}  is used to give Exception message when throwing ContentMismatchException Exception..
 *
 */
package com.incs83.app.handler.exceptions;

import com.incs83.app.handler.response.ResponseCode;

/**
 * This class is used to throw throw ContentMismatchException Exception.
 * <br><br>{@link #code} is used to give Api response code when throwing ContentMismatchException Exception.
 * <br><br>{@link #message} is used to give Exception message when throwing ContentMismatchException Exception.
 */
public class ContentMismatchException extends Exception {
    private int code;
    private String message;

    public ContentMismatchException(ResponseCode responseCode) {
        super(responseCode.getMessage());
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
