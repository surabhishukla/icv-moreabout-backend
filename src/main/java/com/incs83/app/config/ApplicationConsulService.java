/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope

/**
 * This service is used to set value of properties form consul server.consul provides is a key-value store. Keys can be organized into some sort of a tree by separating its “path” components with forward slash.
 * This class contains properties required to connect over some feature(mqtt,elastic,jwt,kafka,aws,openwhisk,simulation,s3 bucket,ses,minio) and contains getters and setters method to access those properties.
 */

public class ApplicationConsulService {
    @Value("${maxtry}")
    private int maxTry;
    @Value("${poolSize}")
    private int poolSize;
    @Value("${elastic-search.hostname}")
    private String elasticSearchHostname;
    @Value("${elastic-search.port}")
    private String elasticSearchPort;
    @Value("${elastic-search.enable}")
    private String elasticSearchEnable;
    @Value("${jwt.refreshTokenExpTime}")
    private String jwtRefreshTokenExpTime;
    @Value("${jwt.tokenExpirationTime}")
    private String jwtTokenExpirationTime;
    @Value("${jwt.tokenSigningKey}")
    private String jwtTokenSigningKey;
    @Value("${jwt.tokenIssuer}")
    private String jwtTokenIssuer;
    @Value("${kafka.topic}")
    private String kafkaTopic;
    @Value("${aws.accessKey}")
    private String awsAccessKey;
    @Value("${aws.area}")
    private String awsRegion;
    @Value("${aws.secretKey}")
    private String awsSecretKey;
    @Value("${openwhisk.url}")
    private String openwhiskUrl;
    @Value("${openwhisk.adminUrl}")
    private String openwhiskAdminUrl;
    @Value("${openwhisk.gatewayUrl}")
    private String openwhiskGatewayUrl;
    @Value("${openwhisk.namespace}")
    private String openwhiskNamespace;
    @Value("${openwhisk.username}")
    private String openwhiskUsername;
    @Value("${openwhisk.password}")
    private String openwhiskPassword;
    @Value("${simulation.url}")
    private String simulationUrl;
    @Value("${mqtt.auth.enabled}")
    private boolean mqttAuthEnabled;
    @Value("${common.mqtt.rootca}")
    private String mqttRootCA;
    @Value("${system.mode}")
    private String systemMode;
    @Value("${mqtt.url}")
    private String mqttUrl;
    @Value("${mqtt.internal.url}")
    private String mqttInternalUrl;
    @Value("${mqtt.username}")
    private String mqttUsername;
    @Value("${mqtt.password}")
    private String mqttPassword;
    @Value("${common.base.url}")
    private String baseUrl;
    @Value("${spring.kafka.bootstrap-servers}")
    private String kafkaBootstrapServers;
    @Value("${sfp.connection.url}")
    private String sfpConnectionUrl;
    @Value("${ctl.url}")
    private String ctlUrl;
    @Value("${s3.bucket}")
    private String s3Bucket;
    @Value("${s3.accessKey}")
    private String s3AccessKey;
    @Value("${s3.secretKey}")
    private String s3SecretKey;
    @Value("${s3.region}")
    private String s3Region;
    @Value("${internal.secretKey}")
    private String internalSecretKey;
    @Value("${platform.server.port}")
    private int platformServerPort;
    @Value("${job.server.port}")
    private int jobServerPort;
    @Value("${minio.url}")
    private String minioUrl;
    @Value("${minio.accessKey}")
    private String minioAccessKey;
    @Value("${minio.secretKey}")
    private String minioSecretKey;
    @Value("${minio.bucket}")
    private String minioBucketName;
    @Value("${ses.accessKey}")
    private String sesAccessKey;
    @Value("${ses.region}")
    private String sesRegion;
    @Value("${ses.secretKey}")
    private String sesSecretKey;
    @Value("${ses.email-from}")
    private String fromEmail;
    @Value("${sagemaker.accessKey}")
    private String sagemakerAccessKey;
    @Value("${sagemaker.secretKey}")
    private String sagemakerSecretKey;
    @Value("${sagemaker.region}")
    private String sagemakerRegion;
    @Value("${consul.url}")
    private String consulUrl;
    @Value("${sns.accessKey}")
    private String snsAccessKey;
    @Value("${sns.region}")
    private String snsRegion;
    @Value("${sns.secretKey}")
    private String snsSecretKey;
    @Value("${sns.senderId}")
    private String snsSenderId;
    @Value("${paypal.secretKey}")
    private String paypalSecretKey;
    @Value("${paypal.clientId}")
    private String paypalClientId;
    @Value("${paypal.env}")
    private String paypalEnv;
    @Value("${paypal.product.home.url}")
    private String paypalProductHomeUrl;
    @Value("${mqtt.session.disconnect}")
    private String mqttSessionDisconnect;
    @Value("${mqtt.session.status}")
    private String mqttSessionStatus;
    @Value("${mqtt.session.key}")
    private String mqttSessionKey;
    @Value("${services.status}")
    private String serviceStatus;
    @Value("${common.activationKeys}")
    private String activationKeys;
    @Value("${common.weatherApiKey}")
    private String weatherApiKey;
    @Value("${common.videoUrl}")
    private String videoUrl;
    @Value("${common.m83LogoUrl}")
    private String m83LogoUrl;
    @Value("${common.documentUrl}")
    private String documentUrl;
    @Value("${EmailAlertAfterExpiryInDays}")
    private int EmailAlertAfterExpiryInDays;
    @Value("${RemoveTrialUserAfterExpiryInDays}")
    private int RemoveTrialUserAfterExpiryInDays;
    @Value("${sns.enableSMSForIndia}")
    private Boolean enableSmsForIndia;
    @Value("${common.sparkPod}")
    private String sparkPod;
    @Value("${basicAuthKey}")
    private String basicAuthKey;

    public String getSnsSenderId() {
        return snsSenderId;
    }

    public void setSnsSenderId(String snsSenderId) {
        this.snsSenderId = snsSenderId;
    }

    public String getM83LogoUrl() {
        return m83LogoUrl;
    }

    public void setM83LogoUrl(String m83LogoUrl) {
        this.m83LogoUrl = m83LogoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getMqttInternalUrl() {
        return mqttInternalUrl;
    }

    public void setMqttInternalUrl(String mqttInternalUrl) {
        this.mqttInternalUrl = mqttInternalUrl;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getMinioUrl() {
        return minioUrl;
    }

    public void setMinioUrl(String minioUrl) {
        this.minioUrl = minioUrl;
    }

    public String getMinioAccessKey() {
        return minioAccessKey;
    }

    public void setMinioAccessKey(String minioAccessKey) {
        this.minioAccessKey = minioAccessKey;
    }

    public String getMinioSecretKey() {
        return minioSecretKey;
    }

    public void setMinioSecretKey(String minioSecretKey) {
        this.minioSecretKey = minioSecretKey;
    }

    public String getMinioBucketName() {
        return minioBucketName;
    }

    public void setMinioBucketName(String minioBucketName) {
        this.minioBucketName = minioBucketName;
    }

    public int getPlatformServerPort() {
        return platformServerPort;
    }

    public void setPlatformServerPort(int platformServerPort) {
        this.platformServerPort = platformServerPort;
    }

    public int getJobServerPort() {
        return jobServerPort;
    }

    public void setJobServerPort(int jobServerPort) {
        this.jobServerPort = jobServerPort;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public boolean isMqttAuthEnabled() {
        return mqttAuthEnabled;
    }

    public void setMqttAuthEnabled(boolean mqttAuthEnabled) {
        this.mqttAuthEnabled = mqttAuthEnabled;
    }

    public String getMqttRootCA() {
        return mqttRootCA;
    }

    public void setMqttRootCA(String mqttRootCA) {
        this.mqttRootCA = mqttRootCA;
    }

    public String getKafkaBootstrapServers() {
        return kafkaBootstrapServers;
    }

    public void setKafkaBootstrapServers(String kafkaBootstrapServers) {
        this.kafkaBootstrapServers = kafkaBootstrapServers;
    }

    public String getSfpConnectionUrl() {
        return sfpConnectionUrl;
    }

    public void setSfpConnectionUrl(String sfpConnectionUrl) {
        this.sfpConnectionUrl = sfpConnectionUrl;
    }

    public String getMqttUrl() {
        return mqttUrl;
    }

    public void setMqttUrl(String mqttUrl) {
        this.mqttUrl = mqttUrl;
    }

    public String getMqttUsername() {
        return mqttUsername;
    }

    public void setMqttUsername(String mqttUsername) {
        this.mqttUsername = mqttUsername;
    }

    public String getMqttPassword() {
        return mqttPassword;
    }

    public void setMqttPassword(String mqttPassword) {
        this.mqttPassword = mqttPassword;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl it is specific to  environment like (__TENANT_NAME__.internal-iot83.com for stage,__TENANT_NAME__.iot83.com for prod.)
     */

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getSystemMode() {
        return systemMode;
    }

    public void setSystemMode(String systemMode) {
        this.systemMode = systemMode;
    }

    public String getCtlUrl() {
        return ctlUrl;
    }

    public void setCtlUrl(String ctlUrl) {
        this.ctlUrl = ctlUrl;
    }

    public String getOpenwhiskUrl() {
        return openwhiskUrl;
    }

    public void setOpenwhiskUrl(String openwhiskUrl) {
        this.openwhiskUrl = openwhiskUrl;
    }

    public String getOpenwhiskAdminUrl() {
        return openwhiskAdminUrl;
    }

    public void setOpenwhiskAdminUrl(String openwhiskAdminUrl) {
        this.openwhiskAdminUrl = openwhiskAdminUrl;
    }

    public String getOpenwhiskGatewayUrl() {
        return openwhiskGatewayUrl;
    }

    public ApplicationConsulService setOpenwhiskGatewayUrl(String openwhiskGatewayUrl) {
        this.openwhiskGatewayUrl = openwhiskGatewayUrl;
        return this;
    }

    public String getOpenwhiskNamespace() {
        return openwhiskNamespace;
    }

    /**
     * @param openwhiskNamespace OpenWhisk actions, triggers, and rules belong in a namespace.namespace is reserved for entities that are distributed with the OpenWhisk system.the namespace can be left off if it is the user's default namespace.
     */
    public void setOpenwhiskNamespace(String openwhiskNamespace) {
        this.openwhiskNamespace = openwhiskNamespace;
    }

    public String getOpenwhiskUsername() {
        return openwhiskUsername;
    }

    /**
     * @param openwhiskUsername Serverless Framework needs access to account credentials for your OpenWhisk provider so that it can create and manage resources on your behalf for that username to be set.
     */
    public void setOpenwhiskUsername(String openwhiskUsername) {
        this.openwhiskUsername = openwhiskUsername;
    }

    public String getOpenwhiskPassword() {
        return openwhiskPassword;
    }

    /**
     * @param openwhiskPassword Serverless Framework needs access to account credentials for your OpenWhisk provider so that it can create and manage resources on your behalf for that password to be set.
     */
    public void setOpenwhiskPassword(String openwhiskPassword) {
        this.openwhiskPassword = openwhiskPassword;
    }

    public String getSimulationUrl() {
        return simulationUrl;
    }

    public void setSimulationUrl(String simulationUrl) {
        this.simulationUrl = simulationUrl;
    }

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    /**
     * @param awsAccessKey Access keys are  for the AWS account user. You can use access keys to sign programmatic requests to the  AWS API.
     */
    public void setAwsAccessKey(String awsAccessKey) {
        this.awsAccessKey = awsAccessKey;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    /**
     * @param awsRegion describe the Regions available for your AWS account.
     */
    public void setAwsRegion(String awsRegion) {
        this.awsRegion = awsRegion;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    /**
     * @param awsSecretKey Secret Access Key are used for programmatic (API) access to AWS services.
     */
    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    public String getKafkaTopic() {
        return kafkaTopic;
    }

    /**
     * @param kafkaTopic A Topic is a feed name .All Kafka records are organized into topics. Producer applications write data to topics and consumer applications read from topics.
     */
    public void setKafkaTopic(String kafkaTopic) {
        this.kafkaTopic = kafkaTopic;
    }

    public String getElasticSearchHostname() {
        return elasticSearchHostname;
    }

    /**
     * @param elasticSearchHostname By default, Elasticsearch binds to loopback addresses only .In order to form a cluster with nodes on other servers, your node will need to bind to a non-loopback address.usually all you need to configure is network.host:
     */

    public void setElasticSearchHostname(String elasticSearchHostname) {
        this.elasticSearchHostname = elasticSearchHostname;
    }

    public String getElasticSearchPort() {
        return elasticSearchPort;
    }

    /**
     * @param elasticSearchPort The Elasticsearch default communication port is 9300/tcp.The communication port can be changed in the Elasticsearch configuration.
     */

    public void setElasticSearchPort(String elasticSearchPort) {
        this.elasticSearchPort = elasticSearchPort;
    }

    public String getElasticSearchEnable() {
        return elasticSearchEnable;
    }

    /**
     * @param elasticSearchEnable The enabled setting to  enable the elastic feature.
     */

    public void setElasticSearchEnable(String elasticSearchEnable) {
        this.elasticSearchEnable = elasticSearchEnable;
    }

    public String getJwtTokenExpirationTime() {
        return jwtTokenExpirationTime;
    }

    /**
     * @param jwtTokenExpirationTime An API that accepts JWTs does an independent verification without depending on the JWT source so the API server has no way of knowing if this was a stolen token. This is why JWTs have an expiry value.
     */

    public void setJwtTokenExpirationTime(String jwtTokenExpirationTime) {
        this.jwtTokenExpirationTime = jwtTokenExpirationTime;
    }

    public String getJwtRefreshTokenExpTime() {
        return jwtRefreshTokenExpTime;
    }

    /**
     * @param jwtRefreshTokenExpTime (whenever an access token is required to access a specific resource, a client may use a refresh token to get a new access token issued by the authentication server.)
     */

    public void setJwtRefreshTokenExpTime(String jwtRefreshTokenExpTime) {
        this.jwtRefreshTokenExpTime = jwtRefreshTokenExpTime;
    }


    public String getJwtTokenSigningKey() {
        return jwtTokenSigningKey;
    }

    /**
     * @param jwtTokenSigningKey This is a string that is used in the algorithm that generates the cryptographic signature for the token.
     */

    public void setJwtTokenSigningKey(String jwtTokenSigningKey) {
        this.jwtTokenSigningKey = jwtTokenSigningKey;
    }

    public String getS3Bucket() {
        return s3Bucket;
    }

    /**
     * @param s3Bucket A bucket is owned by the AWS account that created it.(Be unique across all of Amazon S3.Be between 3 and 63 characters long, Not contain uppercase, Start with a lowercase letter or number. characters
     */
    public void setS3Bucket(String s3Bucket) {
        this.s3Bucket = s3Bucket;
    }

    public String getS3AccessKey() {
        return s3AccessKey;
    }

    /**
     * @param s3AccessKey is required for Security Credentials purpose to access S3bucket .
     */
    public void setS3AccessKey(String s3AccessKey) {
        this.s3AccessKey = s3AccessKey;
    }

    public String getS3SecretKey() {
        return s3SecretKey;
    }

    /**
     * @param s3SecretKey is required for Security Credentials purpose to access S3bucket .
     */
    public void setS3SecretKey(String s3SecretKey) {
        this.s3SecretKey = s3SecretKey;
    }

    public String getS3Region() {
        return s3Region;
    }

    /**
     * @param s3Region S3 buckets are region specific.you create a new bucket you need to select the target region for that bucket.
     */
    public void setS3Region(String s3Region) {
        this.s3Region = s3Region;
    }

    public String getInternalSecretKey() {
        return internalSecretKey;
    }

    public void setInternalSecretKey(String internalSecretKey) {
        this.internalSecretKey = internalSecretKey;
    }

    public int getMaxTry() {
        return maxTry;
    }

    public void setMaxTry(int maxTry) {
        this.maxTry = maxTry;
    }

    public String getSagemakerAccessKey() {
        return sagemakerAccessKey;
    }

    public void setSagemakerAccessKey(String sagemakerAccessKey) {
        this.sagemakerAccessKey = sagemakerAccessKey;
    }

    public String getSagemakerSecretKey() {
        return sagemakerSecretKey;
    }

    public void setSagemakerSecretKey(String sagemakerSecretKey) {
        this.sagemakerSecretKey = sagemakerSecretKey;
    }

    public String getSagemakerRegion() {
        return sagemakerRegion;
    }

    public void setSagemakerRegion(String sagemakerRegion) {
        this.sagemakerRegion = sagemakerRegion;
    }

    public String getConsulUrl() {
        return consulUrl;
    }

    public void setConsulUrl(String consulUrl) {
        this.consulUrl = consulUrl;
    }

    public String getSesAccessKey() {
        return sesAccessKey;
    }

    public void setSesAccessKey(String sesAccessKey) {
        this.sesAccessKey = sesAccessKey;
    }

    public String getSesRegion() {
        return sesRegion;
    }

    public void setSesRegion(String sesRegion) {
        this.sesRegion = sesRegion;
    }

    public String getSesSecretKey() {
        return sesSecretKey;
    }

    public void setSesSecretKey(String sesSecretKey) {
        this.sesSecretKey = sesSecretKey;
    }

    public String getSnsAccessKey() {
        return snsAccessKey;
    }

    public void setSnsAccessKey(String snsAccessKey) {
        this.snsAccessKey = snsAccessKey;
    }

    public String getSnsRegion() {
        return snsRegion;
    }

    public void setSnsRegion(String snsRegion) {
        this.snsRegion = snsRegion;
    }

    public String getSnsSecretKey() {
        return snsSecretKey;
    }

    public void setSnsSecretKey(String snsSecretKey) {
        this.snsSecretKey = snsSecretKey;
    }

    public String getPaypalSecretKey() {
        return paypalSecretKey;
    }

    public void setPaypalSecretKey(String paypalSecretKey) {
        this.paypalSecretKey = paypalSecretKey;
    }

    public String getPaypalClientId() {
        return paypalClientId;
    }

    public void setPaypalClientId(String paypalClientId) {
        this.paypalClientId = paypalClientId;
    }

    public String getPaypalEnv() {
        return paypalEnv;
    }

    public void setPaypalEnv(String paypalEnv) {
        this.paypalEnv = paypalEnv;
    }

    public String getPaypalProductHomeUrl() {
        return paypalProductHomeUrl;
    }

    public void setPaypalProductHomeUrl(String paypalProductHomeUrl) {
        this.paypalProductHomeUrl = paypalProductHomeUrl;
    }

    public String getMqttSessionDisconnect() {
        return mqttSessionDisconnect;
    }

    public void setMqttSessionDisconnect(String mqttSessionDisconnect) {
        this.mqttSessionDisconnect = mqttSessionDisconnect;
    }

    public String getMqttSessionStatus() {
        return mqttSessionStatus;
    }

    public void setMqttSessionStatus(String mqttSessionStatus) {
        this.mqttSessionStatus = mqttSessionStatus;
    }

    public String getMqttSessionKey() {
        return mqttSessionKey;
    }

    public void setMqttSessionKey(String mqttSessionKey) {
        this.mqttSessionKey = mqttSessionKey;
    }

    public String getActivationKeys() {
        return activationKeys;
    }

    public void setActivationKeys(String activationKeys) {
        this.activationKeys = activationKeys;
    }

    public String getWeatherApiKey() {
        return weatherApiKey;
    }

    public void setWeatherApiKey(String weatherApiKey) {
        this.weatherApiKey = weatherApiKey;
    }

    public String getJwtTokenIssuer() {
        return jwtTokenIssuer;
    }

    public void setJwtTokenIssuer(String jwtTokenIssuer) {
        this.jwtTokenIssuer = jwtTokenIssuer;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public int getEmailAlertAfterExpiryInDays() {
        return EmailAlertAfterExpiryInDays;
    }

    /**
     * @param emailAlertAfterExpiryInDays is value to be set for giving no. of days for expiration.
     */
    public void setEmailAlertAfterExpiryInDays(int emailAlertAfterExpiryInDays) {
        EmailAlertAfterExpiryInDays = emailAlertAfterExpiryInDays;
    }

    public int getRemoveTrialUserAfterExpiryInDays() {
        return RemoveTrialUserAfterExpiryInDays;
    }

    /**
     * @param removeTrialUserAfterExpiryInDays is value to be set for giving no. of days for account closed.
     */
    public void setRemoveTrialUserAfterExpiryInDays(int removeTrialUserAfterExpiryInDays) {
        RemoveTrialUserAfterExpiryInDays = removeTrialUserAfterExpiryInDays;
    }

    public Boolean getEnableSmsForIndia() {
        return enableSmsForIndia;
    }

    public void setEnableSmsForIndia(Boolean enableSmsForIndia) {
        this.enableSmsForIndia = enableSmsForIndia;
    }

    public String getSparkPod() {
        return sparkPod;
    }

    public void setSparkPod(String sparkPod) {
        this.sparkPod = sparkPod;
    }

    public String getBasicAuthKey() {
        return basicAuthKey;
    }

    public void setBasicAuthKey(String basicAuthKey) {
        this.basicAuthKey = basicAuthKey;
    }
}
