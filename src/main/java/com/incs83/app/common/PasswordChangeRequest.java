/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.common;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * This request class is used when signed-in authority is newly registered and received password on mail.
 * So this class allows signed-in authority to change password.
 */

public class PasswordChangeRequest {
    @NotNull
    @NotBlank
    private String newPassword;

    private String tokenId;
    private String token;

    public String getToken() {
        return token == null ? null : token.trim();
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword.trim();
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getTokenId() {
        return tokenId == null ? null : tokenId.trim();
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
