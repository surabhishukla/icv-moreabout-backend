/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.context;

import com.incs83.app.security.tokenFactory.UserContext;

import java.util.Objects;

/**
 * This class is used to set or get User Context, User Context is a class which contains the contextual
 *  information about a current logged in user.
 */
public final class ExecutionContext {

    public static final ThreadLocal<ExecutionContext> CONTEXT = new ThreadLocal<>();
    private final UserContext userContext;

    public ExecutionContext(UserContext userContext) {
        this.userContext = userContext;
    }

    public static void clear() {
        CONTEXT.remove();
    }

    public static ExecutionContext get() {
        return CONTEXT.get();
    }

    public static void set(ExecutionContext ec) {
        if (Objects.isNull(ec)) {
            throw new RuntimeException("Invalid Execution Context");
        }
        CONTEXT.set(ec);
    }

    public UserContext getUsercontext() {
        return userContext;
    }

    @Override
    public String toString() {
        return "ExecutionContext{" +
                "userContext=" + userContext +
                '}';
    }
}
