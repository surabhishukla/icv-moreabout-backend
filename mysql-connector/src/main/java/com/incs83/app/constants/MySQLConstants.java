/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.constants;

public interface MySQLConstants {
    String HIBERNATE_DIALECT = "hibernate.dialect";
    String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    String HIBERNATE_DDL_MODE = "hibernate.hbm2ddl.auto";
    String HIBERNATE_CONNECTION_DRIVER="hibernate.connection.driver_class";
    String HIBERNATE_MT_CONNECTION_PROVIDER = "hibernate.multi_tenant_connection_provider";
    String HIBERNATE_MT_TENANT_IDENTIFIER = "hibernate.tenant_identifier_resolver";
    String HIBERNATE_MT = "hibernate.multiTenancy";
    String HIBERNATE_CP_PROVIDER_CLASS = "hibernate.connection.provider_class";
    String HIBERNATE_CP_MIN_SIZE = "hibernate.c3p0.min_size";
    String HIBERNATE_CP_MAX_SIZE = "hibernate.c3p0.max_size";
    String HIBERNATE_CP_TIMEOUT = "hibernate.c3p0.timeout";
    String HIBERNATE_CP_MAX_STATEMENTS = "hibernate.c3p0.max_statements";
    String HIBERNATE_ALLOW_MERGE_ENTITY = "hibernate.event.merge.entity_copy_observer";

    String JDBC_URL = "jdbcurl";
    String MT_JDBC_URL = "mtjdbcurl";
    String DB_USERNAME = "username";
    String DB_PASSWORD = "password";
    String DATABASE = "__DATABASE__";
    String HIBERNATE_PACKAGE_SCAN = "com.incs83.app.entities";
    String HIBERNATE_JDBC_URL="hibernate.jdbc.url";
    String SPRING_PROFILES_ACTIVE = "spring.profiles.active";
    String CONSUL_CONNECT_ENABLED="consul.connect.enabled";
}
