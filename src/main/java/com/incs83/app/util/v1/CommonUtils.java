/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.constants.CommonConstants;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.context.RequestContext;
import com.incs83.app.entities.GroupDetails;
import com.incs83.app.entities.ParentEntity;
import com.incs83.app.entities.Role;
import com.incs83.app.entities.User;
import com.incs83.app.handler.exceptions.InvalidContextException;
import com.incs83.app.mt.DataAccessService;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.security.tokenFactory.Authority;
import com.incs83.app.security.tokenFactory.Entitlement;
import com.incs83.app.security.tokenFactory.UserContext;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.incs83.app.constants.CommonConstants.*;
import static com.incs83.app.mt.SqlCurrentTenantIdentifier._tenantIdentifierSql;
import static com.incs83.app.mt.SqlCurrentTenantIdentifier._tenantName;

/**
 * This class is used for common utility which can be used frequently.
 * <br><br>{@link #generateUUID()} this method is used for generating random UUID which is being returned after replacing hyphen by "".
 * <br><br>{@link #setCreateEntityFields(Object)} this method is used for setting up the value of createdAt and createdBy.
 * <br><br>{@link #initiateVersion()} it is used to the version and then return it.
 * <br><br>{@link #setUpdateEntityFields(Object)} this method is used for setting up the value of updatedAt and updatedBy.
 * <br><br>{@link #getLocaleStringFromClientHTTPRequest(HttpServletRequest)} it is used to get the local language of the place where client belongs.
 * <br><br>{@link #getCurrentTimeInMillis()} it is used to get the time in millisecond.
 * <br><br>{@link #isAccountAdmin()} It is used to check whether the role is of Account admin or not.
 * <br><br>{@link #isSAASDeveloper()} It is used to check whether the role is of SAASDeveloper or not.
 * <br><br>{@link #getAllProjectIdsOfLoggedInUser()} It is used to get all project id of logged in users.
 * <br><br>{@link #isSysAdmin()} It is used to check whether the role is of SysAdmin or not.
 * <br><br>{@link #getUserIdOfLoggedInUser()} It is used to get the user id of logged in user.
 * <br><br>{@link #isEndUser()} It is used to check whether the authorities is of EndUser or not
 * <br><br>{@link #getRoleNameOfLoggedInUser()} It is used to get the role name of logged in user.
 * <br><br>{@link #generateScopePayloadForToken(User)} It is used to prepare certain payload for token.
 * <br><br>{@link #setDBName(HttpServletRequest)} This method is used for setting up the db name.
 * <br><br>{@link #setDBName(String)} This method is used for setting up the db name.
 * <br><br>{@link #getRoleIdOfLoggedInUser()} It is used to get the role id of logged in user.
 * <br><br>{@link #setCreatedBy(Document)} It is used to set the value of createdBy and createdAt.
 * <br><br>{@link #setUpdatedBy(Document)} It is used to set the value of updatedBy and updatedAt.
 * <br><br>{@link #getTenantName()} It is used to get the tenantName.
 * <br><br>{@link #getUserName()} It is used to get the userName.
 * <br><br>{@link #getPassword()} It is used to get the password.
 * <br><br>{@link #ConvertFileToMultiPart(String, String)} It is used to convert the file to multipartFile.
 * <br><br>{@link #getGroupNameById(String)} It is used to get the group name by the id.
 * <br><br>{@link #getBaseUrl(HttpServletRequest)} It is used to get the base url.
 */
@Service
public final class CommonUtils {

    private static final Logger LOG = LogManager.getLogger();
    @Autowired
    private DataAccessService dataAccessService;

    /**
     * This method is used for generating random UUID which is being returned after replacing hyphen by "".
     *
     * @return UUID of String type
     */
    public static String generateUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * This method is used for setting up the value of createdAt and createdBy.
     *
     * @param obj if it's an object of ParentEntity than the value of createdAt and createdBy will be set up.
     */
    public static void setCreateEntityFields(Object obj) {
        if (obj instanceof ParentEntity) {
            ParentEntity entity = (ParentEntity) obj;
            entity.setCreatedAt(DateUtils.getCurrentDate());
            UserContext userContext = SecurityUtils.getAuthenticatedUser();

            entity.setCreatedBy(entity.getCreatedBy() == null || entity.getCreatedBy().equals(CommonConstants.SYSTEM) ? CommonConstants.SYSTEM : userContext.getId());
        }
    }

    /**
     * It is used to the version.
     *
     * @return the version 1.0.0 which is of string type.
     */
    public static String initiateVersion() {
        return "1.0.0";
    }

    private static int minimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }

    /**
     * This method is used for setting up the value of updatedAt and updatedBy.
     *
     * @param obj if it's an object of ParentEntity than the value of updatedAt and updatedBy will be set up.
     */
    public static void setUpdateEntityFields(Object obj) {
        if (obj instanceof ParentEntity) {
            ParentEntity entity = (ParentEntity) obj;
            entity.setUpdatedAt(DateUtils.getCurrentDate());
            UserContext userContext = SecurityUtils.getAuthenticatedUser();
            entity.setUpdatedBy(entity.getUpdatedBy() == null || entity.getUpdatedBy().equals(CommonConstants.SYSTEM) ? CommonConstants.SYSTEM : userContext.getId());
        }
    }

    /**
     * It is used to get the local language of the place where client belongs
     *
     * @param httpServletRequest it helps in getting the language which is used at the place of client
     * @return it returns the local language which of type string.
     */
    public static String getLocaleStringFromClientHTTPRequest(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getLocale().getLanguage();
    }

    /**
     * It is used to get the time in millisecond.
     *
     * @return it returns the time in mills which of long type.
     */
    public static long getCurrentTimeInMillis() {
        return Calendar.getInstance().getTimeInMillis();
    }

    /**
     * It is used to check whether the role is of Account admin or not.
     *
     * @return true or false depending on the condition.
     */
    public static boolean isAccountAdmin() {
        if (ExecutionContext.get() == null) {
            return false;
        }
        if (ExecutionContext.get().getUsercontext().getRoleName().equals("ACCOUNT_ADMIN")) {
            return true;
        }
        return false;
    }

    /**
     * It is used to check whether the role is of SAASDeveloper or not.
     *
     * @return true or false depending on the condition.
     */
    public static boolean isSAASDeveloper() {
        if (ExecutionContext.get() == null) {
            return false;
        }
        if (ExecutionContext.get().getUsercontext().getRoleName().equals("DEVELOPER") && TENANT_TYPE_SAAS.equals(ExecutionContext.get().getUsercontext().getTenantType())) {
            return true;
        }
        return false;
    }

    /**
     * It is used to check whether the role is of DefaultAccountAdmin or not.
     *
     * @return true or false depending on the condition.
     */
    public static boolean isDefaultAccountAdmin() {
        if (ExecutionContext.get() == null) {
            return false;
        }
        if (ExecutionContext.get().getUsercontext().getRoleName().equals("ACCOUNT_ADMIN") && ExecutionContext.get().getUsercontext().getDefaultAccountAdmin()) {
            return true;
        }
        return false;
    }

    /**
     * It is used to get all project id of logged in users.
     *
     * @return list of project id of the logged in users.
     */
    public static List<String> getAllProjectIdsOfLoggedInUser() {
        if (ExecutionContext.get() == null) {
            return new ArrayList<>();
        }

        return ExecutionContext.get().getUsercontext().getProjectIds();
    }

    /**
     * It is used to check whether the role is of SysAdmin or not.
     *
     * @return true or false depending on the condition.
     */
    public static boolean isSysAdmin() {
        if (ExecutionContext.get() == null) {
            return false;
        }
        if (ExecutionContext.get().getUsercontext().getRoleName().equals("SYSTEM_ADMIN")) {
            return true;
        }
        return false;
    }

    /**
     * It is used to get the user id of logged in user.
     *
     * @return user id of the logged in user which of type string.
     */
    public static String getUserIdOfLoggedInUser() {
        if (Objects.nonNull(ExecutionContext.get())) {
            return ExecutionContext.get().getUsercontext().getId();
        } else {
            return RequestContext._currentUserId.get();
        }
    }

    /**
     * It is used to check whether the authorities is of EndUser or not.
     *
     * @return true or false depending on the condition.
     */
    public static boolean isEndUser() {
        if (ExecutionContext.get() == null) {
            return false;
        }
        if (ExecutionContext.get().getUsercontext().getAuthorities().stream().filter(p -> (p.getName().equals("End User"))).collect(Collectors.toList()).size() >= 1) {
            return true;
        }
        return false;
    }

    /**
     * It is used to get the role name of logged in user.
     *
     * @return role name of the logged in user which of type string.
     */
    public static String getRoleNameOfLoggedInUser() {
        if (ExecutionContext.get() == null) {
            return null;
        }
        return ExecutionContext.get().getUsercontext().getAuthorities().get(0).getName();
    }

    /**
     * It is used to prepare certain payload for token.
     *
     * @param user it is the object of user which is used for preparing payload for token.
     * @return list of authorities.
     */
    public static List<Authority> generateScopePayloadForToken(User user) {
        List<Authority> authorities = new ArrayList<>();
        user.getUserRole().forEach(userRole -> {
            Role role = userRole.getRole();
            Authority authority = new Authority();
            authority.setName(role.getName());
            authority.setGrantBy(userRole.getGrantBy());
            authority.setSource(userRole.getSource());
            /*if (Objects.isNull(role.getMapping())) {
                LOG.info("Role Mapping Not Found");
                throw new AuthenticationServiceException("No Mapping Found for current role");
            }*/
            try {
                List<Entitlement> list = (List<Entitlement>) new ObjectMapper().readValue(role.getMapping(), List.class);
                List<Entitlement> entitlements = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    Entitlement entitlement = new ObjectMapper().convertValue(list.get(i), Entitlement.class);
                    entitlements.add(entitlement);
                }
                authority.setEntitlements(entitlements);
            } catch (IOException e) {
                e.printStackTrace();
            }
            authorities.add(authority);
        });
        return authorities;
    }

    /**
     * This method is used for setting up the db name
     *
     * @param httpServletRequest it is the object of HttpServletRequest which is used for setting up the db name.
     * @return true or false.
     */
    public static boolean setDBName(HttpServletRequest httpServletRequest) {
        String dbName = null;
        if (httpServletRequest.getRequestURI().equalsIgnoreCase("/swagger-resources/configuration/ui") ||
                httpServletRequest.getRequestURI().equalsIgnoreCase("/swagger-resources/configuration/security")) {
            return true;
        }
        String header = httpServletRequest.getHeader(VMQ_HEADER);
        if (Objects.nonNull(header))
            return true;
        String context = httpServletRequest.getHeader(CommonConstants.X_TENANT_NAME);
        //TODO: REMOVE AFTER TESTING ON DC
        if (ValidationUtils.isNullOrEmpty(context) && httpServletRequest.getRequestURL().toString().contains("localhost")) {
            context = "83incs";
        }
        Enumeration<String> enumeration = httpServletRequest.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String elem = enumeration.nextElement();
        }
        if (Objects.isNull(context)) {
            throw new InvalidContextException("Please specify 'X-TENANT-NAME' in Header");
        }

        if (context.equals(CommonConstants.SUPER_ADMIN_CONTEXT)) {
            dbName = CommonConstants.M83_MASTER_DB;
        } else {
            dbName = CommonConstants.M83_MASTER_DB;
            _tenantIdentifierSql.set(dbName);
            try {
//                dbName = ConnectionCache.getConnectionDetail(context.toLowerCase()).getSchemaName();
            } catch (Exception e) {
                throw new AuthenticationServiceException("Tenant details not found");
            }
        }
        _tenantIdentifierSql.set(dbName);
        _tenantName.set(context);
        return true;
    }

    /**
     * This method is used for setting up the db name
     *
     * @param tenant it is the name of tenant which is used for setting up the name of db.
     * @return true or false.
     */
    public static boolean setDBName(String tenant) {
        String dbName;
        if (tenant.equals(CommonConstants.SUPER_ADMIN_CONTEXT)) {
            dbName = CommonConstants.M83_MASTER_DB;
        } else {
            try {
                dbName = ConnectionCache.getConnectionDetail(tenant.toLowerCase()).getSchemaName();
            } catch (Exception e) {
                throw new AuthenticationServiceException("Tenant details not found");
            }
        }
        _tenantIdentifierSql.set(dbName);
        _tenantName.set(tenant);
        return true;
    }

    /**
     * It is used to get the role id of logged in user.
     *
     * @return role id of the logged in user which of type string.
     */
    public static String getRoleIdOfLoggedInUser() {
        return ExecutionContext.get().getUsercontext().getRoleId();
    }

    /**
     * It is used to get the project id of logged in user.
     * @return project id of the logged in user which of type string.
     */
    public static String getProjectContextOfLoggedInUser() {
        if (Objects.nonNull(ExecutionContext.get())) {
            return ExecutionContext.get().getUsercontext().getProjectContext();
        } else {
            return RequestContext._currentProjectId.get();
        }
    }

    /**
     * It is used to convert the integer, or float values present in Document into double.
     * @param document it is the object of Document
     */
    public static void convertDocumentIntegerDouble(Document document) {
        Set<String> entrySets = document.keySet();
        entrySets.forEach(set -> {
            Object value = document.get(set);
            if (value instanceof Number && !(value instanceof Double)) {
                if (value instanceof Integer) {
                    Integer i = document.getInteger(set);
                    Double d = i.doubleValue();
                    d = d + 0.12D;
                    document.replace(set, d);
                } else if (value instanceof Long) {
                    Long i = document.getLong(set);
                    Double d = i.doubleValue();
                    d = d + 0.12D;
                    document.replace(set, d);
                }  else if (value instanceof Float) {
                    Float i = (Float) document.get(set);
                    Double d = i.doubleValue();
                    d = d + 0.12D;
                    document.replace(set, d);
                }  else if (value instanceof Document) {
                    convertDocumentIntegerDouble((Document) value);
                }
            }
        });
    }

    /**
     * It is used to set the value of createdBy and createdAt.
     *
     * @param response it is of document type which is used to set the value of createdBy and createdAt.
     */
    public static void setCreatedBy(Document response) {
        response.put("createdBy", ExecutionContext.get().getUsercontext().getId());
        response.put("createdAt", CommonUtils.getCurrentTimeInMillis());
    }

    /**
     * It is used to set the value of updatedBy and updatedAt.
     *
     * @param response it is of document type which is used to set the value of updatedBy and updatedAt.
     */
    public static void setUpdatedBy(Document response) {
        response.put("updatedBy", ExecutionContext.get().getUsercontext().getId());
        response.put("updatedAt", CommonUtils.getCurrentTimeInMillis());
    }

    /**
     * It is used to get the tenantName.
     *
     * @return tenantName which is of string type.
     */
    public static String getTenantName() {
        UserContext userContext = SecurityUtils.getAuthenticatedUser();
        String tenantName = userContext.getId();
        return tenantName;
    }

    /**
     * It is used to get the userName.
     *
     * @return userName which is of string type.
     */
    public static String getUserName() {
        ConnectionCache connectionCache = ConnectionCache.getConnectionDetail(SqlCurrentTenantIdentifier._tenantName.get());
        String owAuth = connectionCache.getOwAuth();
        byte[] byteKey = Base64.getDecoder().decode(owAuth);
        String key = new String(byteKey);
        String[] keyList = key.split(":");
        return keyList[0];
    }

    /**
     * It is used to get the password.
     *
     * @return password which is of string type.
     */
    public static String getPassword() {
        ConnectionCache connectionCache = ConnectionCache.getConnectionDetail(SqlCurrentTenantIdentifier._tenantName.get());
        String owAuth = connectionCache.getOwAuth();
        byte[] byteKey = Base64.getDecoder().decode(owAuth);
        String key = new String(byteKey);
        String[] keyList = key.split(":");
        return keyList[1];
    }

    /**
     * It is used to convert the file to multipartFile.
     *
     * @param path      it is the path of the file location.
     * @param extension it is extension of the file format.
     * @return multipartFile which is of MultipartFile type.
     * @throws Exception this method might throw exception
     */
    public static MultipartFile ConvertFileToMultiPart(String path, String extension) throws Exception {
        MultipartFile multipartFile = null;
        try {
            File file = new File(path);
            FileInputStream input = new FileInputStream(file);
            multipartFile = new MockMultipartFile(file.getName(),
                    extension, "application/x-tgz", IOUtils.toByteArray(input));
        } catch (FileNotFoundException e) {
            LOG.error("File not Found " + path);
        } catch (IOException e) {
            LOG.error("Exception occurred while reading file", e.getMessage());
        }
        return multipartFile;
    }

    /**
     * This method is used to check whether the request is came from DemoTour or not.
     * @return true if condition is correct otherwise return false
     */
    public static Boolean fromDemoTour() {
        return RequestContext._fromDemoTour.get();
    }

    /**
     * This method is used to set true or false depending on the condition
     * @param fromdemotour it contains true if condition is correct otherwise false
     */
    public static void setFromDemoTour(Boolean fromdemotour) {
        RequestContext._fromDemoTour.set(fromdemotour);
    }

    /**
     * This method is used to check whether the request is came from DynamicApplication or not.
     * @return true if condition is correct otherwise return false
     */
    public static Boolean fromDynamicApplication() {
        return RequestContext._dynamicApplication.get();
    }

    /**
     * This method is used to set true or false depending on the condition
     * @param fromDynamicApplication it contains true if condition is correct otherwise false
     */
    public static void setFromDynamicApplication(Boolean fromDynamicApplication) {
        RequestContext._dynamicApplication.set(fromDynamicApplication);
    }

    /**
     * This method is used to set the project id.
     * @param projectId it contains the project id of the current user.
     */
    public static void setProjectId(String projectId) {
        RequestContext._currentProjectId.set(projectId);
    }

    /**
     * This method is used to get the type of tenant such as saas, enterprise etc..
     * @return tenant type if gotten otherwise return ""
     * @throws Exception this method might throw exception
     */
    public static String getTenantType() throws Exception {
        if (ExecutionContext.get() == null) {
            return EMPTY_STRING;
        }
        return ExecutionContext.get().getUsercontext().getTenantType();
    }

    /**
     * This method is used to get the dynamicApplication id from the request
     * @return id of type {String}
     */
    public static String getDynamicApplicationId() {
        return RequestContext._dynamicApplicationId.get();
    }

    /**
     * This method is used to set the dynamicApplication id
     * @param applicationId it contains the application id.
     */
    public static void setDynamicApplicationId(String applicationId) {
        RequestContext._dynamicApplicationId.set(applicationId);
    }

    /**
     * This method is used to check whether Logged In user is having role Project_Admin or not.
     *
     * @return {@code boolean} return true if  Logged In user having role Project_Admin.
     */
    public static boolean isProjectAdmin() {
        if (ExecutionContext.get() == null) {
            return false;
        }
        if (ExecutionContext.get().getUsercontext().getRoleName().equals("PROJECT_ADMIN") && TENANT_TYPE_SAAS.equals(ExecutionContext.get().getUsercontext().getTenantType())) {
            return true;
        }
        return false;
    }

    /**
     * It is used to get the group name by the id.
     *
     * @param groupId it is the group which helps in getting the name of the group.
     * @return groupName which is of string type.
     * @throws Exception this method might throw exception
     */
    public String getGroupNameById(String groupId) throws Exception {
        String groupName = EMPTY_STRING;
        if (Objects.nonNull(groupId) && !groupId.isEmpty()) {
            GroupDetails groupDetails = (GroupDetails) dataAccessService.read(GroupDetails.class, groupId);
            if (Objects.nonNull(groupDetails)) {
                groupName = groupDetails.getName();
            }
        }
        return groupName;
    }

    /**
     * It is used to get the base url.
     *
     * @param httpServletRequest it helps in getting the serverName, serverPort.
     * @return base url if found correct information otherwise returns null.
     */
    public String getBaseUrl(HttpServletRequest httpServletRequest) {
        if (Objects.nonNull(httpServletRequest)) {
            String port = httpServletRequest.getServerName().equals("localhost") ? CommonConstants.COLON + httpServletRequest.getServerPort() : EMPTY_STRING;
            return httpServletRequest.getScheme() + "://" +
                    httpServletRequest.getServerName() +
                    port;
        } else {
            return null;
        }

    }

    /**
     * This method is used to check whether the mobile number is indian or not
     * @param mobileNumber it helps in getting the serverName, serverPort.
     * @return true or false.
     * @throws Exception this method might throw exception
     */
    public static boolean isIndianMobile(String mobileNumber) throws Exception {
        if (Objects.nonNull(mobileNumber) && !mobileNumber.isEmpty()) {
            int length = mobileNumber.length();
            if (mobileNumber.startsWith("91") && length == 12) {
                return true;
            }
        }
        return false;
    }
}
