/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * This class is used to send email using Amazon Simple Email Service.
 * <br><br>{@link #accessKey}  Access Keys are used to sign the requests you send to Amazon S3.
 *      `               Like the Username/Password pair you use to access your AWS Management Console.
 * <br><br>{@link #secretKey} It is like your password. For your own security.
 * <br><br>{@link #region} is a physical location around the world where amazon cluster data centers.
 * <br><br>{@link #emailFrom} It is sender email Id.
 */

@ConfigurationProperties(prefix = "ses")
@Configuration("sesConfig")
@Import(ApplicationConsulService.class)
public class SESConfig {
    @Autowired
    ApplicationConsulService applicationConsulService;

    private String accessKey;
    private String secretKey;
    private String region;
    private String emailFrom;

    public String getAccessKey() {
        return applicationConsulService.getSesAccessKey();
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return applicationConsulService.getSesSecretKey();
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getRegion() {
        return applicationConsulService.getSesRegion();
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEmailFrom() {
        return applicationConsulService.getFromEmail();
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }
}
