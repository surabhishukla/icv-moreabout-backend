/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.dao;

import java.util.List;

/**
 * This class consist of getter and setter methods.
 * <br><br>{@link #setMax(Integer)} This method is used to set the value of max.
 * <br><br>{@link #setTotalCount(Integer)} This method is used to set the value of totalCount.
 * <br><br>{@link #setObjects(List)} This method is used to set the value of objects.
 */
public class Page<T> {
    private Integer max;
    private Integer totalCount;
    private List<T> objects;

    public Page() {
    }

    public Integer getMax() {
        return this.max;
    }

    /**
     * @param max value of max is to be set here
     */
    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getTotalCount() {
        return this.totalCount;
    }

    /**
     * @param totalCount value of totalCount is to be set here
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getObjects() {
        return this.objects;
    }

    /**
     * @param objects value of objects is to be set here where it is of List type
     */
    public void setObjects(List<T> objects) {
        this.objects = objects;
    }
    }
