/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.handler.exceptions.ApiException;
import com.incs83.app.handler.exceptions.OWException;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.incs83.app.constants.ApplicationConstants.EMPTY_STRING;

/**
 * This class extends the HttpService class which helps in performing operations like post, delete, get, put etc... on the server.
 * <br><br>{@link #doGet(String, HashMap, HashMap, String, String, Boolean)} This method  is used to query or to get some information from the server.
 * <br><br>{@link #doGet(String, HashMap, HashMap, String, String, Boolean, Boolean)} This method  is used to query or to get some information from the server.
 * <br><br>{@link #doPost(StringEntity, String, HashMap, HashMap, String, String, Boolean)} This method is used to sending information to the server.
 * <br><br>{@link #doPost(StringEntity, String, HashMap, HashMap, String, String, Boolean, Boolean)} This method is used to sending information to the server.
 * <br><br>{@link #doPut(StringEntity, String, HashMap, HashMap, String, String, Boolean)} This method uses the request URI to supply a modified version of the requested resource which replaces the original version of the resource
 * <br><br>{@link #doDelete(HashMap, String, HashMap, String, String, Boolean)} This method allows the client to remove anything from the server.
 * <br><br>{@link #doGetByte(String, HashMap, HashMap)} This method is used get the information from server in the form of array of bytes.
 * <br><br>{@link #getCloseableHttpClient(String, String, Boolean)} This method is used to provide the CloseableHttpClient object.
 * <br><br>{@link #getSSLContext()} This method acquires the SSLContext object.
 * <br><br>{@link #getOWError(HashMap)} This method is used to get the openWhisk error if occur, otherwise return null.
 * <br><br>{@link #doGetWithNoContent(String, HashMap, HashMap)} This method is used get the information from server.
 * <br><br>{@link #doDelete(String, HashMap, StringEntity)} This method allows the client to remove anything from the server.
 * <br><br>{@link #doPatch(StringEntity, String, HashMap, HashMap)} This method supplies a set of instructions to modify the resource.
 */
@Service(value = "HttpServiceImpl2")
public class HttpServiceImpl2 extends HttpService {
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private ObjectMapper mapper;

    /**
     * This method  is used to query or to get some information from the server.
     * @param url it contains openWhisk url appended with "/api/v1/" and namespace.
     * @param headerInfo it consist of null value
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doGet(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        long t1 = Calendar.getInstance().getTimeInMillis();
        String resp;
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpGet httpGet = new HttpGet(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpGet::addHeader);
            }
            HttpResponse httpResponse = httpClient.execute(httpGet);
            resp = EntityUtils.toString(httpResponse.getEntity());
            long t2 = Calendar.getInstance().getTimeInMillis();
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                LOG.error("Error ::: Response ");
                HashMap<String, Object> error = mapper.readValue(resp, HashMap.class);
                throw new OWException(httpResponse.getStatusLine().getStatusCode(), Objects.nonNull(error.get("error")) ? error.get("error").toString() : "OWException: Exception Occurred while invoking doPost Call...", httpResponse.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doGet Call...", e);
            throw e;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return resp;
    }

    /**
     * This method  is used to query or to get some information from the server.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @param overload it is of boolean type.
     * @throws Exception this method might throw exception
     * @return httpResponse of type {HttpResponse}.
     */
    public HttpResponse doGet(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable, Boolean overload) throws Exception {
        HttpResponse httpResponse = null;
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpGet httpGet = new HttpGet(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpGet::addHeader);
            }

            httpResponse = httpClient.execute(httpGet);
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doGet Call...", e);
        }
        return httpResponse;
    }

    /**
     * This method is used to sending information to the server.
     * @param entity it is of StringEntity type.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPost(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        long t1 = Calendar.getInstance().getTimeInMillis();
        String resp;
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpPost httpPost = new HttpPost(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPost::addHeader);
            }
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            resp = EntityUtils.toString(httpResponse.getEntity());
            long t2 = Calendar.getInstance().getTimeInMillis();
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK && httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED && httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED) {
                HashMap<String, Object> error = mapper.readValue(resp, HashMap.class);
                HashMap<String, Object> appError = getOWError(error);
                if (Objects.nonNull(appError)) {
                    throw new OWException(Integer.valueOf(String.valueOf(appError.get("code"))), String.valueOf(appError.get("message")), Integer.valueOf(String.valueOf(appError.get("httpStatus"))));
                } else {
                    throw new OWException(httpResponse.getStatusLine().getStatusCode(), Objects.nonNull(error.get("error")) ? error.get("error").toString() : "OWException: Exception Occurred while invoking doPost Call...", httpResponse.getStatusLine().getStatusCode());
                }
            }
        } catch (OWException e) {
            LOG.error("Exception Occurred while invoking doPost Call...", e);
            throw e;
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doPost Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return resp;
    }

    /**
     * This method is used to sending information to the server.
     * @param entity it is of StringEntity type.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @param overload it is of boolean type.
     * @throws Exception this method might throw exception
     * @return respMap of type {HashMap}.
     */
    public HashMap<String, Object> doPost(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable, Boolean overload) throws Exception {
        String resp;
        HashMap<String, Object> respMap = new HashMap<>();
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }

            HttpPost httpPost = new HttpPost(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPost::addHeader);
            }
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            resp = EntityUtils.toString(httpResponse.getEntity());
            respMap = new ObjectMapper().readValue(resp, HashMap.class);
            respMap.put("statusCode", httpResponse.getStatusLine().getStatusCode());
        } catch (Exception e) {
            LOG.error("Error while post call");
        } finally {
            try {
                httpClient.close();
            } catch (IOException var26) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }
        return respMap;
    }

    /**
     * This method uses the request URI to supply a modified version of the requested resource which replaces the original version of the resource
     * @param entity it is of StringEntity type.
     * @param url it contains openWhisk url appended with "/api/v1/" and namespace.
     * @param headerInfo it contains information about packageName and the namespace.
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPut(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        long t1 = Calendar.getInstance().getTimeInMillis();
        String resp = null;
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpPut httpPut = new HttpPut(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPut::addHeader);
            }
            httpPut.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPut);
            resp = EntityUtils.toString(httpResponse.getEntity());
            long t2 = Calendar.getInstance().getTimeInMillis();
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                HashMap<String, Object> error = mapper.readValue(resp, HashMap.class);
                throw new OWException(httpResponse.getStatusLine().getStatusCode(), Objects.nonNull(error.get("error")) ? error.get("error").toString() : "OWException: Exception Occurred while invoking doPost Call...", httpResponse.getStatusLine().getStatusCode());
            }
        } catch (OWException e) {
            if (e.getHttpStatus() == 404) {
                httpClient.close();
            }
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doPut Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return resp;
    }

    /**
     * This method allows the client to remove anything from the server.
     * @param queryParams it consist of null value
     * @param url it contains openWhisk url appended with "/api/v1/", tenantName, and packageName.
     * @param headerInfo it consist of null value
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doDelete(HashMap<String, Object> queryParams, String url, HashMap<String, String> headerInfo, String username, String password, Boolean sslEnable) throws Exception {
        long t1 = Calendar.getInstance().getTimeInMillis();
        String resp;
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpDelete httpDelete = new HttpDelete(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpDelete::addHeader);
            }
            HttpResponse httpResponse = httpClient.execute(httpDelete);
            resp = EntityUtils.toString(httpResponse.getEntity());
            long t2 = Calendar.getInstance().getTimeInMillis();
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK && httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_NO_CONTENT) {
                LOG.error("Error ::: Response ");
                HashMap<String, Object> error = mapper.readValue(resp, HashMap.class);
                throw new OWException(httpResponse.getStatusLine().getStatusCode(), Objects.nonNull(error.get("error")) ? error.get("error").toString() : "OWException: Exception Occurred while invoking doPost Call...", httpResponse.getStatusLine().getStatusCode());
            }
        } catch (OWException owe) {
            LOG.error("Exception Occurred while invoking doDelete Call...", owe);
            throw owe;
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doDelete Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return resp;
    }

    /**
     * This method is used get the information from server in the form of array of bytes.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     * @return resp of type {byte[]}.
     */
    public byte[] doGetByte(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        byte[] resp;
        try {
            StringBuffer params = new StringBuffer("");
            if (Objects.nonNull(queryParams)) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }

            if (!params.toString().equals("")) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }

            HttpGet httpGet = new HttpGet(url);
            headerInfo.forEach(httpGet::addHeader);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            resp = EntityUtils.toByteArray(httpResponse.getEntity());
            if (httpResponse.getStatusLine().getStatusCode() != 200) {
                LOG.error("Error ::: Response ");
                throw new ApiException(ApiResponseCode.ERROR);
            }
        } catch (Exception var16) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException var15) {
                LOG.error("Error Closing HTTP HttConnection...");
            }

        }

        return resp;
    }

    /**
     * This method is used to provide the CloseableHttpClient object.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be or false .
     * @throws Exception
     * @return httpClient of type {CloseableHttpClient}.
     */
    private CloseableHttpClient getCloseableHttpClient(String username, String password, Boolean sslEnable) throws Exception {
        CloseableHttpClient httpClient;
        if (ValidationUtils.nonNullOrEmpty(username) && ValidationUtils.nonNullOrEmpty(password)) {
            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
            provider.setCredentials(AuthScope.ANY, credentials);
            if (!sslEnable) {
                SSLContext sc = getSSLContext();
                httpClient = HttpClientBuilder.create().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).setSSLContext(sc).setDefaultCredentialsProvider(provider).build();
            } else {
                httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
            }

        } else {
            httpClient = HttpClients.createDefault();
        }
        return httpClient;
    }

    /**
     * This method acquires the SSLContext object.
     * @throws Exception
     * @return sc of type {SSLContext}.
     */
    private SSLContext getSSLContext() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        return sc;
    }

    /**
     * This method is used to get the openWhisk error if occur, otherwise return null.
     * @param errMap it consist of information like code, httpStatus, message
     * @return map if error exist otherwise return null, it is of type {HashMap}
     */
    public HashMap<String, Object> getOWError(HashMap<String, Object> errMap) {
        Map<String, Object> resp = (Map) errMap.get("response");
        Map<String, Object> err = null;
        if (Objects.nonNull(resp)) {
            Map<String, Object> result = (Map) resp.get("result");
            if (result.get("error") instanceof Map) {
                err = (Map) result.get("error");
            } else {
                err = new HashMap<>();
                err.put("code", HttpStatus.SC_BAD_GATEWAY);
                err.put("httpStatus", HttpStatus.SC_BAD_GATEWAY);
                err.put("message", String.valueOf(result.get("error")));
            }
        } else {
            if (errMap.get("error") instanceof Map) {
                err = (Map) errMap.get("error");
            } else {
                err = new HashMap<>();
                err.put("code", HttpStatus.SC_BAD_GATEWAY);
                err.put("httpStatus", HttpStatus.SC_BAD_GATEWAY);
                err.put("message", String.valueOf(errMap.get("error")));
            }
        }
        if (Objects.nonNull(err) && Objects.nonNull(err.get("httpStatus"))) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("code", err.get("code"));
            map.put("httpStatus", err.get("httpStatus"));
            map.put("message", err.get("message"));
            return map;
        }


        return null;
    }

    /**
     * This method is used get the information from server.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     */
    public void doGetWithNoContent(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        String resp;
        try {
            StringBuffer params = new StringBuffer("");
            if (Objects.nonNull(queryParams)) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }

            if (!params.toString().equals("")) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }

            HttpGet httpGet = new HttpGet(url);
            headerInfo.forEach(httpGet::addHeader);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() != 204) {
                throw new ApiException(ApiResponseCode.ERROR);
            }
        } catch (Exception var16) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException var15) {
                LOG.error("Error Closing HTTP HttConnection...", var15);
            }

        }
    }

    /**
     * This method allows the client to remove anything from the server.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param entity it is of StringEntity type.
     * @throws Exception this method might throw exception
     * @return response of type {boolean}.
     */
    public boolean doDelete(String url, HashMap<String, String> headerInfo, StringEntity entity) throws Exception {
        boolean response;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpResponse httpResponse;
        HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(url);
        httpDelete.setEntity(entity);
        headerInfo.forEach(httpDelete::addHeader);
        httpResponse = httpClient.execute(httpDelete);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT || httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            response = true;
        } else {
            LOG.error("Error Invoking Delete Call");
            throw new ApiException(ApiResponseCode.BAD_REQUEST);
        }
        return response;
    }

    /**
     * This method supplies a set of instructions to modify the resource.
     * @param entity it is of StringEntity type.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc......
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPatch(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        String resp = null;
        try {
            StringBuffer params = new StringBuffer("");
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }

            if (!params.toString().equals("")) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }

            HttpPatch httpPatch = new HttpPatch(url);
            headerInfo.forEach(httpPatch::addHeader);
            httpPatch.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPatch);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK && httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED && httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_NO_CONTENT) {
                throw new Exception("Error");
            }

            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_NO_CONTENT)
                resp = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception var17) {
            throw new Exception("Error");
        } finally {
            try {
                httpClient.close();
            } catch (IOException var16) {
            }

        }

        return resp;
    }
}
