/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.incs83.app.config.SESConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is used for making connection with amazon email service.
 * <br><br>{@link #getAmazonSimpleEmailService()} this method is used to make connection with the amazon email services on the basis of provided access key and secret key.
 */
@Component
public class SESUtils {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private SESConfig sesConfig;

    /**
     * This method is used to make connection with the amazon email services on the basis of provided access key and secret key.
     * @return emailService which of type AmazonSimpleEmailService.
     */
    public AmazonSimpleEmailService getAmazonSimpleEmailService() {
        AmazonSimpleEmailService emailService = null;
        try {
            emailService = AmazonSimpleEmailServiceClientBuilder
                    .standard()
                    .withRegion(sesConfig.getRegion())
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(sesConfig.getAccessKey(), sesConfig.getSecretKey())))
                    .build();
        } catch (Exception e) {
            LOG.error("Unable to establish connection with AWS email service - " + e);
        }
        return emailService;
    }
}
