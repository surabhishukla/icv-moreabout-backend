/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.common;

import com.incs83.app.entities.User;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Objects;

/**
 * This class is used to create new user.
 * <br><br>{@link  #roles} is used to decide what roles have been assigned to users.
 *
 */

public class UserRequest {

    private List<String> roles;

    private List<String> compartments;

    @NotBlank(message = "UserRequest.NotBlank.firstName")
    @Size(min = 1, message = "UserRequest.Min.firstName", max = 255)
    private String firstName;

    private String lastName;

    @NotEmpty
    @Email(message = "UserRequest.Invalid.email")
    private String email;

    @NotBlank(message = "UserRequest.NotBlank.company")
    @NotNull
    private String company;

    private String title;

    private String mobilePhone;

    private Long concurrencyVersion;

    public static User generateFrom(UserRequest userRequest, User u, Boolean isCreateUser) {

        if (Objects.nonNull(userRequest.getCompany())) {
            u.setCompany(userRequest.getCompany());
        }

        if (Objects.nonNull(userRequest.getTitle())) {
            u.setTitle(userRequest.getTitle());
        }

        if (Objects.nonNull(userRequest.getFirstName())) {
            if (StringUtils.isBlank(userRequest.getFirstName()))
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "First Name cannot be blank");
            else
                u.setFirstName(userRequest.getFirstName());
        }

        if (Objects.nonNull(userRequest.getLastName())) {
            u.setLastName(userRequest.getLastName());
        }
        if (isCreateUser) {
            if (Objects.nonNull(userRequest.getEmail())) {
                if (StringUtils.isBlank(userRequest.getEmail())) {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Email Id cannot be blank");
                } else if (ValidationUtils.validateEmail(userRequest.getEmail())) {
                    u.setEmail(userRequest.getEmail());
                } else
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Please Provide a valid email Id");
            }
        }

        if (Objects.nonNull(userRequest.getMobilePhone())) {
            u.setMobile(userRequest.getMobilePhone());
        }
        return u;
    }

    public static User populateAccountAdmin(UserRequest userRequest, User u) {

        if (Objects.nonNull(userRequest.getCompany())) {
            u.setCompany(userRequest.getCompany());
        }

        if (Objects.nonNull(userRequest.getTitle())) {
            u.setTitle(userRequest.getTitle());
        }

        if (Objects.nonNull(userRequest.getFirstName())) {
            u.setFirstName(userRequest.getFirstName());
        }

        if (Objects.nonNull(userRequest.getLastName())) {
            u.setLastName(userRequest.getLastName());
        }

        if (Objects.nonNull(userRequest.getEmail()) && !userRequest.getEmail().equals(u.getEmail())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Cannot update Email");
        }

        if (Objects.nonNull(userRequest.getMobilePhone())) {
            u.setMobile(userRequest.getMobilePhone());
        }

        if (Objects.nonNull(userRequest.getCompartments())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Cannot update Compartments");
        }

        if (Objects.nonNull(userRequest.getRoles())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Cannot update Role");
        }
        return u;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getCompartments() {
        return compartments;
    }

    public void setCompartments(List<String> compartments) {
        this.compartments = compartments;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Long getConcurrencyVersion() {
        return concurrencyVersion;
    }

    public void setConcurrencyVersion(Long concurrencyVersion) {
        this.concurrencyVersion = concurrencyVersion;
    }
}
