/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.mt;

import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.springframework.stereotype.Component;

/**
 * This class is used for providing basic support for MultiTenantConnectionProvider implementations using individual ConnectionProvider instances per tenant.
 * <br><br>{@link #getAnyConnectionProvider()} This method allows access to the database metadata of the underlying database(s) in situations where we do not have a tenant id.
 * <br><br>{@link #selectConnectionProvider(String)} This method is used to select the connection provider.
 */
@Component
public final class MultiTenantService extends AbstractMultiTenantConnectionProvider {
    private static final long serialVersionUID = 3305310005148709133L;

    /**
     * This method allows access to the database metadata of the underlying database(s) in situations where we do not have a tenant id.
     * @return the object of ConnectionProviderService in which default tenant id passed as parameter.
     */
    @Override
    protected ConnectionProvider getAnyConnectionProvider() {
        return new ConnectionProviderService(SqlCurrentTenantIdentifier.DEFAULT_TENANT);
    }

    /**
     * This method is used to select the connection provider.
     * @param s it consist of tenant id.
     * @return the object of ConnectionProviderService in which tenant id passed as parameter.
     */
    @Override
    protected ConnectionProvider selectConnectionProvider(String s) {
        return new ConnectionProviderService(s);
    }
}
