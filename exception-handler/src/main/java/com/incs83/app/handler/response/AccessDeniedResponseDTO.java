/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.handler.response;

import io.swagger.annotations.ApiModelProperty;

/**
 * This class is used as response when an unauthorized authority tries to access resource .
 * <br><br>{@link #code}  is used to give Api response code in AccessDeniedResponseDTO.
 * <br><br>{@link #message}  is used to give Exception message in AccessDeniedResponseDTO.
 * <br><br>{@link #data} is used to provide Response data in AccessDeniedResponseDTO.
 */

public class AccessDeniedResponseDTO {

    @ApiModelProperty(notes = "Response Code", required = true)
    private Integer code;
    @ApiModelProperty(notes = "Response Message", required = true)
    private String message;
    @ApiModelProperty(notes = "Response Data", required = true)
    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
