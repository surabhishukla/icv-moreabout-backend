/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.aop;

import com.incs83.app.constants.CommonConstants;
import com.incs83.app.dao.HibernateService;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AOPTransactional {
    @Autowired
    private SessionFactory sessionFactory;

    @Around("@within(com.incs83.app.annotation.Transactional) || @annotation(com.incs83.app.annotation.Transactional)" + CommonConstants.IGNORE_FINAL)
    public Object handleTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = null;
        if (SqlCurrentTenantIdentifier._isTransactionalInit.get() != null && SqlCurrentTenantIdentifier._isTransactionalInit.get()) {
            try {
                result = joinPoint.proceed();
            } catch (Throwable ex) {
                throw ex;
            }
            return result;
        }
        SqlCurrentTenantIdentifier._isTransactionalInit.set(true);
        Session session = SqlCurrentTenantIdentifier._currentSession.get();
        Transaction trx;
        if (session == null) {
            session = sessionFactory.withOptions().tenantIdentifier(SqlCurrentTenantIdentifier._tenantIdentifierSql.get()).openSession();
            SqlCurrentTenantIdentifier._currentSession.set(session);
            SqlCurrentTenantIdentifier._currentTransaction.set(session.getTransaction());
        }
        trx = SqlCurrentTenantIdentifier._currentTransaction.get();
        if (trx == null) {
            trx = session.getTransaction();
            SqlCurrentTenantIdentifier._currentTransaction.set(trx);
        }
        if (!trx.isActive())
            trx.begin();
        try {
            result = joinPoint.proceed();
            trx = SqlCurrentTenantIdentifier._currentTransaction.get();
            trx.commit();
        } catch (Throwable ex) {
            trx.rollback();
            throw ex;
        } finally {
            HibernateService.closeSilently(session);
            SqlCurrentTenantIdentifier._currentSession.set(null);
            SqlCurrentTenantIdentifier._isTransactionalInit.set(false);
        }
        return result;
    }
}
