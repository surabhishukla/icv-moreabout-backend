/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.common;

/**
 * This class is used as a request class to send email with attachments in case of connectors.
 * <br><br>{@link #filePath} path of the attached file.
 * <br><br>{@link #recipientEmail} to whom email is sent.
 * <br><br>{@link #connectorName} name of the connector.
 * <br><br>{@link #html_part} is the template used in email.
 * <br><br>{@link #text_part} text part of the email.
 * <br><br>{@link #subject} subject of the email.
 */
public class EmailWithAttachmentRequest {
    private String filePath;
    private String recipientEmail;
    private String connectorName;
    private String html_part;
    private String text_part;
    private String subject;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getHtml_part() {
        return html_part;
    }

    public void setHtml_part(String html_part) {
        this.html_part = html_part;
    }

    public String getText_part() {
        return text_part;
    }

    public void setText_part(String text_part) {
        this.text_part = text_part;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
