/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.handler.exceptions;

import com.incs83.app.handler.response.*;
import com.incs83.app.handler.utils.ApiExceptionHandlerUtils;
import com.incs83.app.localization.MessageByLocale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(basePackages = "com.incs83.app")
@RestController
@SuppressWarnings("rawtypes")
public class ApiExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private MessageByLocale messageByLocale;

    @Autowired
    private ApiExceptionHandlerUtils apiExceptionHandlerUtils;

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {ApiException.class})
    public ResponseDTO handleGenericException(ApiException e, HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {ValidationException.class})
    public ResponseDTO handleGenericException(ValidationException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage(messageByLocale.getMessage(e.getMessage()));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {UrlNotFoundException.class})
    public ResponseDTO handleGenericException(UrlNotFoundException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Url Not Found");
        return responseUtil.exception(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {RequestRejectedException.class})
    public ResponseDTO handleGenericException(RequestRejectedException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage(messageByLocale.getMessage(e.getMessage()));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {TypeMismatchException.class})
    public ResponseDTO exception(TypeMismatchException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorDTO errorResponse = new ValidationErrorDTO();
        errorResponse.addError(new ValidationErrorDTO.FieldErrorDTO(e.getErrorCode(), messageByLocale.getMessage(e.getMessage())));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = {AuthMethodNotSupportedException.class})
    public ResponseDTO exception(AuthMethodNotSupportedException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {RecordNotFoundException.class})
    public ResponseDTO exception(RecordNotFoundException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = {AuthEntityNotAllowedException.class})
    public ResponseDTO exception(AuthEntityNotAllowedException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {HttpMessageConversionException.class})
    public ResponseDTO exception(HttpMessageConversionException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage("Bad Request, Request not completed");
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);

    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {BindException.class})
    public ResponseDTO exception(BindException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage("Bad Request, Request not completed");
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {NumberFormatException.class})
    public ResponseDTO exception(NumberFormatException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage("Bad Request, Request not completed");
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseDTO exception(HttpMessageNotReadableException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage("Bad Request, Request not completed");
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = HttpMediaTypeNotSupportedException.class)
    public ResponseDTO exception(HttpMediaTypeNotSupportedException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage(messageByLocale.getMessage(e.getMessage()));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseDTO exception(MethodArgumentNotValidException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        e.getBindingResult().getFieldErrors().forEach(field -> errorResponse.addErrorMessage(messageByLocale.getMessage(field.getDefaultMessage())));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public ResponseDTO exception(MissingServletRequestParameterException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorDTO errorResponse = new ValidationErrorDTO();
        errorResponse.addError(new ValidationErrorDTO.FieldErrorDTO(e.getParameterName(), messageByLocale.getMessage(e.getMessage())));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public ResponseDTO exception(MethodArgumentTypeMismatchException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorDTO errorResponse = new ValidationErrorDTO();
        errorResponse.addError(new ValidationErrorDTO.FieldErrorDTO(e.getMessage(), messageByLocale.getMessage(e.getMessage())));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Data Type Mismatch for Parameter(s) " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ServletRequestBindingException.class)
    public ResponseDTO exception(ServletRequestBindingException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorDTO errorResponse = new ValidationErrorDTO();
        errorResponse.addError(new ValidationErrorDTO.FieldErrorDTO(e.getMessage(), messageByLocale.getMessage(e.getMessage())));
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = BadCredentialsException.class)
    public ResponseDTO exception(BadCredentialsException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ContentMismatchException.class)
    public ResponseDTO exception(ContentMismatchException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addErrorMessage("Bad Request, Request not completed");
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error("Invalid / Missing Parameters " + errorResponse);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);

    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = NullPointerException.class)
    public ResponseDTO exception(NullPointerException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error(String.format("########### Got [[%s]] exception  at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getStackTrace()[0].toString()));
        LOGGER.error(e);
        return responseUtil.exception(ApiResponseCode.ERROR);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {ResourceExceedException.class})
    public ResponseDTO exception(ResourceExceedException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        return responseUtil.validationFailed(e.getCode(), e.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MissingServletRequestPartException.class)
    public ResponseDTO exception(MissingServletRequestPartException e,HttpServletRequest httpServletRequest) throws Exception {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        errorResponse.addErrorMessage("File format not supported");
        return responseUtil.validationFailed(HttpStatus.BAD_REQUEST.value(), errorResponse);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {ServletException.class})
    public ResponseDTO exception(ServletException e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        return responseUtil.exception(ApiResponseCode.NOT_A_VALID_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public ResponseDTO exception(Exception e,HttpServletRequest httpServletRequest) throws Exception {
        apiExceptionHandlerUtils.mdcLog(httpServletRequest);
        LOGGER.error(String.format("########### Got [[%s]] exception with message: %s at path =>  [[%s]]  ########  ########", e.getClass().getName(), e.getMessage(), e.getStackTrace()[0].toString()));
        return responseUtil.exception(ApiResponseCode.ERROR);
    }
}