/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Objects;

public class GenerateUtils<T, U> {

    private static final Logger LOG = LogManager.getLogger();

    public void generateFrom(T request, U etlConfig) {
        CommonUtils.setCreateEntityFields(etlConfig);
        updateFrom(request, etlConfig);
    }

    public void updateFrom(T request, U data) {
        Field[] requestFields = request.getClass().getDeclaredFields();
        for (Field field : requestFields) {
            try {
                System.out.println("field Name" + field.getName());
                field.setAccessible(true);
                if (Objects.nonNull(field.get(request))) {
                    Field dataField = data.getClass().getDeclaredField(field.getName());
                    dataField.setAccessible(true);
                    dataField.set(data, field.get(request));
                }

            } catch (IllegalAccessException e) {
                LOG.error("IllegalArgumentException occurred");
            } catch (NoSuchFieldException e) {
                LOG.error("NoSuchFieldException occurred");
            }
        }
    }
}
