/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.InvalidParameterException;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.incs83.app.common.MobileMessageRequest;
import com.incs83.app.config.ApplicationConsulService;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.util.v1.SNSUtils;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * This is AmazonSNSPublisherService class which is used for sending notification to the number.
 * <br><br>{@link #sendMessage(MobileMessageRequest)} This method is used in preparing and sending the message.
 * <br><br>{@link #sendSMSMessage(AmazonSNS, MobileMessageRequest)} this method is used to set the various details and send the sms.
 */
@Service
public class AmazonSNSPublisherService {

    private static final Logger LOG = LogManager.getLogger();
    @Autowired
    private SNSUtils snsUtils;
    @Autowired
    private ApplicationConsulService applicationConsulService;

    /**
     * This method is used in preparing and sending the message
     * @param request(message,mobileNumber)
     *                message it is the actual message that will be send
     *                mobileNumber it is the mobile number of receiver
     * @return result of PublishResult type
     */
    public PublishResult sendMessage(MobileMessageRequest request) {
        if (ValidationUtils.isNullOrEmpty(request.getMessage())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Message field is Mandatory cannot be null");
        }
        if (ValidationUtils.isNullOrEmpty(request.getMobileNumber())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "MobileNUmber field is Mandatory cannot be null");
        }

        PublishResult result = sendSMSMessage(snsUtils.getAmazonSNS(), request);
        return result;
    }

    /**
     * This method is used to set the various details and send the message.
     *
     * @param snsClient is the object of AmazonSNS
     * @param request(message,mobileNumber)
     *                message it is the actual message that will be send
     *                mobileNumber it is the mobile number of receiver
     * @return result of PublishResult type
     */
    private PublishResult sendSMSMessage(AmazonSNS snsClient, MobileMessageRequest request) {

        try {
            Map<String, MessageAttributeValue> smsAttributes =
                    new HashMap<String, MessageAttributeValue>();
            if (applicationConsulService.getEnableSmsForIndia()) {
                smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                        .withStringValue(applicationConsulService.getSnsSenderId()) //The sender ID shown on the device.
                        .withDataType("String"));
            }
            smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                    .withStringValue("Transactional") //Sets the type to Transactional.
                    .withDataType("String"));
            PublishResult result = snsClient.publish(new PublishRequest()
                    .withMessage(request.getMessage())
                    .withPhoneNumber(request.getMobileNumber())
                    .withMessageAttributes(smsAttributes)
            );
            return result;
        } catch (InvalidParameterException e) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Invalid MobileNumber");
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw e;
        }
    }
}
