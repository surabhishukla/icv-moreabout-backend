/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.incs83.app.handler.exceptions.ApiException;
import com.incs83.app.handler.response.ApiResponseCode;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

/**
 * This class extends the HttpService class which helps in performing operations like post, delete, get, put etc... on the server.
 * <br><br>{@link #doPut(StringEntity, String, HashMap, HashMap)} This method is used to updating anything on the server.
 * <br><br>{@link #doPost(StringEntity, String, HashMap, HashMap)} This method is used to sending information to the server.
 * <br><br>{@link #doDelete(HashMap, String, HashMap)} This method allows the client to remove anything from the server.
 */
@Service
public class HttpServiceImpl extends HttpService {
    private static final Logger LOG = LogManager.getLogger();

    /**
     * This method is used to updating anything on the server.
     * @param entity it is of StringEntity type.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc...
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPut(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        String resp = null;
        try {
            StringBuffer params = new StringBuffer("");
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }

            if (!params.toString().equals("")) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }

            HttpPut httpPut = new HttpPut(url);
            headerInfo.forEach(httpPut::addHeader);
            httpPut.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPut);
            if (httpResponse.getStatusLine().getStatusCode() != 200 && httpResponse.getStatusLine().getStatusCode() != 204) {
                throw new ApiException(ApiResponseCode.ERROR);
            }

            if (httpResponse.getStatusLine().getStatusCode() != 204)
                resp = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception var15) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException var14) {
            }

        }

        return resp;
    }

    /**
     * This method is used to sending information to the server.
     * @param entity it is of StringEntity type.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc...
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPost(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        String resp = null;
        try {
            StringBuffer params = new StringBuffer("");
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }

            if (!params.toString().equals("")) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }

            HttpPost httpPost = new HttpPost(url);
            headerInfo.forEach(httpPost::addHeader);
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            if (!(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK || httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED || httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT)) {
                throw new ApiException(ApiResponseCode.ERROR);
            }

            if (httpResponse.getStatusLine().getStatusCode() != 204)
                resp = EntityUtils.toString(httpResponse.getEntity());

        } catch (Exception var15) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException var14) {
            }

        }

        return resp;
    }

    /**
     * This method allows the client to remove anything from the server.
     * @param queryParams it is used to access key/value pairs in the query string of the URL.
     * @param url it contains url either appended with some extra info or not.
     * @param headerInfo it consist of some extra information like Cookie,Content-Length,Connection,Accept-Language etc...
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doDelete(HashMap<String, Object> queryParams, String url, HashMap<String, String> headerInfo) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        String resp;
        try {
            StringBuffer params = new StringBuffer("");
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    params.append(k).append("=").append(v).append("&");
                });
            }

            if (!params.toString().equals("")) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1).toString();
                url = url + "?" + queryParamsFinal;
            }

            HttpDelete httpDelete = new HttpDelete(url);
            headerInfo.forEach(httpDelete::addHeader);
            HttpResponse httpResponse = httpClient.execute(httpDelete);
            resp = EntityUtils.toString(httpResponse.getEntity());
            if (httpResponse.getStatusLine().getStatusCode() != 200) {
                LOG.info("Status Code " + httpResponse.getStatusLine().getStatusCode());
                throw new ApiException(ApiResponseCode.ERROR);
            }

        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException ioEx) {
            }

        }
        return resp;
    }
}
