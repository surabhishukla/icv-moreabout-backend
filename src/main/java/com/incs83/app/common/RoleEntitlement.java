/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.common;

import com.incs83.app.security.tokenFactory.Entitlement;
import com.incs83.app.security.tokenFactory.Permission;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class is used to assign resources to users.
 * <br><br>{@link #resource} this is used to decide what resources should be assigned to user.
 * <br><br>{@link #permissions} this is used to decide what permission should be assigned to a user for a particular resource.
 */

public class RoleEntitlement {
    private String resource;
    private List<Permission> permissions;

    public static Entitlement mapToEntitilementV2(String resource, Set<String> permissions) {
        Entitlement entitlement = new Entitlement();
        entitlement.setResource(resource);
        List<String> permissionList = new ArrayList<>();
        permissions.forEach(p -> permissionList.add(p));
        entitlement.setPermissions(permissionList);
        return entitlement;
    }

    public String getResource() {
        return Objects.nonNull(resource) ? resource.trim() : resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions.stream().map(p -> Permission.valueOf(p)).collect(Collectors.toList());
    }

    public Entitlement mapToEntitlement() {
        Entitlement entitlement = new Entitlement();
        entitlement.setPermissions(permissions.stream().map(Permission::getValue).collect(Collectors.toList()));
        entitlement.setResource(resource);
        return entitlement;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "RoleEntitlement{", "}")
                .add("resourceType=" + resource)
                .add("permission=" + permissions)
                .toString();
    }
}
