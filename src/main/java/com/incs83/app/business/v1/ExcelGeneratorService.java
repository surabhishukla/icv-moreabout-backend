/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.business.v1;

import com.incs83.app.common.FileGenerator;
import com.incs83.app.constants.HttpStatus;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.util.v1.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.bson.Document;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static com.incs83.app.constants.ApplicationConstants.EMPTY_STRING;

/**
 * This class is used to generate a file of excel type.
 */
@Service(value = "excel")
public class ExcelGeneratorService implements FileGenerator {

    private static final Logger LOG = LogManager.getLogger();

    /**
     * This method is used to generate Excel file.
     * @param fileName is the name by which excel file will be created.
     * @param data It has data in the form of keys and values.
     * @param columns are the list of strings that can be declared as columns of excel file.
     * @param directory is the location where excel file will be stored.
     * @return finalPath (location of excel file).
     * @throws IOException this method might throw exception
     */
    @Override
    public String generate(String fileName, List<Document> data, List<String> columns, String directory) throws IOException {
        Long startTime = CommonUtils.getCurrentTimeInMillis();
        if (!directory.endsWith("/")) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Directory must end with slash");
        }
        String finalPath = directory + fileName + ".xlsx";
        Path pathdir = Paths.get(directory);
        if (!Files.exists(pathdir)) {
            Files.createDirectory(pathdir);
        }
        SXSSFWorkbook workbook = new SXSSFWorkbook();
        SXSSFSheet sheet = workbook.createSheet("Sheet1");
        int rowCount = 0;
        Integer count = 0;
        if (count == 0) {
            SXSSFRow row = sheet.createRow(rowCount);
            Integer columnCount = 0;
            for (String column : columns) {
                SXSSFCell cell = row.createCell(columnCount);
                cell.setCellValue(column);
                sheet.trackColumnForAutoSizing(columnCount);
                sheet.autoSizeColumn(columnCount);
                columnCount++;
            }
            rowCount++;
            count++;
        }
        try {
            for (Document report : data) {
                if (rowCount <= data.size()) {
                    SXSSFRow row = sheet.createRow(rowCount);
                    Integer columnCount = 0;
                    for (String column : columns) {
                        SXSSFCell cell = row.createCell(columnCount);
                        cell.setCellValue(Objects.nonNull(report.get(column)) ? String.valueOf(report.get(column)) : EMPTY_STRING);
                        columnCount++;
                    }
                    rowCount++;
                } else {
                    break;
                }
            }
            LOG.info("Written To Excel");

            try (FileOutputStream outputStream = new FileOutputStream(finalPath)) {
                workbook.write(outputStream);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                throw e;
            }
        } catch (Exception e) {
            LOG.error("Error in creating excel file ");
            LOG.error(e.getMessage(), e);
            throw e;
        }
        LOG.info("Total Execution time " + ((CommonUtils.getCurrentTimeInMillis() - startTime)) + "milliSeconds");
        return finalPath;
    }
}
