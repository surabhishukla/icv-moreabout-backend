/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.auth.jwt.provider;

import com.incs83.app.business.v2.RouteApiService;
import com.incs83.app.config.JwtConfig;
import com.incs83.app.constants.CommonConstants;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.entities.Role;
import com.incs83.app.entities.User;
import com.incs83.app.handler.exceptions.*;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.handler.response.ResponseUtil;
import com.incs83.app.mt.DataAccessService;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.security.exceptions.AccountSuspendedException;
import com.incs83.app.security.exceptions.LicenseExpiredException;
import com.incs83.app.security.tokenFactory.JwtAuthenticationToken;
import com.incs83.app.security.tokenFactory.RawAccessJwtToken;
import com.incs83.app.security.tokenFactory.UserContext;
import com.incs83.app.util.v1.AuthUtils;
import com.incs83.app.util.v1.CommonUtils;
import com.incs83.app.util.v1.ValidationUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.incs83.app.constants.ApplicationConstants.FORWARD_SLASH;
import static com.incs83.app.constants.CommonConstants.*;

/**
 * This class is used to manage the authentication token generation.
 * <br><br>{@link #authenticate(Authentication)} a method is used to create jwt token using authentication credential.
 */
@Component
@SuppressWarnings("unchecked")
public class JwtAuthenticationProvider implements AuthenticationProvider {
    private static final Logger LOG = LogManager.getLogger();
    private final JwtConfig jwtConfig;
    @Autowired
    ResponseUtil responseUtil;
    @Autowired
    private DataAccessService dataAccessService;

    @Autowired
    private AuthUtils authUtils;

    @Autowired
    public JwtAuthenticationProvider(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        HttpServletRequest request = SqlCurrentTenantIdentifier._servletRequest.get();
        HttpServletResponse response = SqlCurrentTenantIdentifier._servletResponse.get();
        RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();
        Jws<Claims> jwsClaims = null;
        UserContext context;

        try {
            jwsClaims = rawAccessToken.parseClaims(jwtConfig.getTokenSigningKey());
        } catch (Exception e) {
            LOG.error("#### INVALID or COMPROMISED TOKEN #####");
        }
        if (Objects.nonNull(jwsClaims) && !authUtils.isTokenExpired(jwsClaims.getBody().getExpiration())) {
            try {
                if (!authUtils.matchToken(rawAccessToken, jwsClaims.getBody().get("jti", String.class), request.getHeader(X_TENANT_NAME))) {
                    throw new InvalidJwtToken("Authentication failed, Token Mismatch");
                }
            } catch (Exception e) {
                throw new InvalidJwtToken("TOKEN MISMATCH");
            }

            String subject = jwsClaims.getBody().getSubject();
            String id = jwsClaims.getBody().getId();
            String issuer = jwsClaims.getBody().getIssuer();
            String tenant = jwsClaims.getBody().get("tenant", String.class);
            String name = jwsClaims.getBody().get("name", String.class);
            String tenantType = jwsClaims.getBody().get("tenantType", String.class);
            boolean isVerified = jwsClaims.getBody().get("isVerified", Boolean.class);
            boolean internalUser = jwsClaims.getBody().get("internalUser", Boolean.class);

            try {
                if (ValidationUtils.nonNullOrEmpty(jwtConfig.getTokenIssuer()) && !jwtConfig.getTokenIssuer().equals(issuer)) {
                    throw new InvalidJwtToken("Invalid Token Issuer");
                }
            } catch (InvalidJwtToken invalidJwtToken) {
                throw invalidJwtToken;
            } catch (Exception e) {
                LOG.error("======Error while fetching token issuer from DB==========");
            }

            if (!isVerified && !request.getRequestURI().endsWith("/changePassword"))
                throw new InvalidJwtToken("Please change your password after first login.");

            if (Objects.isNull(tenant))
                throw new InvalidJwtToken("Tenant isNull" + tenant);
            String xTenantName = request.getHeader(CommonConstants.X_TENANT_NAME);
            if (Objects.nonNull(xTenantName) && !xTenantName.equals("localhost") && !tenant.equals(xTenantName))
                throw new InvalidJwtToken(tenant + " isEqualTo " + xTenantName + " or localhost");
            CommonUtils.setDBName(tenant);
            User user;
            Role role;
            List<String> projectIds = new ArrayList<>();
            List<String> groupIds = new ArrayList<>();
            try {
                user = (User) dataAccessService.read(User.class, id);
                if (Objects.isNull(user))
                    throw new InvalidJwtToken("Authorization failed" + "User isNull =" + user);
                if (user.getUserRole().isEmpty())
                    throw new InvalidJwtToken("Authorization failed" + "userRole isEmpty =" + user.getUserRole());
                role = user.getUserRole().iterator().next().getRole();
                if (Objects.isNull(role) || Objects.isNull(role.getMapping())) {
                    LOG.error("======Role mapping for user is NULL==============");
                }


            } catch (InvalidJwtToken invalidJwtToken) {
                throw new InvalidJwtToken(invalidJwtToken.getMessage());
            } catch (ValidationException e) {
                throw new InvalidJwtToken(e.getMessage());
            } catch (Exception e) {
                throw new ApiException(ApiResponseCode.ERROR);
            }


            CommonUtils.setDBName(tenant);
            String currentProjectId = null;

            if (Objects.nonNull(request.getHeader(X_PROJECT_ID))) {
                currentProjectId = request.getHeader(X_PROJECT_ID);
            }
            context = UserContext.create(id, subject, tenant, issuer, role.getId(), groupIds, name, isVerified, isLicenseExpired, internalUser, role.getName(), CommonUtils.generateScopePayloadForToken(user), projectIds, currentProjectId, user.isDefault(), tenantType);
            ExecutionContext ec = new ExecutionContext(context);
            ExecutionContext.CONTEXT.set(ec);
            String remarks = "Request Completed Successfully";
        } else {
            throw new InvalidJwtToken("Authorization failed, Token Expired");
        }
        return new JwtAuthenticationToken(context);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }

}
