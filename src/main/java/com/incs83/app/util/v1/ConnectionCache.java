/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import com.incs83.app.business.v2.UtilService;
import com.incs83.app.common.CachePayload;
import com.incs83.app.service.HazelcastService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationServiceException;

import java.io.Serializable;
import java.util.Objects;

import static com.incs83.app.constants.ApplicationConstants.LOGGER_PREFIX;
import static com.incs83.app.constants.CommonConstants.COMMON;
import static com.incs83.app.constants.CommonConstants.DB_CONNECTIONS_CACHE_MAP;
import static com.incs83.app.mt.SqlCurrentTenantIdentifier._tenantName;

/**
 * This class is used to get the various details like tenant name, db name, tenant id, owAuth of login
 * <br><br>{@link #getConnectionDetail(String)} this method is used to fetch the tenant info.
 * <br><br>{@link #getDBName(String)} This method is used to get the db name.
 * <br><br>{@link #getDBName()} This method is used to get the db name.
 * <br><br>{@link #getTenantId()} This method is used to get the tenantId.
 * <br><br>{@link #setTenantId(String)} This method is used to set the value of tenantId.
 * <br><br>{@link #getTenantName()} This method is used to get the tenant name.
 * <br><br>{@link #setTenantName(String)} This method is used to set the value of tenantName.
 * <br><br>{@link #getOwAuth()} This method is used to get the owAuth.
 * <br><br>{@link #setOwAuth(String)} This method is used to set the value of owAuth.
 */
public class ConnectionCache implements Serializable {
    private static final long serialVersionUID = 8647319399229204631L;

    private String tenantId;
    private String tenantName;
    private String schemaName;
    private String owAuth;

    private static final Logger LOG = LogManager.getLogger();

    /**
     * This method is used to fetch the tenant info.
     * @param tenantName it consist the name of tenant
     * @return connectionCache of type {ConnectionCache}.
     */
    public static ConnectionCache getConnectionDetail(String tenantName) {
        LOG.debug(LOGGER_PREFIX+"TenantName inside getConnectionDetail "+tenantName);
        HazelcastService hazelcastService = BeanUtils.getBean(HazelcastService.class);
        CachePayload cachePayload = CachePayload.Creator.getInstance().setTenant(COMMON).setMap(DB_CONNECTIONS_CACHE_MAP).setKey(tenantName.toLowerCase()).build();
        LOG.debug(LOGGER_PREFIX+"  PREPARED CACHE PAYLOAD "+cachePayload);
        ConnectionCache connectionCache = (ConnectionCache) hazelcastService.read(cachePayload);
        if (Objects.nonNull(connectionCache)) {
            return connectionCache;
        } else {
            UtilService utilService = BeanUtils.getBean(UtilService.class);
            utilService.syncConnectionCache();
            connectionCache = (ConnectionCache) hazelcastService.read(cachePayload);
            LOG.debug(LOGGER_PREFIX+" CACHE PAYLOAD FETCHED FROM HAZELCAST -- "+cachePayload);

            if (Objects.isNull(connectionCache)) {
                LOG.error(LOGGER_PREFIX+"Exception Occured while fetching tenant info from cache ");
                throw new AuthenticationServiceException("Invalid Tenant name");
            } else {
                return connectionCache;
            }
        }
    }

    /**
     * This method is used to get the db name.
     * @param tenantName it consist the name of tenant
     * @return the db name.
     */
    public static String getDBName(String tenantName) {
        return getConnectionDetail(tenantName.toLowerCase()).getSchemaName();
    }

    /**
     * This method is used to get the db name.
     * @return the db name.
     */
    public static String getDBName() {
        return getConnectionDetail(_tenantName.get()).getSchemaName();
    }

    /**
     * This method is used to get the tenantId.
     * @return the tenantId.
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * This method is used to set the value of tenantId.
     * @param tenantId it is tenant id
     * @return the tenantId.
     */
    public ConnectionCache setTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    /**
     * This method is used to get the tenantId.
     * @return the tenantId.
     */
    public String getTenantName() {
        return tenantName;
    }

    /**
     * This method is used to set the value of tenantName.
     * @param tenantName it is the name of tenant
     * @return the tenantName.
     */
    public ConnectionCache setTenantName(String tenantName) {
        this.tenantName = tenantName;
        return this;
    }

    /**
     * This method is used to get the schemaName.
     * @return the schemaName.
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * This method is used to set the value of schemaName.
     * @param schemaName it is the name of db
     * @return the schemaName.
     */
    public ConnectionCache setSchemaName(String schemaName) {
        this.schemaName = schemaName;
        return this;
    }

    /**
     * This method is used to get the owAuth.
     * @return the owAuth.
     */
    public String getOwAuth() {
        return owAuth;
    }

    /**
     * This method is used to set the value of owAuth.
     * @param owAuth it consist the owAuth details
     */
    public void setOwAuth(String owAuth) {
        this.owAuth = owAuth;
    }
}
