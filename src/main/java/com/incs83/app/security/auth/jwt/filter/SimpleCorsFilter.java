/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.auth.jwt.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.business.v2.HttpConnectorService;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.handler.exceptions.AuthMethodNotSupportedException;
import com.incs83.app.handler.exceptions.InvalidApiKey;
import com.incs83.app.handler.exceptions.UrlNotFoundException;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.handler.response.ResponseUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCorsFilter implements Filter {

    private static final Logger LOG = LogManager.getLogger();
    @Autowired
    private HttpConnectorService httpConnectorService;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private ResponseUtil responseUtil;

    public SimpleCorsFilter() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, X-Authorization, X-Requested-With, X-TENANT-NAME, isSecure, apiKey, X-PROJECT-ID");
        response.setHeader("Access-Control-Expose-Headers", "X-Authorization");

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else if (request.getRequestURI().contains(ApplicationConstants.HTTP_API_PREFIX)) {
            response.setContentType("application/json");
            try {
                httpConnectorService.callHttpConnectorApi(request, response);
                response.setStatus(HttpServletResponse.SC_OK);
                mapper.writeValue(response.getWriter(), responseUtil.ok(ApiResponseCode.SUCCESS));
            } catch (Exception e) {
                if (e instanceof UrlNotFoundException) {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.NOT_FOUND.value(), e.getMessage()));
                } else if (e instanceof AuthMethodNotSupportedException) {
                    LOG.error("####Auth Method Not Supported Exception -- ", e);
                    response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
                    mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.METHOD_NOT_ALLOWED.value(), "Authentication method not Supported"));
                } else if (e instanceof InvalidApiKey) {
                    LOG.error("####Auth Method Not Supported Exception -- ", e);
                    response.setStatus(HttpStatus.UNAUTHORIZED.value());
                    mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
                }
            }
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
