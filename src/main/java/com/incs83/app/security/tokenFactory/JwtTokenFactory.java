/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.tokenFactory;

import com.incs83.app.config.JwtConfig;
import com.incs83.app.enums.ScopesEnum;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.util.v1.CommonUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

@Component
public class JwtTokenFactory {

    private final JwtConfig jwtConfig;

    @Autowired
    public JwtTokenFactory(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    public AccessJwtToken createAccessJwtToken(UserContext userContext) {
        if (StringUtils.isBlank(userContext.getUsername()))
            throw new IllegalArgumentException("Cannot persist JWT Token without username");

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.setId(userContext.getId());
        claims.put("tenant", userContext.getTenant());
        claims.put("name", userContext.getName());
        claims.put("isVerified", userContext.getVerified());
        claims.put("isLicenseExpired", userContext.getLicenseExpired());
        claims.put("internalUser", userContext.getInternalUser());
        claims.put("role", userContext.getRoleName());
        claims.put("key", CommonUtils.generateUUID());
        claims.put("tenantType", userContext.getTenantType());
        LocalDateTime currentTime = LocalDateTime.now();
        String access_token = Jwts.builder()
                .setClaims(claims)
                .setIssuer(jwtConfig.getTokenIssuer())
                .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                .setExpiration(Date.from(currentTime
                        .plusMinutes(jwtConfig.getTokenExpirationTime())
                        .atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getTokenSigningKey())
                .compact();

        return new AccessJwtToken(access_token, claims);
    }

    public JwtToken createRefreshToken(UserContext userContext) {
        if (StringUtils.isBlank(userContext.getUsername())) {
            throw new IllegalArgumentException("Cannot persist JWT Token without username");
        }

        LocalDateTime currentTime = LocalDateTime.now();

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.put("scopes", Arrays.asList(ScopesEnum.REFRESH_TOKEN.authority()));
        claims.put("tenant", SqlCurrentTenantIdentifier._tenantIdentifierSql.get());

        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer(jwtConfig.getTokenIssuer())
                .setId(userContext.getId())
                .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                .setExpiration(Date.from(currentTime
                        .plusMinutes(jwtConfig.getRefreshTokenExpTime())
                        .atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getTokenSigningKey())
                .compact();

        return new AccessJwtToken(token, claims);
    }
}
