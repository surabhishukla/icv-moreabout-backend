package com.incs83.app.handler.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * This class is used to throw exception when the requested URL is not found.
 * <br><br>{@link #code}  is used to give Api response code when throwing UrlNotFoundException Exception.
 * <br><br>{@link #message}  is used to give Exception message when throwing UrlNotFoundException Exception..
 *
 */
public class UrlNotFoundException extends AuthenticationException {

    private static final long serialVersionUID = -6874215222854343L;

    private int code;
    private String message;

    public UrlNotFoundException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
