/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.common;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.List;

/**
 * This class is a request class for PrepareAndSendEmailService.
 * <br><br>{@link #templateName} name of the template saved in AWS.
 * <br><br>{@link #emailData} content of email.
 */
public class PrepareEmailRequest {

    @NotBlank(message = "Template Name cannot be blank")
    private String templateName;
    private List<EmailData> emailData;

    /**
     * <br><br>{@link #addressTo} email address of recipient.
     * <br><br>{@link #replacementData} data that changes according to the requirement.
     * <br><br>{@link #userMobile} mobile number of recipient.
     */
    public static class EmailData {
        private String addressTo;
        private HashMap<String, String> replacementData;
        private String userMobile;

        public String getAddressTo() {
            return addressTo;
        }

        public void setAddressTo(String addressTo) {
            this.addressTo = addressTo;
        }

        public HashMap<String, String> getReplacementData() {
            return replacementData;
        }

        public void setReplacementData(HashMap<String, String> replacementData) {
            this.replacementData = replacementData;
        }

        public String getUserMobile() {
            return userMobile;
        }

        public void setUserMobile(String userMobile) {
            this.userMobile = userMobile;
        }
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public List<EmailData> getEmailData() {
        return emailData;
    }

    public void setEmailData(List<EmailData> emailData) {
        this.emailData = emailData;
    }

}

