/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.dao;

import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static com.incs83.app.mt.SqlCurrentTenantIdentifier._tenantIdentifierSql;

/**
 * This class is used for performing CREATE,READ,UPDATE,DELETE operation in database.
 * <br><br>{@link #tenant()} This method is used for getting tenant.
 * <br><br>{@link #create(FunctionalCreate)} This method is used for creating or saving data to the database.
 * <br><br>{@link #read(FunctionalRead)} This method is used for fetching data from the database.
 * <br><br>{@link #update(FunctionalUpdate)} This method is used for updating data to the database.
 * <br><br>{@link #delete(FunctionalDelete)} This method is used for deleting data from the database.
 */
@Service
public class PersistenceRepository {

    private static final Logger LOG = LogManager.getLogger();
    @Autowired
    private SessionFactory sessionFactory;

  /*  private SessionFactory sessionFactory;

    public PersistenceRepository(SessionFactory session) {

        this.sessionFactory = session;

    }*/

    /**
     * This method is used for getting tenant.
     * @return the fetched detail of tenant which is {String} type.
     */
    private String tenant() {
        return _tenantIdentifierSql.get();
    }

    /**
     * This method is used for creating or saving data to the database.
     * @param cl it helps in creating connection so that creation or saving of data can be performed in database.
     * @return object which is {Object} type.
     */
    public Object create(FunctionalCreate cl) {
        Transaction transaction = null;
        Session session = null;
        Object object;
        if (SqlCurrentTenantIdentifier._currentSession.get() != null) {
            session = SqlCurrentTenantIdentifier._currentSession.get();
            return cl.persist(session);
        }
        try {
            session = sessionFactory.withOptions().tenantIdentifier(tenant()).openSession();
            transaction = session.beginTransaction();
            object = cl.persist(session);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("### ERROR WHILE PERSISTING DATA TO DB!! ####", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw e;
        } finally {
            HibernateService.closeSilently(session);
        }
        return object;
    }

    /**
     * This method is used for This method is used for fetching data from the database.
     * @param cl it helps in creating connection so that fetching is performed from database.
     * @return object which is {Object} type.
     * @throws Exception this method might throw exception
     */
    public Object read(FunctionalRead cl) throws Exception {
        Session session = null;
        Object object;
        try {
            session = sessionFactory.withOptions().tenantIdentifier(tenant()).openSession();
            object = cl.read(session);
        } catch (Exception e) {
            LOG.error("### ERROR WHILE FETCHING DATA FROM DB: ", e);
            throw new Exception();
        } finally {
            HibernateService.closeSilently(session);
        }
        return object;
    }

    /**
     * This method is used for updating data to the database.
     * @param cl it helps in creating connection so that update operation is performed to database.
     * @throws Exception this method might throw exception
     */
    public void update(FunctionalUpdate cl) throws Exception {
        Transaction transaction = null;
        Session session = null;
        if (SqlCurrentTenantIdentifier._currentSession.get() != null) {
            session = SqlCurrentTenantIdentifier._currentSession.get();
            cl.update(session);
            return;
        }
        try {
            session = sessionFactory.withOptions().tenantIdentifier(tenant()).openSession();
            transaction = session.beginTransaction();
            cl.update(session);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("### ERROR WHILE UPDATING DATA TO DB!! ####", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }

            throw new Exception();
        } finally {
            HibernateService.closeSilently(session);
        }
    }

    /**
     * This method is used for deleting data from the database.
     * @param cl it helps in creating connection so that delete operation is performed to database.
     */
    public void delete(FunctionalDelete cl) {
        Transaction transaction = null;
        Session session = null;
        if (SqlCurrentTenantIdentifier._currentSession.get() != null) {
            session = SqlCurrentTenantIdentifier._currentSession.get();
            cl.delete(session);
            return;
        }
        try {
            session = sessionFactory.withOptions().tenantIdentifier(tenant()).openSession();
            transaction = session.beginTransaction();
            cl.delete(session);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("### ERROR WHILE DELETING DATA FROM DB : ", e);
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw e;
        } finally {
            HibernateService.closeSilently(session);
        }
    }

    @FunctionalInterface
    public interface FunctionalCreate {
        Object persist(Session session);
    }


    @FunctionalInterface
    public interface FunctionalRead {
        Object read(Session session);
    }

    @FunctionalInterface
    public interface FunctionalUpdate {
        void update(Session session);
    }

    @FunctionalInterface
    public interface FunctionalDelete {
        void delete(Session session);
    }

}
