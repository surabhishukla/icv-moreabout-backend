/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

/**
 * This class to establish connection with s3 using aws required keys and region.
 *
 * <br><br>{@link #getAccessKey}  Access Key is used to sign the requests you send to Amazon S3.
 *      Like the Username/Password pair you use to access your AWS Management Console.
 * <br><br>{@link #getSecretKey} is like your password. For your own security.
 * <br><br>{@link #getArea()}  is a physical location around the world where amazon cluster data centers.
 */

@ConfigurationProperties(prefix = "aws")
@Configuration("awsConfig")
@Import(ApplicationConsulService.class)
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class AWSConfig {
    @Autowired
    private ApplicationConsulService applicationConsulService;

    public String getAccessKey() {
        return applicationConsulService.getAwsAccessKey();
    }

    public String getSecretKey() {
        return applicationConsulService.getAwsSecretKey();
    }

    public String getArea() {
        return applicationConsulService.getAwsRegion();
    }
}
