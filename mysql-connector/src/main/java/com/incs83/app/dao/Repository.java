/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.dao;

import com.incs83.app.request.PaginatedRequest;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * This class is used for providing various methods for performing create,update,read,delete operation in database.
 * <br><br>{@link #create(List)} This method is used create or save data in database.It returns list of objects.
 * <br><br>{@link #read(Class)} This method is used for fetching the data from database on the basis of entity name.
 * <br><br>{@link #read(Class, PaginatedRequest)} This method is used for fetching the data from database on the basis of entity name and display the data on the basis of offset and max.
 * <br><br>{@link #read(Class,String)} This method is used for fetching the data from database on the basis of entity name and the given id.
 * <br><br>{@link #read(String,HashMap)} This method is used for fetching the data from database on the basis of hql query and parameters required in the query.
 * <br><br>{@link #readNative(String,HashMap)} This method is used for fetching the data from database on the basis of sql query and parameters required in the query.
 * <br><br>{@link #read(String,HashMap, PaginatedRequest)} This method is used for fetching the data from database on the basis of hql query and parameters required in the query which should be displayed on the basis offset and max.
 * <br><br>{@link #read(String,HashMap, PaginatedRequest)} This method is used for fetching the data from database on the basis of multiple hql queries and multiple parameters required in the queries which should be displayed on the basis offset and max.
 * <br><br>{@link #delete(String,HashMap)} This method is used for deleting the data from database on the basis of hql query and parameters required in the query.
 * <br><br>{@link #deleteUpdateNative(String,HashMap)} This method is used for deleting the data from database on the basis of sql query and parameters required in the query.
 * <br><br>{@link #update(String, HashMap)} This method is used for updating the data in database on the basis of hql query and parameters required in the query.
 * <br><br>{@link #delete(Class, String)} This method is used for deleting the data from database on the basis of entity name and the given id.
 */
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class Repository<A> {

    @Autowired
    private  PersistenceRepository persistenceRepository;

    /**
     * This method is used to create or save data in database.
     * @param a it is class type  object which consist of some data to be saved in database.
     * @return a of {A} type.
     * @throws IOException this method might throw IOException
     */
    public A create(A a) throws IOException {
        return (A) persistenceRepository.create(session -> {
            session.save(a);
            return a;
        });
    }

    /**
     * This method is used to create or save data in database.
     * @param objects it is List type objects which consist of some data to be saved in database.
     * @return objects of {List} type.
     * @throws IOException this method might throw IOException
     */
    public List<A> create(List<A> objects) throws IOException {
        return (List<A>) persistenceRepository.create(session -> {
            for (A object : objects) {
                session.save(object);
            }
            return objects;
        });
    }

    /**
     * This method is used to fetching of data from database.
     * @param clazz it's a class on the basis of which the data is fetched from database.
     * @return it returns {List} in descending order on the basis of its creation date.
     * @throws Exception this method might throw exception
     */
    public List<A> read(Class clazz) throws Exception {
        return (List<A>) persistenceRepository.read(session -> {
            Query query = session.createQuery("from " + clazz.getSimpleName() + " order by createdAt DESC");
            return (List<A>) query.list();
        });
    }

    /**
     * This method is used to fetching of data from database.
     * @param clazz it's a class on the basis of which the data is fetched from database.
     * @param paginatedRequest (offset,max,order,sortBy,tableName)
     *                        offset it consist of 0 as a value.
     *                        max it consist of 10 as a value.
     *                        order it is of order type whose value can be one of these ASC and DSC.
     *                        sortBy it provides condition on which sorting is done.
     *                        tableName it is used for table name.
     * @return it returns {List} in descending order on the basis of its creation date.
     * @throws Exception this method might throw exception
     */
    public List<A> read(Class clazz, PaginatedRequest paginatedRequest) throws Exception {
        return (List<A>) persistenceRepository.read(session -> {
            Query query = session.createQuery("from " + clazz.getSimpleName() + " order by createdAt DESC");
            query.setFirstResult(paginatedRequest.getOffset().intValue()).setMaxResults(paginatedRequest.getMax().intValue());
            Page<A> page=new Page();
            page.setObjects(query.list());
            return (List<A>) query.list();
        });
    }

    /**
     * This method is used to fetching of data from database.
     * @param clazz it's a class on the basis of which the data is fetched from database.
     * @param id it's a id of given class.
     * @return it returns single data of type {A}.
     * @throws Exception this method might throw exception
     */
    public A read(Class clazz, String id) throws Exception {
        return (A) persistenceRepository.read(session -> session.get(clazz, id));
    }

    /**
     * This method is used to fetching of data from database.
     * @param strQuery it's a hql query required for fetching the data from database.
     * @param params these are parameters required in query.
     * @return it returns list of data of type {List} on the basis of the query.
     * @throws Exception this method might throw exception
     */
    public List<A> read(String strQuery, HashMap<String, Object> params) throws Exception {
        return (List<A>) persistenceRepository.read(session -> {
            Query<A> query = session.createQuery(strQuery);
            params.forEach(query::setParameter);
            return query.list();
        });
    }

    /**
     * This method is used to fetching of data from database.
     * @param strQuery it's a sql query required for fetching the data from database.
     * @param params these are parameters required in query.
     * @return it returns list of data of type {List} on the basis of the query.
     * @throws Exception this method might throw exception
     */
    public List<A> readNative(String strQuery, HashMap<String, Object> params) throws Exception {
        return (List<A>) persistenceRepository.read(session -> {
            Query<A> query = session.createNativeQuery(strQuery);
            params.forEach(query::setParameter);
            return query.list();
        });
    }

    /**
     * This method is used to fetching of data from database.
     * @param strQuery it's a hql query required for fetching the data from database.
     * @param params these are parameters required in query.
     * @param paginatedRequest (offset,max,order,sortBy,tableName)
     *                        offset it consist of 0 as a value.
     *                        max it consist of 10 as a value.
     *                        order it is of order type whose value can be one of these ASC and DSC.
     *                        sortBy it provides condition on which sorting is done.
     *                        tableName it is used for table name.
     * @return it returns page of type {Page} on the basis of the query and max value.
     * @throws Exception this method might throw exception
     */
    public Page<A> read(String strQuery, HashMap<String, Object> params, PaginatedRequest paginatedRequest) throws Exception {
        return (Page<A>) persistenceRepository.read(session -> {
            Query<A> query = session.createQuery(strQuery);
            params.forEach(query::setParameter);
            query.setFirstResult(paginatedRequest.getOffset()).setMaxResults(paginatedRequest.getMax());
            Page<A> page=new Page();
            page.setObjects(query.list());
            page.setTotalCount(page.getObjects().size());
            page.setMax(paginatedRequest.getMax());
            return page;
        });
    }

    /**
     * This method is used to fetching of data from database.
     * @param queries these are hql queries required for fetching the data from database.
     * @param params these are parameters required in query.
     * @param paginatedRequest (offset,max,order,sortBy,tableName)
     *                        offset it consist of 0 as a value.
     *                        max it consist of 10 as a value.
     *                        order it is of order type whose value can be one of these ASC and DSC.
     *                        sortBy it provides condition on which sorting is done.
     *                        tableName it is used for table name.
     * @return it returns page of type {Page} on the basis of the queries and max value.
     * @throws Exception this method might throw exception
     */
    public A read(HashMap<String, Object> params, PaginatedRequest paginatedRequest, String... queries) throws Exception {
        return (A) this.persistenceRepository.read((session) -> {
            StringBuilder finalQuery = (new StringBuilder(queries[0])).append(" ORDER BY ").append(paginatedRequest.getSortBy()).append(" ").append(paginatedRequest.getOrder());
            Query result = session.createQuery(finalQuery.toString());
            params.forEach(result::setParameter);
            result.setFirstResult(paginatedRequest.getOffset().intValue()).setMaxResults(paginatedRequest.getMax().intValue());
            Page<A> page = new Page();
            page.setObjects(result.list());
            result = session.createQuery(queries[1]);
            params.forEach(result::setParameter);
            page.setTotalCount(Integer.valueOf(result.getSingleResult().toString()));
            return page;
        });
    }

    /**
     * This method is used to delete data from database.
     * @param strQuery it's a hql query required for fetching the data from database which is going to be delete.
     * @param params these are parameters required in query.
     */
    public void delete(String strQuery, HashMap<String, Object> params) {
        persistenceRepository.delete(session -> {
            Query<A> query = session.createQuery(strQuery);
            params.forEach(query::setParameter);
            query.executeUpdate();
        });
    }

    /**
     * This method is used to delete data from database.
     * @param strQuery it's a sql query required for fetching the data from database which is going to be delete.
     * @param params these are parameters required in query.
     */
    public void deleteUpdateNative(String strQuery, HashMap<String, Object> params) {
        persistenceRepository.delete(session -> {
            Query<A> query = session.createNativeQuery(strQuery);
            params.forEach(query::setParameter);
            query.executeUpdate();
        });
    }

    /**
     * This method is used to update the data in database.
     * @param a it is class type  object which consist of some data to be updated in database.
     * @return a of type {A}
     * @throws Exception this method might throw exception
     */
    public A update(A a) throws Exception {
        persistenceRepository.update(session -> session.saveOrUpdate(a));
        return a;
    }

    /**
     * This method is used to update the data in database.
     * @param queryStr it's a hql query required for updating the data in database.
     * @param params these are parameters required in query.
     * @throws Exception this method might throw exception
     */
    public void update(String queryStr, HashMap<String, Object> params) throws Exception {
        persistenceRepository.update(session -> {
            Query query = session.createQuery(queryStr);
            params.forEach(query::setParameter);
            query.executeUpdate();
        });
    }

    /**
     * This method is used to delete data from database.
     * @param clazz it's a class on the basis of which the data is fetched from database which is going to be delete.
     * @param id it's a id of given class.
     * @throws Exception this method might throw exception
     */
    public void delete(Class clazz, String id) throws Exception {
        persistenceRepository.delete(session -> {
            A a = (A) session.get(clazz, id);
            if (a != null) {
                session.delete(a);
            } else {
                }
        });
    }

    /**
     * This method is used to update the data in database.
     * @param a it is class type  object which consist of some data to be updated in database.
     * @throws Exception this method might throw exception
     */
    public void merge(A a) throws Exception {
        persistenceRepository.update(session -> session.merge(a));
    }
}
