/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.request;

import com.incs83.app.sql.Order;

/**
 * This class is used for pagination.
 * It's consist of various getters and setters method for getting and setting the values.
 */
public class PaginatedRequest {
    private Integer offset = Integer.valueOf(0);
    private Integer max = Integer.valueOf(10);
    private Order order;
    private String sortBy;
    private String tableName;

    public PaginatedRequest() {
        this.order = Order.ASC;
        this.sortBy = "id";
    }

    public Integer getOffset() {
        return this.offset;
    }

    /**
     * @param offset it consist of 0 as a value in it.
     *               it's the minimum number required in the pagination.
     */
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getMax() {
        return this.max;
    }

    /**
     * @param max it consist of 10 as a value in it.
     *               it's the maximum number required in the pagination.
     */
    public void setMax(Integer max) {
        this.max = max;
    }

    public Order getOrder() {
        return this.order;
    }

    /**
     * @param order it is of Order type whose value can be one of these ASC and DSC.
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    public String getSortBy() {
        return this.sortBy;
    }

    /**
     * @param sortBy value of sortBy is to be set here.
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getTableName() {
        return this.tableName;
    }

    /**
     * @param tableName value of tableName is to be set here.
     * @return this
     */
    public PaginatedRequest setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String toString() {
        return "PaginatedRequest{offset=" + this.offset + ", max=" + this.max + ", order=" + this.order + ", sortBy='" + this.sortBy + '\'' + ", tableName='" + this.tableName + '\'' + '}';
    }
}
