/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.mt;

import com.incs83.app.constants.MySQLConstants;
import com.incs83.app.dao.HibernateService;
import com.mchange.v2.c3p0.DriverManagerDataSource;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class is used for managing connections from an underlying DataSource.
 * <br><br>{@link #ConnectionProviderService(String)} This method is used for setting up  the database details .
 * <br><br>{@link #getDataSource()} This method is used for gathering all of the technical information needed to access the data such as driver name, network address etc...
 * <br><br>{@link #getConnection()} This method creates a Connection object, which is used to create SQL statements, send them to the database, and process the results .
 * <br><br>{@link #closeConnection(Connection)} This method is simply close the connection to the server as defined in the connection string .
 * <br><br>{@link #supportsAggressiveRelease()} It checks that does this connection provider support aggressive release of JDBC connections and re-acquisition of those connections (if need be) later.
 * <br><br>{@link #isUnwrappableAs(Class)} It checks that can this wrapped service be unwrapped as the indicated type.
 * <br><br>{@link #unwrap(Class)} This method is used for unproxy the service proxy.
 */
public final class ConnectionProviderService implements ConnectionProvider {


    private static final long serialVersionUID = -5698996471658672233L;
    private final DriverManagerDataSource basicDataSource = new DriverManagerDataSource();


    public ConnectionProviderService() {
    }

    /**
     * This method is used for setting up  the database details.
     * @param database it consist of tenant related information
     */
    public ConnectionProviderService(String database) {
        String jdbcURL = HibernateService.multiTenantProperties.getProperty(MySQLConstants.MT_JDBC_URL);
        jdbcURL = jdbcURL.replace(MySQLConstants.DATABASE, database);
        this.basicDataSource.setJdbcUrl(jdbcURL);
        this.basicDataSource.setUser(HibernateService.multiTenantProperties.get(MySQLConstants.DB_USERNAME).toString());
        this.basicDataSource.setPassword(HibernateService.multiTenantProperties.get(MySQLConstants.DB_PASSWORD).toString());

    }

    /**
     * This method is used for gathering all of the technical information needed to access the data such as driver name, network address etc...
     * @return basicDataSource of {DataSource} type.
     */
    public DataSource getDataSource() {
        return this.basicDataSource;
    }

    /**
     * This method creates a Connection object, which is used to create SQL statements, send them to the database, and process the results .
     * @return basicDataSource.getConnection() of {Connection} type.
     */
    @Override
    public Connection getConnection() throws SQLException {
        return this.basicDataSource.getConnection();
    }

    /**
     * This method is simply close the connection to the server as defined in the connection string .
     * @param connection it's object Connection class help in closing the connection.
     */
    @Override
    public void closeConnection(Connection connection) throws SQLException {
        connection.close();
    }

    /**
     * It checks that does this connection provider support aggressive release of JDBC connections and re-acquisition of those connections (if need be) later.
     * @return false of {boolean} type.
     */
    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    /**
     * It checks that, can this wrapped service be unwrapped as the indicated type
     * @param aClass the type to check.
     * @return false of {boolean} type
     */
    @SuppressWarnings("rawtypes")
    @Override
    public boolean isUnwrappableAs(Class aClass) {
        return false;
    }

    /**
     * This method is used for unproxy the service proxy.
     * @param aClass the unwrapped reference.
     * @return null.
     */
    @Override
    public <T> T unwrap(Class<T> aClass) {
        return null;
    }
}
