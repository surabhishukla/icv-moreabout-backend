/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import com.incs83.app.config.ApplicationConsulService;
import com.incs83.app.dao.MySqlConsulService;
import com.incs83.app.entities.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

/**
 * This is the utility class for SQl.
 * This class is used for doing various Sql operation such as creating,deleting,and dropping account.
 * <br><br>{@link #createAccountFromSQL(Connection,String, Account,String)} this method is used to create a database for particular account in the db.
 * <br><br>{@link #dropAccountFromSQL(Connection,String)} this method is used to drop the database from db created for particular account.
 * <br><br>{@link #deleteUser(Connection,String)} this method is used to delete the user.
 */
@Component
public final class SQLUtils {
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private MySqlConsulService mySqlConsulService;
    @Autowired
    private ApplicationConsulService applicationConsulService;


    /**
     * This method is used to create a database for particular account in the db.
     * @param connection it is the object of connection
     * @param mobile it is of String type
     * @param account it is of Account type
     * @param fileName it is of String type
     * @return true or false depending on the condition.
     * @throws Exception this method might throw exception
     */
    public boolean createAccountFromSQL(Connection connection, String mobile, Account account, String fileName) throws Exception {
        LOG.info("  CREATING SCHEMA ==> " + account.getSchemaName());
        try (InputStream fs = getClass().getClassLoader().getResourceAsStream(fileName);
             BufferedReader br = new BufferedReader(new InputStreamReader(fs))) {
            connection.createStatement().execute("CREATE DATABASE " + account.getSchemaName());
            connection.setCatalog(account.getSchemaName());
            StringBuilder sbOut = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sbOut.append(line);
            }
            String sql = sbOut.toString();
            String[] commands = sql.split(";");
            for (String command : commands) {
                if (command.startsWith("--")) {
                    LOG.info(command);
                } else {
                    LOG.info(command.replaceAll("__TENANT_NAME__", account.getTenantName()));
                    command = command.replaceAll("__TENANT_NAME__", account.getTenantName());
                    command = command.replaceAll("__MOBILE__", mobile);
                    command = command.replaceAll("__BASE_URL__", applicationConsulService.getBaseUrl().replaceAll("__TENANT_NAME__", account.getTenantName()));
                    String dns = "";
                    if (Objects.nonNull(applicationConsulService.getBaseUrl().split("\\.")[1])) {
                        dns = applicationConsulService.getBaseUrl().split("\\.")[1];
                    }
                    command = command.replaceAll("__DNS__", dns);
                    connection.createStatement().execute(command);
                    LOG.info("******************************************************************************************************************************");
                }
            }
            return true;
        } catch (Exception e) {

            LOG.error("exception occurred while creating account " + e);
            dropAccountFromSQL(connection, account.getSchemaName());
        }
        return false;
    }

    /**
     * This method is used to drop the database from db created for particular account.
     * @param connection it is of Connection type
     * @param schemaName it is of String type
     * @return true or false depending on the condition.
     */
    public boolean dropAccountFromSQL(Connection connection, String schemaName) {
        LOG.info("DROPPING SCHEMA ==> " + schemaName);
        try {
            connection.createStatement().execute("DROP DATABASE IF EXISTS " + schemaName);
            deleteUser(connection, schemaName);
        } catch (SQLException e) {
            LOG.error(String.format("[[%s]] -- exception occurred while dropping account ", e.getMessage(), e));
            return false;
        }
        return true;
    }

    /**
     * This method is used in deleting operation of account.
     * @param connection it is the object of connection
     * @param schemaName it consist the name of schema
     */
    public void deleteUser(Connection connection, String schemaName) {
        try {
            String host = (mySqlConsulService.getHibernateConnectionUrl().split("//")[1]).split("\\:")[0];
            String arr[] = {"r", "rw"};
            for (String role : arr) {
                String username = schemaName + "_" + role;
                String query = "DROP USER " + "'" + username + "'" + "@" + "'" + host + "'";
                LOG.info("  DROPPING USERS ==> ");
                connection.createStatement().execute(query);
            }
        } catch (Exception e) {
            LOG.error("exception occurred while deleting account " + e);
        }
    }
}
