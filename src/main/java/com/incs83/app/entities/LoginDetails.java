/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.entities;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "login_details")
public class LoginDetails extends ParentEntity implements Serializable {

    private static final long serialVersionUID = 8043939240736104198L;

    @Id
    @Column(name = "user_id")
    private String userId;

    @Lob
    @Column(name = "token")
    private String token;

    public String getUserId() {
        return userId;
    }

    /**
     * @param userId is Id of user to be set.
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    /**
     * @param token is jwt token to be set.
     */
    public void setToken(String token) {
        this.token = token;
    }

    public static LoginDetails generateFrom(String userContextId, String accessToken) {
        LoginDetails loginDetails = new LoginDetails();
        loginDetails.setUserId(userContextId);
        loginDetails.setCreatedAt(new Date());
        return updateFrom(loginDetails, accessToken);
    }

    public static LoginDetails updateFrom(LoginDetails loginDetails, String accessToken) {
        loginDetails.setToken(accessToken);
        loginDetails.setUpdatedAt(new Date());
        return loginDetails;
    }
}
