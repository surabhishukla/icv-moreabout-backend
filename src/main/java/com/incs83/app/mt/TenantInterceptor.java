/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.mt;

import com.incs83.app.annotation.CheckProject;
import com.incs83.app.annotation.PreHandle;
import com.incs83.app.context.ExecutionContext;
import com.incs83.app.context.RequestContext;
import com.incs83.app.handler.exceptions.AuthEntityNotAllowedException;
import com.incs83.app.handler.exceptions.AuthMethodNotSupportedException;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.security.tokenFactory.Authority;
import com.incs83.app.security.tokenFactory.Entitlement;
import com.incs83.app.security.tokenFactory.Permission;
import com.incs83.app.security.tokenFactory.UserContext;
import com.incs83.app.util.v1.ConnectionCache;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.incs83.app.constants.CommonConstants.*;

/**
 * This class is used to perform operation under two situations,(i) before sending the request to the controller (ii) before sending the response to the client.
 * <br><br>{@link #preHandle(HttpServletRequest, HttpServletResponse, Object)} This method is used before a request is handled by a request handler.
 * <br><br>{@link #postHandle(HttpServletRequest, HttpServletResponse, Object, ModelAndView)} This method is used after a request is handled by a request handler.
 * <br><br>{@link #checkResourceEntitlementForRole(HttpServletRequest, Class[], String)} This method is used to check whether the user has permission to access the resources.
 * <br><br>{@link #getRequiredPermission(String)} This method is used to get the permission on the basis of requested method.
 * <br><br>{@link #checkResourceEntitlementForRole(HttpServletRequest, String, String)} This method is used to check whether the user has permission to access the resources.
 * <br><br>{@link #projectAccess(HttpServletRequest)} This method is used to check the availability of project id, if project id will not available then it throws exception RESOURCE PERMISSION DENIED
 * <br><br>{@link #logAccess(HttpServletRequest)} This method is used for logging purpose it fetch the projectName,tenantName,userName from the request and set it to the ThreadContext with the help of put method.
 */
@Component
public final class TenantInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private DataAccessService dataAccessService;

    private static final Logger LOG = LogManager.getLogger();
    private static final String[] AUTH_WHITELIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",

    };

    /**
     * This method is used before a request is handled by a request handler.
     * @param request it is the current HTTP request.
     * @param response it is the current HTTP response.
     * @param handler chosen handler to execute, for type and/or instance evaluation
     * @throws AuthenticationServiceException Exception
     * @return true if the execution chain should proceed with the next interceptor or the handler itself. Else, DispatcherServlet assumes that this interceptor has already dealt with the response itself.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws AuthenticationServiceException, Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod method = (HandlerMethod) handler;
            PreHandle preHandleAnnotation = method.getMethodAnnotation(PreHandle.class);
            CheckProject checkProjectHandle = method.getMethodAnnotation(CheckProject.class);
            if (Objects.nonNull(preHandleAnnotation)) {
                checkResourceEntitlementForRole(request, preHandleAnnotation.resourceType().getSimpleName(), preHandleAnnotation.requestMethod().name()); // Pass the Endpoint and Method Info
                if (preHandleAnnotation.resourceTypes().length > 0) {
                    checkResourceEntitlementForRole(request, preHandleAnnotation.resourceTypes(), preHandleAnnotation.requestMethod().name()); // Pass the Endpoint and Method Info
                }
                if (Objects.nonNull(checkProjectHandle)) {
                    projectAccess(request);
                }

            }
            if (ValidationUtils.nonNullOrEmpty(request.getHeader("X_TENANT_NAME"))) {
                logAccess(request);
            }
            if (ValidationUtils.nonNullOrEmpty(request.getHeader("X_TENANT_NAME"))) {
                logAccess(request);
            }
        }

        return super.preHandle(request, response, handler);
    }

    /**
     * This method is used after a request is handled by a request handler.
     * @param request it is the current HTTP request.
     * @param response it is the current HTTP response.
     * @param handler the handler that started asynchronous execution, for type and/or instance examination
     * @param modelAndView the ModelAndView that the handler returned (can also be null)
     * @throws Exception this method might throw exception
     */
    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        SqlCurrentTenantIdentifier._tenantIdentifierSql.remove();
        SqlCurrentTenantIdentifier._tenantName.remove();
        RequestContext._dynamicApplicationId.remove();
        RequestContext._currentProjectId.remove();
        RequestContext._fromDemoTour.remove();
        ThreadContext.clearMap();
        ThreadContext.clearAll();
    }

    /**
     * This method is used to check whether the user has permission to access the resources.
     * @param request it is the current HTTP request.
     * @param resources it is the resources or we can say it is the module.
     * @param requestMethod it gives the information about the type of method
     * @throws AuthenticationServiceException
     */
    private synchronized void checkResourceEntitlementForRole(HttpServletRequest request, Class[] resources, String requestMethod) throws AuthenticationServiceException {
        if (ExecutionContext.CONTEXT.get() == null) {
            return;
        }

        UserContext uc = ExecutionContext.CONTEXT.get().getUsercontext();
        List<Authority> authorities = uc.getAuthorities();
        List<Entitlement> entitlements = new ArrayList<>();

        for (Class resource : resources) {
            authorities.forEach(authority ->
                    entitlements.addAll(authority.getEntitlements().stream().filter(p ->
                            p.getResource().equals("ALL") || p.getResource().equalsIgnoreCase(resource.getSimpleName()))
                            .collect(Collectors.toList())));
        }

        if (entitlements.isEmpty()) {
            LOG.error("=========Entitlements Are Empty RESOURCE_NOT_FOUND=============");
            throw new AuthEntityNotAllowedException(ApiResponseCode.RESOURCE_NOT_ALLOWED);
        }

        Permission requiredPermission = getRequiredPermission(requestMethod);
        List<String> permissions = new ArrayList<>();
        entitlements.forEach(entitlement ->
                permissions.addAll(entitlement.getPermissions().stream().filter(p ->
                        p.equals("ALL") || p.equalsIgnoreCase(requiredPermission.getValue()))
                        .collect(Collectors.toList())));

        String permission = permissions.stream().filter(p -> p.equals("ALL") || p.equalsIgnoreCase(requiredPermission.getValue())).findFirst().orElse(null);
        if (Objects.isNull(permission)) {
            throw new AuthMethodNotSupportedException(ApiResponseCode.RESOURCE_PERMISSION_DENIED);
        }
    }

    /**
     * This method is used to get the permission on the basis of requested method.
     * @param requestMethod it gives the information about the type of method
     * @return permission {Permission}
     */
    private Permission getRequiredPermission(String requestMethod) {
        Permission permission = null;
        switch (requestMethod) {
            case "GET":
                permission = Permission.READ;
                break;
            case "HEAD":
                break;
            case "POST":
                permission = Permission.CREATE;
                break;
            case "PUT":
                permission = Permission.UPDATE;
                break;
            case "PATCH":
                permission = Permission.UPDATE;
                break;
            case "DELETE":
                permission = Permission.DELETE;
                break;
            case "OPTIONS":
                break;
            case "TRACE":
                break;
        }
        return permission;
    }

    /**
     * This method is used to check whether the user has permission to access the resources.
     * @param request it is the current HTTP request.
     * @param resource it is the resource or we can say it is the module.
     * @param requestMethod it gives the information about the type of method
     * @throws AuthenticationServiceException
     */
    private synchronized void checkResourceEntitlementForRole(HttpServletRequest request, String resource, String requestMethod) {
        if (ExecutionContext.CONTEXT.get() == null) {
            return;
        }
        UserContext uc = ExecutionContext.CONTEXT.get().getUsercontext();
        if (!uc.getInternalUser() && request.getRequestURI().endsWith("/users/changePassword"))
            throw new AuthEntityNotAllowedException(ApiResponseCode.RESOURCE_NOT_ALLOWED);

        List<Authority> authorities = uc.getAuthorities();
        List<Entitlement> entitlements = new ArrayList<>();
        authorities.forEach(authority ->
                entitlements.addAll(authority.getEntitlements().stream().filter(p ->
                        p.getResource().equals("ALL") || p.getResource().equalsIgnoreCase(resource))
                        .collect(Collectors.toList())));

        if (entitlements.isEmpty()) {
            LOG.error("Entitlements Not Found");
            throw new AuthEntityNotAllowedException(ApiResponseCode.RESOURCE_NOT_ALLOWED);
        }

        Permission requiredPermission = getRequiredPermission(requestMethod);
        List<String> permissions = new ArrayList<>();
        entitlements.forEach(entitlement ->
                permissions.addAll(entitlement.getPermissions().stream().filter(p ->
                        p.equals("ALL") || p.equalsIgnoreCase(requiredPermission.getValue()))
                        .collect(Collectors.toList())));

        String permission = permissions.stream().filter(p -> p.equals("ALL") || p.equalsIgnoreCase(requiredPermission.getValue())).findFirst().orElse(null);
        if (Objects.isNull(permission)) {
            throw new AuthMethodNotSupportedException(ApiResponseCode.RESOURCE_PERMISSION_DENIED);
        }
    }

    /**
     * This method is used to check the availability of project id, if project id will not available then it throws exception RESOURCE PERMISSION DENIED
     * @param httpServletRequest it is the current HTTP request.
     * @throws Exception
     */
    private synchronized void projectAccess(HttpServletRequest httpServletRequest) throws Exception {
        UserContext userContext = ExecutionContext.CONTEXT.get().getUsercontext();
        LOG.debug("PROJECT IDS IN USER CONTEXT ===>" + userContext.getProjectIds());
        LOG.debug("PROJECT ID FROM HEADER ===>" + httpServletRequest.getHeader(X_PROJECT_ID));
        if (Objects.isNull(httpServletRequest.getHeader(X_PROJECT_ID))) {
            throw new AuthMethodNotSupportedException(ApiResponseCode.RESOURCE_PERMISSION_DENIED);
        }
        if (!(userContext.getProjectIds().contains(httpServletRequest.getHeader(X_PROJECT_ID)))) {
            throw new AuthMethodNotSupportedException(ApiResponseCode.RESOURCE_PERMISSION_DENIED);
        }
    }

    /**
     * This method is used for logging purpose it fetch the projectName,tenantName,userName from the request and set it to the ThreadContext with the help of put method.
     * @param httpServletRequest it is the current HTTP request.
     * @throws Exception
     */
    private synchronized void logAccess(HttpServletRequest httpServletRequest) throws Exception {
        try {
            String projectName = NONE;
            String tenantName = httpServletRequest.getHeader("X_TENANT_NAME");
            String userName = ExecutionContext.get().getUsercontext().getName();
            HashMap<String, Object> query = new HashMap<>();
            query.put("projectId", httpServletRequest.getHeader("X-PROJECT-ID"));
            if (SUPER_ADMIN_CONTEXT.equals(tenantName)) {
                SqlCurrentTenantIdentifier._tenantIdentifierSql.set(M83_MASTER_DB);
            } else {
                SqlCurrentTenantIdentifier._tenantIdentifierSql.set(ConnectionCache.getDBName(tenantName));
            }
            List<String> projectNames = (List<String>) dataAccessService.readNative("select p.project_name from project p where id=:projectId", query);
            if (Objects.nonNull(projectNames) && !projectNames.isEmpty())
                projectName = projectNames.get(0);
            ThreadContext.put("projectName", projectName);
            ThreadContext.put("tenantName", tenantName);
            ThreadContext.put("userName", userName);
        }catch (Exception e){
        }
    }
}
