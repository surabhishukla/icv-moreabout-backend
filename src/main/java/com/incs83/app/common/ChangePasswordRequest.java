/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.common;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * This request class is used to change password.
 * <br><br>{@link #oldPassword} Old password is the current password of the user which user intents to change.
 * <br><br>{@link #newPassword} New password is the replacement of old password.
 */
public class ChangePasswordRequest {

    @NotNull
    @NotBlank
    @Size(min = 1, message = "Cannot be empty")
    private String oldPassword;
    @NotNull
    @NotBlank
    @Size(min = 1, message = "Cannot be empty")
    private String newPassword;

    public String getOldPassword() {
        return Objects.nonNull(oldPassword) ? oldPassword.trim() : oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return Objects.nonNull(newPassword) ? newPassword.trim() : newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
