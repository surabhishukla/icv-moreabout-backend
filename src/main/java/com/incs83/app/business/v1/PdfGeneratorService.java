/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.business.v1;

import com.incs83.app.common.FileGenerator;
import com.incs83.app.constants.HttpStatus;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.util.v1.CommonUtils;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static com.incs83.app.constants.ApplicationConstants.EMPTY_STRING;

/**
 * This class is used to generate a file of pdf type.
 */
@Service(value = "pdf")
public class PdfGeneratorService implements FileGenerator {

    private static final Logger LOG = LogManager.getLogger();

    /**
     * This method is used to generate PDF file.
     * @param fileName is the name by which pdf file will be created.
     * @param data It has data in the form of keys and values.
     * @param columns are the list of strings that can be declared as columns of pdf file.
     * @param directory is the location where pdf file will be stored.
     * @return finalPath (location of pdf file).
     * @throws IOException this method might throw exception
     */
    @Override
    public String generate(String fileName, List<Document> data, List<String> columns, String directory) throws IOException, DocumentException {
        Long startTime = CommonUtils.getCurrentTimeInMillis();
        if (!directory.endsWith("/")) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Directory must end with slash");
        }
        String finalPath = directory + fileName + ".pdf";
        Path pathdir = Paths.get(directory);
        if (!Files.exists(pathdir)) {
            Files.createDirectory(pathdir);
        }
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        try {
            LOG.debug("Generating Pdf");
            PdfWriter.getInstance(document, new FileOutputStream(finalPath));
            document.open();
            Integer count = 0;
            PdfPTable table = new PdfPTable(columns.size());
            if (count++ == 0) {
                for (String column : columns) {
                    PdfPCell c1 = new PdfPCell(new Phrase(column));
                    c1.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                    table.addCell(c1);
                }
                table.setHeaderRows(1);
            }
            if (data.size() != 0) {
                for (Document report : data) {
                    for (String column : columns) {
                        table.addCell(Objects.nonNull(report.get(column)) ? String.valueOf(report.get(column)) : EMPTY_STRING);
                    }
                }
            } else {
                for (String column : columns) {
                    table.addCell(EMPTY_STRING);
                }

            }
            document.add(table);
            document.addCreationDate();
            document.addCreator("IoT83");
            LOG.info("PDF Generated Successfully");
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw e;
        } finally {
            document.close();
        }
        LOG.info("Total Execution time " + ((CommonUtils.getCurrentTimeInMillis() - startTime)) + "milliSeconds");
        return finalPath;
    }
}
