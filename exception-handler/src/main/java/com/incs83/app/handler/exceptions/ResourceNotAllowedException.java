package com.incs83.app.handler.exceptions;

import com.incs83.app.handler.response.ResponseCode;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;

/**
 * This class is used to throw exception when signed in authority aims to access resource which was not assigned to it.
 * <br><br>{@link #message}  is used to give Exception message when throwing ResourceNotAllowedException Exception..
 *
 */
public class ResourceNotAllowedException extends AuthenticationException {

    private String message;

    public ResourceNotAllowedException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
