/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.security.tokenFactory.Entitlement;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity()
@Table(name = "role")
public class Role extends ParentEntity implements Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Id
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "icon")
    private String icon;

    @Lob
    @JsonIgnore
    @Column(name = "role_mapping")
    private String mapping;

    @Transient
    private List<Entitlement> roleEntitlement;

    @Column(name = "type")
    private String type;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "role_entity")
    @Column(name = "entity")
    private Set<String> entity;
    @Column(name = "access_type")
    private String accessType;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getType() {
        return type;
    }

    public Role setType(String type) {
        this.type = type;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    public List<Entitlement> getRoleEntitlement() {
        return roleEntitlement;
    }

    public void setRoleEntitlement(List<Entitlement> roleEntitlement) {
        this.roleEntitlement = roleEntitlement;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getEntity() {
        return entity;
    }

    public void setEntity(Set<String> entity) {
        this.entity = entity;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
