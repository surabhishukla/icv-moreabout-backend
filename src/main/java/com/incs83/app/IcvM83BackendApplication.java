package com.incs83.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcvM83BackendApplication {

    public static void main(String[] args) {
        System.out.println("hello");
        SpringApplication.run(IcvM83BackendApplication.class, args);
    }

}
