/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.security.tokenFactory.UserContext;
import com.incs83.app.util.v1.DateUtils;
import com.incs83.app.util.v1.SecurityUtils;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class ParentEntity implements Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Column(name = "created_by", nullable = false)
    @JsonIgnore
    private String createdBy;

    @Column(name = "updated_by")
    @JsonIgnore
    private String updatedBy;

    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @Column(name = "updated_at")
    @JsonIgnore
    private Date updatedAt;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Long getCreatedAt() {
        return createdAt.getTime();
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt.getTime();
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @PrePersist
    void setCreateEntityFields() {
        this.setCreatedAt(DateUtils.getCurrentDate());
        this.setUpdatedAt(DateUtils.getCurrentDate());
        UserContext userContext = SecurityUtils.getAuthenticatedUser();
        this.setCreatedBy(userContext != null ? userContext.getId() : "SYSTEM");
        this.setUpdatedBy(userContext != null ? userContext.getId() : "SYSTEM");
    }

    @PreUpdate
    void setUpdateEntityFields() {
        this.setUpdatedAt(DateUtils.getCurrentDate());
        UserContext userContext = SecurityUtils.getAuthenticatedUser();
        this.setUpdatedBy(userContext != null ? userContext.getId() : "SYSTEM");
    }

}