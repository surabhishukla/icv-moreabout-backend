/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.entities.LoginDetails;
import com.incs83.app.handler.exceptions.ApiException;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.handler.response.ResponseUtil;
import com.incs83.app.mt.DataAccessService;
import com.incs83.app.security.tokenFactory.JwtToken;
import com.incs83.app.security.tokenFactory.JwtTokenFactory;
import com.incs83.app.security.tokenFactory.UserContext;
import com.incs83.app.util.v2.AuthUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * This class is used to manage the authentication process on our portal.
 * <br><br>{@link #onAuthenticationSuccess(HttpServletRequest, HttpServletResponse, Authentication)} a method is used to play with Http request and response required for our portal and save essential data of the user while user login.
 * <br><br>{@link #clearAuthenticationAttributes(HttpServletRequest)} the method is used to clear http request sessions.
 */

@Component
public class AjaxAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger logger = LogManager.getLogger();
    private final ObjectMapper mapper;
    private final JwtTokenFactory tokenFactory;
    @Autowired
    private DataAccessService dataAccessService;
    @Autowired
    private ResponseUtil responseUtil;
    @Autowired
    private AuthUtils authUtils;

    @Autowired
    public AjaxAwareAuthenticationSuccessHandler(final ObjectMapper mapper, final JwtTokenFactory tokenFactory) {
        this.mapper = mapper;
        this.tokenFactory = tokenFactory;
    }

    /**
     * This method is used to play with Http request and response required for our portal and save essential data of the user while user login.
     * @param request the request during which the authentication attempt occurred..
     * @param response it is the response.
     * @param authentication the exception which was thrown to reject the authentication request.
     * @throws IOException ServletException
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        UserContext userContext = (UserContext) authentication.getPrincipal();
        logger.info("######## login success ######### " + userContext.getUsername());
        Map<String, Object> tokenMap = new HashMap<String, Object>();
        try {
            JwtToken accessToken = null;
            LoginDetails loginData = (LoginDetails) dataAccessService.findById(LoginDetails.class, userContext.getId());
            if (Objects.isNull(loginData)) {
                accessToken = tokenFactory.createAccessJwtToken(userContext);
                tokenMap.put("accessToken", accessToken.getToken());
                dataAccessService.create(LoginDetails.class, LoginDetails.generateFrom(userContext.getId(), accessToken.getToken()));
            } else if (Objects.nonNull(loginData)) {
                accessToken = tokenFactory.createAccessJwtToken(userContext);
                tokenMap.put("accessToken", accessToken.getToken());
                dataAccessService.update(LoginDetails.class, LoginDetails.updateFrom(loginData, accessToken.getToken()));
            }
            tokenMap.put("isVerified", userContext.getVerified());
            tokenMap.put("isLicenseExpired", userContext.getLicenseExpired());
        } catch (Exception e) {
            throw new ApiException(ApiResponseCode.ERROR);
        }
        HashMap<String, String> payload = new HashMap<>();
        payload.put("username", userContext.getUsername());
        try {
            authUtils.createLoginAudit(userContext, request, payload);
        } catch (Exception e) {
            logger.error("====Exception occured in creating audit logs in login ========");
        }
        mapper.writeValue(authUtils.httpResponse(request, response).getWriter(), responseUtil.ok(tokenMap, ApiResponseCode.SUCCESS));
        clearAuthenticationAttributes(request);
    }

    /**
     * This method is used to clear http request sessions.
     * @param request the request during which the authentication attempt occurred..
     */
    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
