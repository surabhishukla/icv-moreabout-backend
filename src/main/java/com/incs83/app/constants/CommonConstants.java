/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.constants;

import java.text.DecimalFormat;

public interface CommonConstants {

    String FORWARD_SLASH = "/";
    DecimalFormat TWO_DECIMAL_PLACE = new DecimalFormat("#.##");
    int ZERO = 0;
    String NOT_A_NUMBER = "NaN";
    String EMPTY_STRING = "";
    String NONE = "NONE";
    String COMMA = ",";
    String UNDERSCORE = "_";
    String TODAY = "Today";
    String LAST_7_DAYS = "Last 7 days";
    String LAST_30_DAYS = "Last 30 days";
    String LAST_90_DAYS = "Last 90 days";
    String DAY = "Day";
    String TIMESTAMP = "timestamp";
    String DATE_CREATED = "dateCreated";
    String CREATED_AT = "createdAt";
    Integer MONGODB_QUERY_LOG_MAX_TIME = 200;
    Long _1_DAY_IN_MILLISECONDS = 86400000L;
    Long _1_HOUR_IN_MILLISECONDS = 3600000L;
    Long _1_MIN_IN_MILLISECONDS = 60000L;
    Long NEXT_WEEK = 604800000L;
    String REPORT_FILE_PATH = "/tmp/";
    String REPORT_SHEET_NAME = "Sheet1";
    String ALL = "ALL";
    String M83_MASTER_DB = "m83_master_db";
    String SPACE = " ";
    String DEFAULT_EMAIL = "admin@iot83.com";
    String X_TENANT_NAME = "X_TENANT_NAME";
    String X_PROJECT_ID = "X-PROJECT-ID";
    String VMQ_HEADER = "vernemq-hook";
    String COLON = ":";
    String LAST_3_DAYS = "Last 3 days";
    String START_TIME_END_TIME = "startTime&EndTime";
    String DATE = "date";
    String SYSTEM_ADMIN = "SYSTEM_ADMIN";
    String SUPER_ADMIN_CONTEXT = "83incs";
    String SYSTEM_ROLE = "SYSTEM_ROLE";
    String ENTERPRISE_ROLE = "ENTERPRISE_ROLE";
    String SYSTEM = "SYSTEM";
    String IGNORE_FINAL = " && !within(is(FinalType))";
    String DEFAULT_TENANT_NAME = "83incs";
    String TICKET_GENERATED = "<b>This is system generated message to inform you that ticket has been created.</b> <br /> <br /> <b>Ticket Titile </b> : ";
    String TICKET_ASSIGNED_BY = "<br /><br /> <b>Ticket assigned By</b> : ";
    String DESCRIPTION = "<br /><br /><b>Ticket description </b>: ";
    String TICKET_TYPE = "<br /><br /><b>Ticket type </b>: ";
    String DO_NOT_REPLY = "<br /><br /><b> Please Do not reply to this message. All comments and replies should be done in the ticket for tracking purposes<br /><br />Thank you <br />Iot83 Team</b>";
    String ANOMALY_GENERATED = "<b>This is system generated message to inform you that an Anomaly has been generated.</b> <br /> <br />~";
    String ANOMALY_DESCRIPTION = "<br /><br /><b>Anomaly Description </b>: ";
    String ANOMALY_DO_NOT_REPLY = "<br /><br /><b> Please do not reply to this message. <br /><br />Thank you <br />Iot83 Team</b>";
    String CREATE_USER_EMAIL_TYPE = "createUserEmail";
    String CREATE_USER_SUBJECT = "User Registration";
    String CHANGE_PASSWORD = "Password changed notification.";
    String RESET_PASSWORD = "Reset password notification.";
    String X_AUTHORIZATION = "X-Authorization";
    String APPLICATION_JSON = "application/json";
    String OW_BASE_API = "/api/v1/";
    String OW_ACTIONS_API = "namespaces/{namespace}/actions/";
    String OW_PACKAGES_API = "namespaces/{namespace}/packages/";
    String OW_NAMESPACE = "{namespace}";
    String FAAS_API_GATEWAY = "api/faas/gateway/";
    String TENANT_TYPE_SAAS = "SAAS";
    String TENANT_TYPE_ENTERPRISE = "ENTERPRISE";
    String PAYMENT_CONFIRMATION_SUBJECT = "Payment Confirmation";
    String DB_CONNECTIONS_CACHE_MAP="DB_CONNECTIONS";
    String COMMON="COMMON";
    interface Template {

        String TICKET_HEADER = "<html>\n" +
                "<body style=\" margin: 50px auto 50px auto;#ccc solid; background: #fff; font-size: 20px; font-family: verdana, sans-serif;\" align=\"center\">\n" +
                " <table style=\"width: 500px; margin: 50px auto 50px auto; border: 1px #ccc solid; background: #fff; font-size: 11px; font-family: verdana, sans-serif;\" align=\"center\">\n" +
                "    <tbody>\n" +
                "      <tr>\n" +
                "        <td colspan=\"2\" style=\"padding: 9px;color: #fff;font-size: 20px; background: #1B3073;\">\n" +
                "<b>Iot83</b>\n" +
                "<br>\n" +
                "</td>\n" +
                " </tbody>\n" +
                " </table>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
    }
    String MQTT_TEMPLATE="MqttCredentials";
    String MQTT_URL = "platform-mqtt.__baseUrl__.com";
}
