/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.incs83.app.config.SNSConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is used for making connection with amazon notification service.
 * <br><br>{@link #getAmazonSNS()} this method is used to make connection with the amazon notification services on the basis of provided access key and secret key.
 */
@Component
public class SNSUtils {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private SNSConfig snsConfig;

    /**
     * This method is used to make connection with the amazon notification services on the basis of provided access key and secret key.
     * @return amazonSNS which of type AmazonSNS.
     */
    public AmazonSNS getAmazonSNS() {
        AmazonSNS amazonSNS = null;
        try {
            amazonSNS = AmazonSNSClientBuilder
                    .standard()
                    .withRegion(snsConfig.getRegion())
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(snsConfig.getAccessKey(), snsConfig.getSecretKey())))
                    .build();

        } catch (Exception e) {
            LOG.error("Unable to establish connection with AWS SMS service - " + e);
        }
        return amazonSNS;
    }
}
