/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * This request class allow users, account admin to login.
 * <br><br>{@link #username} username is the unique id that was provided by user at the time of account creation.
 * <br><br>{@link #isPlatformLogin} it means if singed-in authority wants to login as platform or not
 *  (if yes then signed-in authority is able to access and change the code in M83 platform).
 */
public class LoginRequest implements Serializable {

    private static final long serialVersionUID = 5167493525237622847L;
    private String username;
    private String password;

    @JsonCreator
    public LoginRequest(@JsonProperty("username") String username, @JsonProperty("password") String password, @JsonProperty("isPlatformLogin") Boolean isPlatformLogin) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return Objects.nonNull(username) ? username.trim() : username;
    }

    public String getPassword() {
        return Objects.nonNull(password) ? password : password;
    }
}
