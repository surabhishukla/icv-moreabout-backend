/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
/**
 * @Author Jayant Puri
 * @Created 11-Apr-2017
 */
package com.incs83.app.mt;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used for callback registered with the SessionFactory that is responsible for resolving the current tenant identifier for use with CurrentSessionContext and SessionFactory.getCurrentSession().
 * <br><br>{@link #resolveCurrentTenantIdentifier()} This method is used for resolving the current tenant identifier.
 * <br><br>{@link #validateExistingCurrentSessions()} This method is used to check that Should we validate that the tenant identifier on "current sessions" that already exist when CurrentSessionContext.currentSession() is called matches the value returned here from resolveCurrentTenantIdentifier().
 */
public final class SqlCurrentTenantIdentifier implements CurrentTenantIdentifierResolver {


    public static ThreadLocal<String> _tenantIdentifierSql = new ThreadLocal<>();
    public static ThreadLocal<String> _tenantName = new ThreadLocal<>();
    public static ThreadLocal<Session> _currentSession = new ThreadLocal<>();
    public static ThreadLocal<HttpServletRequest> _servletRequest = new ThreadLocal<>();
    public static ThreadLocal<HttpServletResponse> _servletResponse = new ThreadLocal<>();

    public static ThreadLocal<Transaction> _currentTransaction = new ThreadLocal<>();
    public static ThreadLocal<Boolean> _isTransactionalInit = new ThreadLocal<>();

    public static String DEFAULT_TENANT = "m83_master_db";

    /**
     * This method is used for resolving the current tenant identifier.
     * @return it returns the current tenant id of {String} type.
     */
    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantId = _tenantIdentifierSql.get();
        if (tenantId == null) {
            tenantId = DEFAULT_TENANT;
        }
        return  tenantId;
    }

    /**
     * This method is used to check that Should we validate that the tenant identifier on "current sessions" that already exist when CurrentSessionContext.currentSession() is called matches the value returned here from resolveCurrentTenantIdentifier().
     * @return it returns true indicates that the extra validation will be performed or false indicates it will not.
     */
    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}