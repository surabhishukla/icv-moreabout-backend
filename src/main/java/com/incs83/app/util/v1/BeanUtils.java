/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * This is a utility class for Bean.
 * This class implements ApplicationContextAware interface which allows beans access to the ApplicationContext.
 */
@Service
public class BeanUtils implements ApplicationContextAware {

    private static ApplicationContext context;

    /**
     * This method is used to set the context.
     * @param applicationContext it is the applicationContext request object
     * @throws BeansException this method might throw BeansException
     */
    public static void setContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static <T> T getBean(Class<T> beanClass) {
        return Objects.isNull(context) ? null : context.getBean(beanClass);
    }

    /**
     * This method Set the ApplicationContext that this object runs in..
     * @param applicationContext it is the applicationContext request object
     * @throws BeansException this method might throw BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}