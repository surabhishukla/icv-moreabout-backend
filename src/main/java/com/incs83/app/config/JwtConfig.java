/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * This class is used for JWT token purposes.
 */
@Component
@Configuration
@Import(ApplicationConsulService.class)
public class JwtConfig {
    @Autowired
    private ApplicationConsulService applicationConsulService;

    /**
     * This method is used to get the refreshTokenExpirationTime from application consul server.
     * @return refresh token
     */
    public Integer getRefreshTokenExpTime() {
        return Integer.valueOf(applicationConsulService.getJwtRefreshTokenExpTime());
    }

    /**
     * @return This method is used to return the expiration time of jwt token.
     */
    public Integer getTokenExpirationTime() {
        return Integer.valueOf(applicationConsulService.getJwtTokenExpirationTime());
    }

    /**
     * @return This method is used to return the signing key which will be used to encrypt
     *  or decrypt JWT token.
     */
    public String getTokenSigningKey() {
        return applicationConsulService.getJwtTokenSigningKey();
    }

    /**
     * @return This method is used to return the token issuer from consul server.
     */
    public String getTokenIssuer() {
        return applicationConsulService.getJwtTokenIssuer();
    }
}
