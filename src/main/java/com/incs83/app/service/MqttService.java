/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.incs83.app.config.ApplicationConsulService;
import com.incs83.app.util.v1.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class is used to perform publish operation to the server, which means it is used to send data to the server.
 * <br><br>{@link #publish(String messageString, String topic)} this method  is used to perform publish operation.
 */
@Service
public class MqttService {
    private static final Logger LOG = LogManager.getLogger();
    @Autowired
    private ApplicationConsulService applicationConsulService;

    /**
     * This method  is used to perform publish operation to the server.
     * @param messageString it is the actual message which client want to send to the server.
     * @param topic it refers to a form of addressing that allows MQTT clients to share information.
     */
    public void publish(String messageString, String topic) {
        try {
            String brokerURL = "tcp://" + applicationConsulService.getMqttInternalUrl();
            MqttClient mqttClient = new MqttClient(brokerURL, CommonUtils.generateUUID());
            MqttConnectOptions connectOptions = new MqttConnectOptions();
            connectOptions.setAutomaticReconnect(true);
            connectOptions.setMaxInflight(1000);
            if (applicationConsulService.isMqttAuthEnabled()) {
                connectOptions.setUserName(applicationConsulService.getMqttUsername());
                connectOptions.setPassword(applicationConsulService.getMqttPassword().toCharArray());
            }
            mqttClient.connect(connectOptions);
            if (mqttClient.isConnected()) {
                mqttClient.getTopic(topic);
                MqttMessage message = new MqttMessage();
                message.setPayload(messageString.getBytes());
                mqttClient.publish(topic, message);
                mqttClient.disconnect();
            } else {
                LOG.error("MQTT Connection not established");
            }
        } catch (Exception e) {
            LOG.error("publish error", e.getMessage());
        }
    }
}
