/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.entities.LoginDetails;
import com.incs83.app.entities.User;
import com.incs83.app.mt.DataAccessService;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.security.tokenFactory.RawAccessJwtToken;
import com.incs83.app.security.tokenFactory.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import static com.incs83.app.constants.CommonConstants.DEFAULT_TENANT_NAME;
import static com.incs83.app.constants.CommonConstants.M83_MASTER_DB;

/**
 * This is the Utility class to manage auth related stuff, contains all the utility methods for {@link com.incs83.app.security.auth}
 * <br><br>{@link #httpResponse(HttpServletRequest, HttpServletResponse)} is used to return response headers.
 */

@Component
public class AuthUtils {

    @Autowired
    private ClientInfoUtils clientInfoUtils;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DataAccessService dataAccessService;

    /**
     * This method is used to return the response of Http servlet.
     *
     * @param request  is Http request to be used for the response.
     * @param response is Http response to be delivered.
     * @return response
     */
    public HttpServletResponse httpResponse(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT,PATCH, OPTIONS, DELETE");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, X-Authorization, X-Requested-With, isSecure, apiKey");
        response.setHeader("Access-Control-Expose-Headers", "X-Authorization");
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return response;
    }

    /**
     * It is used to check the role of user matches with system_admin or not.
     * @param user it is the object of User
     * @return true or false
     */
    public boolean isSystemAdmin(User user) {
        return user.getUserRole().iterator().next().getRole().getName().equals("SYSTEM_ADMIN");
    }



    /**
     * This method is used to match saved or updated token from the database while login.
     *
     * @param rawAccessJwtToken is jwt token saved in database.
     * @param userId            is used to set a raw token with a user id for identification.
     * @param tenantName        is used to set a database of a given tenant.
     * @return true or false
     * @throws Exception this method might throw exception
     */
    public boolean matchToken(RawAccessJwtToken rawAccessJwtToken, String userId, String tenantName) throws Exception {
        CommonUtils.setFromDemoTour(false);
        CommonUtils.setFromDynamicApplication(false);
        if (DEFAULT_TENANT_NAME.equals(tenantName)) {
            SqlCurrentTenantIdentifier._tenantIdentifierSql.set(M83_MASTER_DB);
        } else {
            SqlCurrentTenantIdentifier._tenantIdentifierSql.set(ConnectionCache.getDBName(tenantName));
        }
        LoginDetails loginDetails = (LoginDetails) dataAccessService.read(LoginDetails.class, userId);
        if (Objects.isNull(loginDetails))
            return false;
        else
            return rawAccessJwtToken.getToken().equals(loginDetails.getToken());
    }


    /**
     * This method is used to check the expiration of the token.
     * @param tokenExpiryDate it is the expiry date of token
     * @return true or false
     */
    public boolean isTokenExpired(Date tokenExpiryDate) {
        if (tokenExpiryDate.after(Calendar.getInstance().getTime())) {
            return false;
        }
        return true;
    }

}
