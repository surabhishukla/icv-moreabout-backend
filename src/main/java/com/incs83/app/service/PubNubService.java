/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.incs83.app.config.PubNubConfig;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.access_manager.PNAccessManagerGrantResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

/**
 * This class is used to perform operation related to the pubnub like setting up the configuration and publishing the data.
 * <br><br>{@link #getConfigInstance(PubNubConfig)} This method is used to set required configuration for the pubNub.
 * <br><br>{@link #pushNotification(String, HashMap)} This method is used to publish the message on the basis of given channel Name.
 */
@Service
@EnableAsync
public class PubNubService {
    public static final Logger LOG = LogManager.getLogger();

    @Autowired
    private PubNubConfig pubNubConfig;

    private static PNConfiguration pnConfiguration = null;

    /**
     * This method is used to set required configuration for the pubNub.
     * @param pubNubConfig(subscribe,publish,secret)
     *                            subscribe it consist of subscription key for pubNub.
     *                            publish it consist of publish key for pubNub.
     *                            secret it consist of secret key for pubNub.
     * @throws Exception
     * @return pnConfiguration of type {PNConfiguration}
     */
    private static PNConfiguration getConfigInstance(PubNubConfig pubNubConfig) throws Exception {
        pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(pubNubConfig.getSubscribeKey());
        pnConfiguration.setPublishKey(pubNubConfig.getPublishKey());
        pnConfiguration.setSecretKey(pubNubConfig.getSecretKey());
        pnConfiguration.setAuthKey("noWritePermission");
        pnConfiguration.setSecure(true);

        return pnConfiguration;
    }

    /**
     * This method is used to publish the message on the basis of given channel Name.
     * @param channelName it is the name on which the message will be published on pubNub.
     * @param message it is the actual message which is going to be published.
     * @throws Exception this method might throw exception
     */
    @Async
    public void pushNotification(String channelName, HashMap<String, Object> message) throws Exception {
        if (Objects.isNull(pnConfiguration)) {
            try {
                pnConfiguration = getConfigInstance(pubNubConfig);
            } catch (Exception e) {
                LOG.error("==========Error while PubNub Config==========");
            }
        }
        PubNub pubnub = new PubNub(pnConfiguration);
        pubnub.publish().channel(channelName).message(message).async(new PNCallback<PNPublishResult>() {
            @Override
            public void onResponse(PNPublishResult result, PNStatus status) {
                if (!status.isError()) {
                } else {
                    status.retry();
                }
            }
        });

        pubnub.grant()
                .channels(Arrays.asList(channelName))
                .authKeys(Arrays.asList("noWritePermission"))
                .write(false)
                .read(true)
                .ttl(0)
                .manage(true)
                .async(new PNCallback<PNAccessManagerGrantResult>() {
                    @Override
                    public void onResponse(PNAccessManagerGrantResult result, PNStatus status) {
                    }
                });
    }
}
