/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.auth.ajax;

import com.incs83.app.entities.User;
import org.springframework.security.core.GrantedAuthority;

public class UserAuthorization implements GrantedAuthority {

    private static final long serialVersionUID = 3733447884331675194L;

    private User user;

    private String authority;

    public User getUser() {
        return user;
    }

    public UserAuthorization setUser(User user) {
        this.user = user;
        return this;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public UserAuthorization setAuthority(String authority) {
        this.authority = authority;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UserAuthorization))
            return false;

        UserAuthorization ua = (UserAuthorization) obj;
        return ua.getAuthority() == this.getAuthority() || ua.getAuthority().equals(this.getAuthority());
    }
}