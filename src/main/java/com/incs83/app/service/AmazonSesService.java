/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import com.incs83.app.common.EmailRequest;
import com.incs83.app.common.EmailWithAttachmentRequest;
import com.incs83.app.config.ApplicationConsulService;
import com.incs83.app.constants.CommonConstants;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.request.EmailSendRequest;
import com.incs83.app.request.EmailTemplateRequest;
import com.incs83.app.request.TemplateListRequest;
import com.incs83.app.util.v1.SESUtils;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * This is AmazonSesService class which is used for sending email.
 * <br><br>{@link #email(EmailRequest,String)} this method is used to check various fields that whether it is available or not, if everything goes good then it returns to sendEmail method.
 * <br><br>{@link #sendEmail(EmailRequest,String)} this method is used to set the various details and send the email.
 * <br><br>{@link #sendPolicyEmail(String,String,String,String)} this method is used to send policy email.
 * <br><br>{@link #tenantCreationEmail(String,String,String)} This method is used to send email when tenant is created.
 * <br><br>{@link #sendWithAttachment(EmailWithAttachmentRequest)} This method is used to send email with attaching files.
 * <br><br>{@link #createEmailTemplate(EmailTemplateRequest)} This method is used to create template in Amazon Web Service.
 * <br><br>{@link #getTemplateByName(String)} This method is used to fetch the template created in Amazon Web Service on the basis of templateName.
 * <br><br>{@link #getAllTemplates(TemplateListRequest)} This method is used to fetch all the template created in Amazon Web Service.
 * <br><br>{@link #updateTemplate(EmailTemplateRequest)} This method is used to update the template present in Amazon Web Service.
 * <br><br>{@link #deleteTemplate(String)} This method is used to delete the template from Amazon Web Service.
 * <br><br>{@link #sendTemplatedEmail(EmailSendRequest)} This method is used to send email in bulk.
 */
@Service
public class AmazonSesService {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private SESUtils sesUtils;
    @Autowired
    private ApplicationConsulService applicationConsulService;

    /**
     * This method is used to check various fields that whether it is available or not, if everything goes good then it returns to sendEmail method.
     * @param emailRequest(to,message,cc,bcc,subject,ticketStatus,assignee,ticketId,ticketType)
     *                     to it contains the email of receiver.
     *                     message it is the actual message written in the email.
     *                     cc it is the address of receiver whose are just getting the Carbon Copy of mail
     *                     bcc it is the address of receiver whose are just getting the Blind Carbon Copy of mail.
     *                     ticketStatus it consist the status of the ticket
     *                     assignee it tells about the assignee of the ticket.
     *                     ticketId it is the id of the ticket
     * @param type it is of String type
     * @throws Exception this method might throw exception
     * @return sendEmail(emailRequest, type) of SendEmailResult type
     */
    public SendEmailResult email(EmailRequest emailRequest, String type) throws Exception {
        if (ValidationUtils.isNullOrEmpty(emailRequest.getTo())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "To field is Mandatory cannot be null");
        }
        if (ValidationUtils.isNullOrEmpty(emailRequest.getSubject())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Subject field is Mandatory cannot be null");
        }
        if (ValidationUtils.isNullOrEmpty(emailRequest.getMessage())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "message field is Mandatory cannot be null");
        }
        return sendEmail(emailRequest, type);
    }

    /**
     * This method is used to set the various details and send the email.
     * @param emailRequest(to,message,cc,bcc,subject,ticketStatus,assignee,ticketId,ticketType)
     *                     to it contains the email of receiver.
     *                     message it is the actual message written in the email.
     *                     cc it is the address of receiver whose are just getting the Carbon Copy of mail
     *                     bcc it is the address of receiver whose are just getting the Blind Carbon Copy of mail.
     *                     subject it gives the brief idea about the email.
     *                     ticketStatus it consist the status of the ticket
     *                     assignee it tells about the assignee of the ticket.
     *                     ticketId it is the id of the ticket
     * @param type it is of String type
     * @exception Exception
     * @return emailResult of SendEmailResult type
     */
    private SendEmailResult sendEmail(EmailRequest emailRequest, String type) throws Exception {
        Destination destination = new Destination();
        destination.withToAddresses(emailRequest.getTo());
        SendEmailResult emailResult;
        SendEmailRequest request;

        if (ValidationUtils.nonNullOrEmpty(emailRequest.getCc())) {
            destination.withCcAddresses(emailRequest.getCc());
        }
        if (ValidationUtils.nonNullOrEmpty(emailRequest.getBcc())) {
            destination.withBccAddresses(emailRequest.getBcc());
        }

        if (type.equals("anomaly")) {
            request = new SendEmailRequest()
                    .withDestination(destination)
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8").withData(CommonConstants.Template.TICKET_HEADER + CommonConstants.ANOMALY_GENERATED + CommonConstants.ANOMALY_DESCRIPTION + emailRequest.getMessage() + CommonConstants.ANOMALY_DO_NOT_REPLY)))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(emailRequest.getSubject())))
                    .withSource(applicationConsulService.getFromEmail());

            emailResult = sesUtils.getAmazonSimpleEmailService().sendEmail(request);
        } else if (type.equals(CommonConstants.CREATE_USER_EMAIL_TYPE) || type.equals(CommonConstants.CHANGE_PASSWORD)) {
            request = new SendEmailRequest()
                    .withDestination(destination)
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8").withData(emailRequest.getMessage())))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(emailRequest.getSubject())))
                    .withSource(applicationConsulService.getFromEmail());

            emailResult = sesUtils.getAmazonSimpleEmailService().sendEmail(request);
        } else {
            request = new SendEmailRequest()
                    .withDestination(destination)
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8").withData(CommonConstants.Template.TICKET_HEADER + CommonConstants.TICKET_GENERATED + emailRequest.getSubject() + CommonConstants.TICKET_ASSIGNED_BY + emailRequest.getAssignee() + CommonConstants.DESCRIPTION + emailRequest.getMessage() + CommonConstants.TICKET_TYPE + emailRequest.getTicketType() + CommonConstants.DO_NOT_REPLY)))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(emailRequest.getSubject())))
                    .withSource(applicationConsulService.getFromEmail());

            emailResult = sesUtils.getAmazonSimpleEmailService().sendEmail(request);
        }

        return emailResult;
    }

    /**
     * This method is used to send policy email.
     * @param email it is of String type.
     * @param subject it is of String type.
     * @param message it is of String type.
     * @param template it is of String type
     * @throws Exception this method might throw exception
     * @return emailResult of SendEmailResult type
     */
    public SendEmailResult sendPolicyEmail(String email, String subject, String message, String template) throws Exception {
        String defaultTemplate = "<html>\n<body style=\" margin: 50px auto 50px auto;#ccc solid; background: #fff; font-size: 20px; font-family: verdana, sans-serif;\" align=\"center\">\n <table style=\"width: 500px; margin: 50px auto 50px auto; border: 1px #ccc solid; background: #fff; font-size: 11px; font-family: verdana, sans-serif;\" align=\"center\">\n    <tbody>\n      <tr>\n        <td colspan=\"2\" style=\"padding: 9px;color: #fff;font-size: 20px; background: #1B3073;\">\n<b>IoT83</b>\n<br>\n</td>\n </tbody>\n </table>\n\n</body>\n</html><b>This is system generated message to inform you that an Anomaly has been generated.</b> <br /> <br />~<br /><br /><b>Anomaly Description </b>: " + message + "<br /><br /><b> Please do not reply to this message. <br /><br />Thank you <br />IoT83 Team</b>";
        template = ValidationUtils.isNullOrEmpty(template) ? defaultTemplate : template;
        Destination destination = new Destination();
        destination.withToAddresses(email);

        SendEmailResult emailResult;
        SendEmailRequest request;

        request = (new SendEmailRequest()).withDestination(destination).withMessage((new Message()).withBody((new Body()).withHtml((new Content()).withCharset("UTF-8").withData(template))).withSubject((new Content()).withCharset("UTF-8").withData(subject))).withSource(applicationConsulService.getFromEmail());
        emailResult = this.sesUtils.getAmazonSimpleEmailService().sendEmail(request);

        return emailResult;

    }

    /**
     * This method is used to send email when tenant is created.
     * @param email it is of String type.
     * @param subject it is of String type.
     * @param message it is of String type.
     * @throws Exception this method might throw exception
     * @return emailResult of SendEmailResult type
     */
    public SendEmailResult tenantCreationEmail(String email, String subject, String message) throws Exception {
        Destination destination = new Destination();
        destination.withToAddresses(email);
        SendEmailRequest request = (new SendEmailRequest()).withDestination(destination).withMessage((new Message()).withBody((new Body()).withHtml((new Content()).withCharset("UTF-8").withData(message))).withSubject((new Content()).withCharset("utf-8").withData(subject))).withSource(applicationConsulService.getFromEmail());
        SendEmailResult emailResult = this.sesUtils.getAmazonSimpleEmailService().sendEmail(request);

        return emailResult;

    }

    /**
     * This method is used to send email with attaching files.
     * @param request(filePath,recipientEmail,connectorName,html_part,text_part,subject)
     *               filePath it is the path of the file.
     *               recipientEmail it is the email of receiver.
     *               connectorName it is the name of the connector.
     *               html_part it consist of html.
     *               text_part it is the place where actual message will be written.
     *               subject it gives the brief idea about the email.
     * @throws Exception this method might throw exception
     */
    public void sendWithAttachment(EmailWithAttachmentRequest request) throws Exception {
        Session session = Session.getDefaultInstance(new Properties());
        MimeMessage message = new MimeMessage(session);
        message.setSubject(request.getSubject(), "UTF-8");
        message.setFrom(new InternetAddress(applicationConsulService.getFromEmail()));
        message.setRecipients(MimeMessage.RecipientType.TO, request.getRecipientEmail());
        MimeMultipart msg_body = new MimeMultipart("alternative");
        MimeBodyPart wrap = new MimeBodyPart();
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setContent(request.getText_part(), "text/plain; charset=UTF-8");
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(request.getHtml_part(), "text/html; charset=UTF-8");
        msg_body.addBodyPart(textPart);
        msg_body.addBodyPart(htmlPart);
        wrap.setContent(msg_body);
        MimeMultipart msg = new MimeMultipart("mixed");
        message.setContent(msg);
        msg.addBodyPart(wrap);
        MimeBodyPart att = new MimeBodyPart();
        DataSource fds = new FileDataSource(request.getFilePath());
        att.setDataHandler(new DataHandler(fds));
        att.setFileName(fds.getName());
        msg.addBodyPart(att);
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            message.writeTo(outputStream);
            RawMessage rawMessage =
                    new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
            SendRawEmailRequest rawEmailRequest =
                    new SendRawEmailRequest(rawMessage);
            this.sesUtils.getAmazonSimpleEmailService().sendRawEmail(rawEmailRequest);
        } catch (Exception ex) {
            LOG.info(ex.getMessage(), ex);
        }

    }

    /**
     * This method is used to create template in Amazon Web Service.
     * @param emailTemplateRequest(templateName,subjectPart,htmlPart,textPart)
     *                             templateName it is the name of the template which will be used
     *                             subjectPart it is the subject of the email.
     *                             htmlPart it consist of html.
     *                             textPart it is the place where actual message will be written
     * @throws Exception this method might throw exception
     * @return createTemplateResult of type CreateTemplateResult.
     */
    public CreateTemplateResult createEmailTemplate(EmailTemplateRequest emailTemplateRequest) throws Exception {
        CreateTemplateRequest createTemplateRequest = new CreateTemplateRequest();
        Template template;
        CreateTemplateResult createTemplateResult;
        AmazonSimpleEmailService emailService = sesUtils.getAmazonSimpleEmailService();
        template = emailTemplateRequest.mapToTemplate();
        createTemplateRequest.setTemplate(template);
        try {
            createTemplateResult = emailService.createTemplate(createTemplateRequest);
        } catch (AlreadyExistsException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Template already exists");
        } catch (InvalidTemplateException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Invalid template");
        } catch (LimitExceededException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Template limit exceeds");
        } catch (Exception e) {
            LOG.error("Error while creating template " + e);
            throw e;
        }
        return createTemplateResult;
    }

    /**
     * This method is used to fetch the template created in Amazon Web Service on the basis of templateName.
     * @param templateName it is of type  String.
     * @throws Exception this method might throw exception
     * @return getTemplateResult of type GetTemplateResult.
     */
    public GetTemplateResult getTemplateByName(String templateName) throws Exception {
        if (ValidationUtils.isNullOrEmpty(templateName)) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Invalid template name");
        }
        GetTemplateRequest getTemplateRequest = new GetTemplateRequest();
        GetTemplateResult getTemplateResult;
        getTemplateRequest.setTemplateName(templateName);
        AmazonSimpleEmailService emailService = sesUtils.getAmazonSimpleEmailService();
        try {
            getTemplateResult = emailService.getTemplate(getTemplateRequest);
        } catch (TemplateDoesNotExistException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Template does not exists");
        } catch (Exception e) {
            LOG.error("Error while fetching template by name : " + templateName + " " + e);
            throw e;
        }
        return getTemplateResult;
    }

    /**
     * This method is used to fetch all the template created in Amazon Web Service.
     * @param templateListRequest(nextToken,maxItems)
     *                           nextToken it contains the value of next token.
     *                           maxItems it tells about maximum item allowed at a time
     * @throws Exception this method might throw exception
     * @return listTemplatesResult of type ListTemplatesResult.
     */
    public ListTemplatesResult getAllTemplates(TemplateListRequest templateListRequest) throws Exception {
        ListTemplatesResult listTemplatesResult;
        ListTemplatesRequest listTemplatesRequest;
        AmazonSimpleEmailService emailService = sesUtils.getAmazonSimpleEmailService();
        listTemplatesRequest = templateListRequest.mapToListTemplatesRequest();
        try {
            listTemplatesResult = emailService.listTemplates(listTemplatesRequest);
        } catch (Exception e) {
            LOG.error("Error while fetching list of templates");
            throw e;
        }
        return listTemplatesResult;
    }

    /**
     * This method is used to update the template present in Amazon Web Service.
     * @param emailTemplateRequest(templateName,subjectPart,htmlPart,textPart)
     *                             templateName it is the name of the template which will be used
     *                             subjectPart it is the subject of the email.
     *                             htmlPart it consist of html.
     *                             textPart it is the place where actual message will be written
     * @throws Exception this method might throw exception
     * @return updateTemplateResult of type UpdateTemplateResult.
     */
    public UpdateTemplateResult updateTemplate(EmailTemplateRequest emailTemplateRequest) throws Exception {
        Template template;
        UpdateTemplateResult updateTemplateResult;
        UpdateTemplateRequest updateTemplateRequest = new UpdateTemplateRequest();
        template = emailTemplateRequest.mapToTemplate();
        updateTemplateRequest.setTemplate(template);
        AmazonSimpleEmailService emailService = sesUtils.getAmazonSimpleEmailService();
        try {
            updateTemplateResult = emailService.updateTemplate(updateTemplateRequest);
        } catch (TemplateDoesNotExistException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Template does not exists");
        } catch (InvalidTemplateException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Invalid template");
        } catch (Exception e) {
            LOG.error("Error while updating template : " + emailTemplateRequest.getTemplateName() + " " + e);
            throw e;
        }
        return updateTemplateResult;
    }

    /**
     * This method is used to delete the template from Amazon Web Service.
     * @param templateName it is of type  String.
     * @throws  Exception this method might throw exception
     * @return deleteTemplateResult of type DeleteTemplateResult.
     */
    public DeleteTemplateResult deleteTemplate(String templateName) throws Exception {
        if (ValidationUtils.isNullOrEmpty(templateName)) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Invalid template name");
        }
        try {
            getTemplateByName(templateName);
        } catch (TemplateDoesNotExistException e) {
            throw new ValidationException(com.incs83.app.constants.HttpStatus.BAD_REQUEST.value(), "Template does not exists");
        } catch (Exception e) {
            throw e;
        }
        DeleteTemplateResult deleteTemplateResult;
        DeleteTemplateRequest deleteTemplateRequest = new DeleteTemplateRequest();
        deleteTemplateRequest.setTemplateName(templateName);
        AmazonSimpleEmailService emailService = sesUtils.getAmazonSimpleEmailService();
        try {
            deleteTemplateResult = emailService.deleteTemplate(deleteTemplateRequest);
        } catch (Exception e) {
            LOG.error("Error while deleting template : " + templateName + " " + e);
            throw e;
        }
        return deleteTemplateResult;
    }

    /**
     * This method is used to send email in bulk.
     * @param emailSendRequest(source,templateName,destinations,defaultTemplateData)
     *                             source it consist of the sender address
     *                             templateName it is the name of the template which will be used
     *                             destinations it consist of the receiver address
     * @throws Exception this method might throw exception
     * @return sendBulkTemplatedEmailResult of type SendBulkTemplatedEmailResult.
     */
    public SendBulkTemplatedEmailResult sendTemplatedEmail(EmailSendRequest emailSendRequest) throws Exception {
        SendBulkTemplatedEmailResult sendBulkTemplatedEmailResult;
        SendBulkTemplatedEmailRequest sendBulkTemplatedEmailRequest = new SendBulkTemplatedEmailRequest();
        List<BulkEmailDestination> bulkEmailDestinationList = new ArrayList<>();
        AmazonSimpleEmailService emailService = sesUtils.getAmazonSimpleEmailService();
        sendBulkTemplatedEmailRequest.setSource(emailSendRequest.getSource());
        sendBulkTemplatedEmailRequest.setTemplate(emailSendRequest.getTemplateName());
        sendBulkTemplatedEmailRequest.setDefaultTemplateData(emailSendRequest.getDefaultTemplateData());
        emailSendRequest.getDestinations().forEach(destinationData -> {
            Destination destination = new Destination();
            BulkEmailDestination bulkEmailDestination = new BulkEmailDestination();
            destination.setToAddresses(destinationData.getToAddresses());
            destination.setCcAddresses(destinationData.getCcAddresses());
            destination.setBccAddresses(destinationData.getBccAddresses());
            bulkEmailDestination.setDestination(destination);
            bulkEmailDestination.setReplacementTemplateData(destinationData.getReplacementTemplateData());
            bulkEmailDestinationList.add(bulkEmailDestination);
        });
        sendBulkTemplatedEmailRequest.setDestinations(bulkEmailDestinationList);
        try {
            sendBulkTemplatedEmailResult = emailService.sendBulkTemplatedEmail(sendBulkTemplatedEmailRequest);
        } catch (MessageRejectedException e) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Message rejected");
        } catch (MailFromDomainNotVerifiedException e) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Mail domain not verified");
        } catch (TemplateDoesNotExistException e) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Template does not exists");
        } catch (ConfigurationSetDoesNotExistException e) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Configuration set does not exists");
        } catch (ConfigurationSetSendingPausedException e) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Email sending is disabled for the configuration set");
        } catch (Exception e) {
            LOG.error("Error while sending email template : " + emailSendRequest.getTemplateName());
            throw e;
        }
        return sendBulkTemplatedEmailResult;
    }
}



