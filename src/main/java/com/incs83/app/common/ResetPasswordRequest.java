/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.common;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This request class is used when user forgot the password.
 * <br><br>{@link #username} is the unique id of the user.
 * <br><br>{@link #baseUrl} it is required to redirect to another API.
 */
public class ResetPasswordRequest {
    @NotNull
    @NotBlank
    @Size(min = 1, message = "Cannot be empty")
    private String username;

    private String baseUrl;

    public String getUsername() {
        return username == null ? null : username.trim();
    }

    public ResetPasswordRequest setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getBaseUrl() {
        return baseUrl == null ? null : baseUrl.trim();
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
