/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.auth.ajax;

import com.incs83.app.common.LoginRequest;
import com.incs83.app.config.JwtConfig;
import com.incs83.app.constants.CommonConstants;
import com.incs83.app.constants.queries.UserSQL;
import com.incs83.app.entities.*;
import com.incs83.app.enums.Applicable;
import com.incs83.app.handler.exceptions.ApiException;
import com.incs83.app.handler.exceptions.ResourceNotAllowedException;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.mt.DataAccessService;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.security.tokenFactory.UserContext;
import com.incs83.app.util.v1.CommonUtils;
import com.incs83.app.util.v1.ConnectionCache;
import com.incs83.app.util.v1.ValidationUtils;
import com.incs83.app.util.v2.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.incs83.app.constants.CommonConstants.EMPTY_STRING;
import static com.incs83.app.constants.CommonConstants.M83_MASTER_DB;
import static com.incs83.app.constants.queries.AccountSQL.ACCOUNT_BY_TENANT_NAME;

/**
 * This class is used to validate whether the logged in user's credentials are valid or not. It also verifies whether user is authenticated for platform or for application login
 * <br><br>{@link #authenticate(Authentication)} This method is used to perform the authentication.
 * <br><br>{@link #supports(Class)} This method returns true if this AjaxAuthenticationProvider supports the indicated Authentication object.
 */
@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {
    private final BCryptPasswordEncoder encoder;
    private final DataAccessService dataAccessService;
    @Autowired
    private JwtConfig jwtConfig;
    @Autowired
    private AuthUtils authUtils;

    @Autowired
    public AjaxAuthenticationProvider(final DataAccessService dataAccessService, final BCryptPasswordEncoder encoder) {
        this.dataAccessService = dataAccessService;
        this.encoder = encoder;
    }

    /**
     * This method is used to validate whether the logged in user's credentials are valid or not. It also verifies whether user is authenticated for platform or for application login
     * @param authentication it is the authentication request object.
     * @return a fully authenticated object including credentials.
     * @throws AuthenticationException this method might throw AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        Assert.notNull(authentication, "No authentication data provided");
        LoginRequest loginRequest = (LoginRequest) authentication.getCredentials();
        String applicableFor = null;
        if (loginRequest.isPlatformLogin()) {
            applicableFor = Applicable.PLATFORM.get();
        } else {
            applicableFor = Applicable.APPLICATION.get();
        }
        String username = (String) authentication.getPrincipal();
        String password = ((LoginRequest) authentication.getCredentials()).getPassword();
        String query;
        HashMap<String, Object> params = new HashMap<>();
        query = UserSQL.GET_USER_BY_EMAIL;
        params.put("email", username);

        Iterable<ParentEntity> userList;
        User user;
        try {
            userList = dataAccessService.read(User.class, query, params);
        } catch (Exception e) {
            throw new ApiException(ApiResponseCode.ERROR);
        }

        if (!userList.iterator().hasNext()) {
            throw new BadCredentialsException("Authentication Failed. Invalid Username or Password");

        }
        user = (User) userList.iterator().next();
        Role role = user.getUserRole().iterator().next().getRole();
        String access = role.getAccessType();
        if (!access.equalsIgnoreCase(applicableFor) && !access.equalsIgnoreCase(Applicable.BOTH.get())) {
            throw new ResourceNotAllowedException("You are not authorized to login to " + authUtils.getDisplayName(applicableFor));
        }
        if (!encoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Authentication Failed. Invalid Username or Password");
        }
        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + username);
        }

        if (user.getUserRole() == null) throw new InsufficientAuthenticationException("User has no roles assigned");
        String roleId = user.getUserRole().iterator().next().getRole().getId();
        List<String> groupIds = new ArrayList<>();
        groupIds.addAll((Objects.nonNull(user.getUserGroup()) ? (user.getUserGroup().stream().map(ele -> ele.getGroupDetails().getId()).collect(Collectors.toList())) : new ArrayList<>()));
        String name = (user.getFirstName() + CommonConstants.SPACE + user.getLastName()).trim();
        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.put("roleId", roleId);
        List<String> projectIds = new ArrayList<>();

        if (ValidationUtils.nonNullOrEmpty(user.getUserProject())) {
            for (UserProject userProject : user.getUserProject()) {
                if (Objects.nonNull(userProject.getProject().getId())) {
                    projectIds.add(userProject.getProject().getId());
                }
            }
        }

        boolean isLicenseExpired = false;
        String tenantType = EMPTY_STRING;
        if (!authUtils.isSystemAdmin(user)) {
            License license;
            String tenantName = SqlCurrentTenantIdentifier._tenantName.get();
            try {
                license = (License) dataAccessService.read(License.class).iterator().next();
                if (ValidationUtils.nonNullOrEmpty(license.getType())) {
                    if ((((CommonUtils.getCurrentTimeInMillis()) - license.getCreatedAt()) / (24 * 60 * 60 * 1000)) > (license.getNumberOfDays())) {
                        isLicenseExpired = true;
                    }
                }

                if (!tenantName.equals("83incs")) {
                    SqlCurrentTenantIdentifier._tenantIdentifierSql.set(M83_MASTER_DB);
                    HashMap<String, Object> queryParam = new HashMap<>();
                    queryParam.put("tenantName", tenantName);

                    Account account = (Account) dataAccessService.read(Account.class, ACCOUNT_BY_TENANT_NAME, queryParam).iterator().next();
                    if (ValidationUtils.nonNullOrEmpty(account.getType())) {
                        tenantType = account.getType();
                    }
                }
            } catch (Exception e) {
                throw new ApiException(ApiResponseCode.ERROR);
            }
            SqlCurrentTenantIdentifier._tenantIdentifierSql.set(ConnectionCache.getDBName(tenantName));
        }


        UserContext userContext = UserContext.create(user.getId(), user.getUsername(),
                SqlCurrentTenantIdentifier._tenantName.get(), jwtConfig.getTokenIssuer(), roleId, groupIds, name, user.isVerified(), isLicenseExpired, user.isInternalUser(), user.getUserRole().iterator().next().getRole().getName(), null, projectIds, null, user.isDefault(), tenantType);
        return new UsernamePasswordAuthenticationToken(userContext, null);
    }

    /**
     * This method returns true if this AjaxAuthenticationProvider supports the indicated Authentication object.
     * @param authentication it is the Class object.
     * @return true if the implementation can more closely evaluate the Authentication class presented
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
