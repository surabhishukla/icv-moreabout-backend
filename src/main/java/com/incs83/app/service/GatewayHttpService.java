/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.handler.exceptions.ApiException;
import com.incs83.app.handler.exceptions.OWException;
import com.incs83.app.handler.response.ApiResponseCode;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.incs83.app.constants.CommonConstants.EMPTY_STRING;

/**
 * This class helps in performing execution operation for FaaS-ApiGateway.
 * <br><br>{@link #doGet(String, HashMap, HashMap)} this method  is used to query or to get some information from the server.
 * <br><br>{@link #doGet(String, HashMap, HashMap, String, String, boolean)} this method  is also used to query or to get some information from the server.It has some extra parameter such as userName,password etc...
 * <br><br>{@link #doGet(String, HashMap, HashMap, CloseableHttpClient)} this method  is also used to query or to get some information from the server.
 * <br><br>{@link #doPost(StringEntity,String,HashMap)} This method is used to sending information to the server.
 * <br><br>{@link #doPost(HttpEntity, String, HashMap, HashMap, String, String, Boolean)} This method is used to sending information to the server.
 * <br><br>{@link #doDelete(HashMap, String, HashMap)} This method allows the client to remove anything from the server.
 * <br><br>{@link #doPut(StringEntity, String url, HashMap, HashMap, String, String, Boolean)} This method is used to updating anything on the server.
 * <br><br>{@link #doPut(HttpEntity, String url, HashMap, HashMap, String, String, Boolean)} This method is used to updating anything on the server.
 * <br><br>{@link #doDelete(HashMap, String, HashMap, String, String, Boolean)} This method allows the client to remove anything from the server.
 * <br><br>{@link #doDelete(HashMap, String, HashMap, CloseableHttpClient)} This method allows the client to remove anything from the server.
 * <br><br>{@link #doDelete(HashMap, String, HashMap, CloseableHttpClient)} This method allows the client to remove anything from the server.
 * <br><br>{@link #getCloseableHttpClient(String, String, Boolean)} This method is used to provide the CloseableHttpClient object.
 * <br><br>{@link #getSSLContext()} This method acquires the SSLContext object.
 */
@Service
public class GatewayHttpService {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private ObjectMapper mapper;

    /**
     * This method  is used to query or to get some information from the server.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @throws Exception this method might throw exception
     * @return the fetched information as response of type {HashMap}.
     */
    public HashMap<String, Object> doGet(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        return doGet(url, headerInfo, queryParams, httpClient);
    }

    /**
     * This method is used to query or to get some information from the server.It has some extra parameter such as userName,password etc...
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return the fetched information as response of type {HashMap}.
     */
    public HashMap<String, Object> doGet(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, boolean sslEnable) throws Exception {
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        return doGet(url, headerInfo, queryParams, httpClient);
    }

    /**
     * This method is used to query or to get some information from the server.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param httpClient it is used for executing http request and also for closing connection manager associated with it.
     * @throws Exception
     * @return finalResp of type {HashMap}.
     */
    private HashMap<String, Object> doGet(String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, CloseableHttpClient httpClient) {
        String resp;
        HashMap<String, Object> finalResp = new HashMap<>();
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    try {
                        params.append(k).append("=").append(URLEncoder.encode(String.valueOf(v), "UTF-8")).append("&");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                });
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpGet httpGet = new HttpGet(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpGet::addHeader);
            }
            HttpResponse httpResponse = httpClient.execute(httpGet);
            resp = EntityUtils.toString(httpResponse.getEntity());
            finalResp.put("data", mapper.readValue(resp, HashMap.class));
            finalResp.put("httpCode", httpResponse.getStatusLine().getStatusCode());
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doGet Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return finalResp;
    }

    /**
     * This method is used to sending information to the server.
     * @param entity it is of StringEntity type.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return finalResp of type {HashMap}.
     */
    public HashMap<String, Object> doPost(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        String resp;
        HashMap<String, Object> finalResp = new HashMap<>();
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    if (v.getClass().equals(new String[]{}.getClass())) {
                        String[] values = (String[]) v;
                        for (String value : values) {
                            params.append(k).append("=").append(value).append("&");
                        }
                    } else {
                        params.append(k).append("=").append(v).append("&");
                    }
                });
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpPost httpPost = new HttpPost(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPost::addHeader);
            }
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            resp = EntityUtils.toString(httpResponse.getEntity());
            finalResp.put("data", mapper.readValue(resp, HashMap.class));
            finalResp.put("httpCode", httpResponse.getStatusLine().getStatusCode());
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doPost Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return finalResp;
    }

    /**
     * This method is used to sending information to the server.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param params it is of StringEntity type.
     * @throws Exception this method might throw exception
     * @return resp of type {String}.
     */
    public String doPost(StringEntity params, String url, HashMap<String, String> headerInfo) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String resp;
        try {
            HttpPost httpPost = new HttpPost(url);
            headerInfo.forEach(httpPost::addHeader);
            httpPost.setEntity(params);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            resp = EntityUtils.toString(httpResponse.getEntity());
            if (!(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK)) {
                LOG.error("Error ::: Response " + resp);
                throw new ApiException(ApiResponseCode.ERROR);
            }
        } catch (Exception e) {
            throw new ApiException(ApiResponseCode.ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error Closing HTTP HttConnection...");
            }
        }
        return resp;
    }

    /**
     * This method is used to sending information to the server.
     * @param entity it is of HttpEntity type.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return finalResp of type {HashMap}.
     */
    public HashMap<String, Object> doPost(HttpEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        String resp;
        HashMap<String, Object> finalResp = new HashMap<>();
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> {
                    if (v.getClass().equals(new String[]{}.getClass())) {
                        String[] values = (String[]) v;
                        for (String value : values) {
                            params.append(k).append("=").append(value).append("&");
                        }
                    } else {
                        params.append(k).append("=").append(v).append("&");
                    }
                });
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }

            HttpPost httpPost = new HttpPost(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPost::addHeader);
            }
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            resp = EntityUtils.toString(httpResponse.getEntity());
            finalResp.put("data", mapper.readValue(resp, HashMap.class));
            finalResp.put("httpCode", httpResponse.getStatusLine().getStatusCode());
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                String msg = String.valueOf(((Map) finalResp.get("data")).get("error"));
                if (Objects.nonNull(msg) && msg.contains("The request content was malformed")) {
                    msg = "File upload not supported";
                    throw new ApiException(ApiResponseCode.ERROR);
                }
            }
        } /*catch (GenericAuthException ex) {
            throw ex;
        }*/ catch (Exception e) {
            LOG.error("Exception Occurred while invoking doPost Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return finalResp;
    }

    /**
     * This method allows the client to remove anything from the server.
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @throws Exception this method might throw exception
     * @return the data and httpCode as response of type {HashMap}.
     */
    public HashMap<String, Object> doDelete(HashMap<String, Object> queryParams, String url, HashMap<String, String> headerInfo) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        return doDelete(queryParams, url, headerInfo, httpClient);
    }

    /**
     * This method is used to updating anything on the server.
     * @param entity it is of StringEntity type.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return finalResp of type {HashMap}.
     */
    public HashMap<String, Object> doPut(StringEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        String resp;
        HashMap<String, Object> finalResp = new HashMap<>();
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpPut httpPut = new HttpPut(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPut::addHeader);
            }
            httpPut.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPut);
            resp = EntityUtils.toString(httpResponse.getEntity());
            finalResp.put("data", mapper.readValue(resp, HashMap.class));
            finalResp.put("httpCode", httpResponse.getStatusLine().getStatusCode());
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doPut Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return finalResp;
    }

    /**
     * This method is used to updating anything on the server.
     * @param entity it is of HttpEntity type.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return finalResp of type {HashMap}.
     */
    public HashMap<String, Object> doPut(HttpEntity entity, String url, HashMap<String, String> headerInfo, HashMap<String, Object> queryParams, String username, String password, Boolean sslEnable) throws Exception {
        String resp;
        HashMap<String, Object> finalResp = new HashMap<>();
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpPut httpPut = new HttpPut(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpPut::addHeader);
            }
            httpPut.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPut);
            resp = EntityUtils.toString(httpResponse.getEntity());
            finalResp.put("data", mapper.readValue(resp, HashMap.class));
            finalResp.put("httpCode", httpResponse.getStatusLine().getStatusCode());
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                String msg = String.valueOf(((Map) finalResp.get("data")).get("error"));
                if (Objects.nonNull(msg) && msg.contains("The request content was malformed")) {
                    msg = "File upload not supported";
                    throw new ApiException(ApiResponseCode.ERROR);
                }

            }
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doPut Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return finalResp;
    }

    /**
     * This method allows the client to remove anything from the server.
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception this method might throw exception
     * @return the data and httpCode as response of type {HashMap}.
     */
    public HashMap<String, Object> doDelete(HashMap<String, Object> queryParams, String url, HashMap<String, String> headerInfo, String username, String password, Boolean sslEnable) throws Exception {
        CloseableHttpClient httpClient = getCloseableHttpClient(username, password, sslEnable);
        return doDelete(queryParams, url, headerInfo, httpClient);

    }

    /**
     * This method allows the client to remove anything from the server.
     * @param queryParams it consist of information about timeout,result, blocking.
     * @param url it is the url with information like gatewayUrl, customized uri "api/faas/gateway/", userName, and project id of that user.
     * @param headerInfo it consist of information like platform address, connection information of(mongo,mysql,sfp,mqtt,elstic) etc...
     * @param httpClient it is used for executing http request and also for closing connection manager associated with it.
     * @throws Exception
     * @return finalResp of type {HashMap}.
     */
    private HashMap<String, Object> doDelete(HashMap<String, Object> queryParams, String url, HashMap<String, String> headerInfo, CloseableHttpClient httpClient) throws IOException {
        String resp = "{}";
        HashMap<String, Object> finalResp = new HashMap<>();
        try {
            StringBuffer params = new StringBuffer(EMPTY_STRING);
            if (Objects.nonNull(queryParams) && !queryParams.isEmpty()) {
                queryParams.forEach((k, v) -> params.append(k).append("=").append(v).append("&"));
            }
            if (!params.toString().equals(EMPTY_STRING)) {
                String queryParamsFinal = params.toString().substring(0, params.toString().length() - 1);
                url = url + "?" + queryParamsFinal;
            }
            HttpDelete httpDelete = new HttpDelete(url);
            if (Objects.nonNull(headerInfo) && !headerInfo.isEmpty()) {
                headerInfo.forEach(httpDelete::addHeader);
            }
            HttpResponse httpResponse = httpClient.execute(httpDelete);
            if (Objects.nonNull(httpResponse.getEntity())) {
                resp = EntityUtils.toString(httpResponse.getEntity());
            }
            finalResp.put("data", mapper.readValue(resp, HashMap.class));
            finalResp.put("httpCode", httpResponse.getStatusLine().getStatusCode());
        } catch (Exception e) {
            LOG.error("Exception Occurred while invoking doDelete Call...", e);
            throw new OWException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                LOG.error("Error while Closing HTTP Connection...");
            }
        }

        return finalResp;
    }


    /**
     * This method is used to provide the CloseableHttpClient object.
     * @param username it consist of information about the userName.
     * @param password it consist of information about the password.
     * @param sslEnable it is used for enabling security, it's value can either be true or false .
     * @throws Exception
     * @return httpClient of type {CloseableHttpClient}.
     */
    private CloseableHttpClient getCloseableHttpClient(String username, String password, Boolean sslEnable) throws Exception {
        CloseableHttpClient httpClient;
        if (ValidationUtils.nonNullOrEmpty(username) && ValidationUtils.nonNullOrEmpty(password)) {
            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
            provider.setCredentials(AuthScope.ANY, credentials);
            if (!sslEnable) {
                SSLContext sc = getSSLContext();
                httpClient = HttpClientBuilder.create().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).setSSLContext(sc).setDefaultCredentialsProvider(provider).build();
            } else {
                httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
            }

        } else {
            httpClient = HttpClients.createDefault();
        }
        return httpClient;
    }

    /**
     * This method acquires the SSLContext object.
     * @throws Exception
     * @return sc of type {SSLContext}.
     */
    private SSLContext getSSLContext() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        return sc;
    }
}
