/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.business.v1;

/**
 * @author Sahil Sachdeva on 13/12/18 at 3:03 PM
 */

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.incs83.app.config.AWSConfig;
import com.incs83.app.config.S3Config;
import com.incs83.app.constants.ApplicationConstants;
import com.incs83.app.handler.exceptions.ValidationException;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.util.v1.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.incs83.app.constants.ApplicationConstants.S3_DAP_PATH;
import static com.incs83.app.constants.ApplicationConstants.SLASH;

/**
 * This class is used to manage crud for s3 Server.
 * <br><br>{@link #getS3Client()} ()} ()} is setup client for s3 Server.
 * <br><br>{@link #uploadFileByPath(MultipartFile, String, Boolean)}  (MultipartFile, String)} is used to upload file to s3 server.
 * <br><br>{@link #deleteFile(String)} is used to delete file from s3 Server.
 * <br><br>{@link #deleteFolder(String, String)} is used to delete folder from s3 Server.
 * <br><br>{@link #downloadFileFromS3(String, String, String, String, String)} is used to download file from s3 Server.
 * <br><br>{@link #getObjectslistFromFolder(String, String, String, String, String, String)} is used to get file list from s3 Server.
 * <br><br>{@link #getCustomS3Client(String, String, String)} is used to setup custom client for s3 Server.
 * <br><br>{@link #uploadImageByteArray(String, byte[])} is used to upload image in byte.
 */

@Service
public class S3Service {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private S3Config s3Config;

    @Autowired
    private AWSConfig awsConfig;


    /**
     * This method is used to create a client of s3 server.
     *
     * @return s3Client
     */
    @PostConstruct
    public AmazonS3 getS3Client() {
        LOG.debug("####################  " + s3Config.getAccessKey() + "  ######################  " + s3Config.getSecretKey());
        AmazonS3 s3Client = null;
        try {
            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3Config.getAccessKey(), s3Config.getSecretKey()))).withRegion(s3Config.getRegion())
                    .build();
        } catch (Exception e) {
            LOG.error("Unable to establish connection with AWS S3.");
        }

        return s3Client;
    }

    /**
     * This method is used to upload files to cdn/assest/ directory aws s3 server.     *
     *
     * @param file      is a multipart file to be upload.
     * @param fileType  is a directory path in which file to be uploaded.
     * @param isPrivate if false then file will be downloaded easily fro browser, else it needs access keys to download file.
     * @return details for uploading file
     */
    public HashMap<String, String> uploadFile(MultipartFile file, String fileType, Boolean isPrivate) {
        if (Objects.nonNull(fileType)) {
            if (fileType.equalsIgnoreCase("EXCEL") || fileType.equalsIgnoreCase("certificate") || fileType.equalsIgnoreCase("report") || fileType.equalsIgnoreCase("credential")) {
                String tenantName = SqlCurrentTenantIdentifier._tenantName.get();
                String path = tenantName + SLASH + file.getName();
                return uploadFileByPath(file, path, isPrivate);
            }
        }
        return uploadFileByPath(file, ApplicationConstants.S3_CDN_PATH, isPrivate);
    }

    /**
     * This method is used to upload file to s3 server.
     *
     * @param file      is a multipart file to be upload.
     * @param path      is a directory path in which file to be uploaded.
     * @param isPrivate if false then file will be downloaded easily fro browser, else it needs access keys to download file.
     * @return result
     */
    public HashMap<String, String> uploadFileByPath(MultipartFile file, String path, Boolean isPrivate) {
        String pathAndName = path + file.getOriginalFilename();
        if (Objects.isNull(file) || file.isEmpty())
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error while processing");
        String bucket = s3Config.getBucket();
        if (Objects.isNull(bucket) || bucket.isEmpty())
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No bucket found in property");
        AmazonS3 s3Client = getS3Client();
        if (Objects.isNull(s3Client))
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in Establishing connection with AWS S3.");
        HashMap<String, String> result = new HashMap<>();
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.getSize());
            metadata.setContentType(file.getContentType());
            PutObjectRequest request = new PutObjectRequest(bucket, pathAndName, file.getInputStream(), metadata);
            if (isPrivate) {
                request.withCannedAcl(CannedAccessControlList.Private);
            } else {
                request.withCannedAcl(CannedAccessControlList.PublicRead);
            }
            s3Client.putObject(request);
        } catch (Exception ex) {
            LOG.error("Error while uploadFile.", ex);
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error while uploadFile.");
        }
        result.put("secure_url", s3Client.getUrl(bucket, pathAndName).toExternalForm());
        return result;
    }

    /**
     * This method uploads file to dap/raw/ path to keep the file Private.
     *
     * @param file      The file to be uploaded.
     * @param isPrivate if false then file will be downloaded easily fro browser
     *                  else it needs access keys to download file.
     * @return the URL of uploaded file.
     */
    public HashMap<String, String> uploadDataAggPipelineFile(MultipartFile file, Boolean isPrivate) {
        return uploadFileByPath(file, S3_DAP_PATH, isPrivate);
    }

    /**
     * This method is use to delete file from s3 Server.
     *
     * @param fileUrl is the path in which the file is stored.
     */
    public void deleteFile(String fileUrl) {
        if (!ValidationUtils.isNullOrEmpty(fileUrl)) {
            String bucket = s3Config.getBucket();
            if (Objects.isNull(bucket) || bucket.isEmpty()) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No bucket found in property");
            }
            AmazonS3 s3Client = getS3Client();
            if (Objects.isNull(s3Client)) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in Establishing connection with AWS S3.");
            }
            HashMap<String, String> result = new HashMap<>();
            try {
                ObjectMetadata metadata = new ObjectMetadata();

                DeleteObjectRequest dor = new DeleteObjectRequest(bucket, fileUrl);
                s3Client.deleteObject(dor);

            } catch (Exception ex) {
                LOG.error("Error while Deleting file.", ex);
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error while Deleting file.");
            }
        }
    }

    /**
     * This method is used to delete a complete folder saved in s3 Server.
     *
     * @param folderPath is path of folder.
     * @param bucketName is name of bucket in which folder is saved.
     */
    public void deleteFolder(String folderPath, String bucketName) {
        if (!ValidationUtils.isNullOrEmpty(folderPath)) {
            String bucket = bucketName;
            if (Objects.isNull(bucket) || bucket.isEmpty()) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No bucket found in property");
            }
            AmazonS3 s3Client = getS3Client();
            if (Objects.isNull(s3Client)) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in Establishing connection with AWS S3.");
            }
            ObjectListing objects = s3Client.listObjects(bucket, folderPath);
            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
                try {
                    s3Client.deleteObject(bucket, objectSummary.getKey());
                } catch (AmazonServiceException e) {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Exception");
                }
            }
            DeleteObjectRequest dor = new DeleteObjectRequest(bucket, folderPath);
            s3Client.deleteObject(dor);
        }
    }

    /**
     * This method is used to upload an image into a byte.
     *
     * @param pathAndName is the path in which the file is stored.
     * @param blob        is the length of the file specified.
     * @return url
     */
    public String uploadImageByteArray(String pathAndName, byte[] blob) {
        String bucket = this.s3Config.getBucket();
        if (!Objects.isNull(bucket) && !bucket.isEmpty()) {
            AmazonS3 s3Client = this.getS3Client();
            if (Objects.isNull(s3Client)) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in Establishing connection with AWS S3.");
            } else {
                if (Objects.nonNull(blob) && blob.length > 0) {
                    try {
//                        MinIOService.setMetadata(pathAndName, blob, bucket, s3Client);
                    } catch (Exception var8) {
                        LOG.error("Error while uploading image to S3 :: ", var8);
                        return null;
                    }
                }

                return s3Client.getUrl(bucket, pathAndName).toExternalForm();
            }
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No bucket found in property");
        }
    }

    /**
     * This method is used to set up the custom client of s3 server.
     */
    private AmazonS3 getCustomS3Client(String accessKey, String secretKey, String region) {
        AmazonS3 s3Client = null;
        try {
            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey))).withRegion(region)
                    .build();
        } catch (Exception e) {
            LOG.error("Unable to establish connection with AWS S3.");
        }
        return s3Client;
    }

    /**
     * This method is used to download files from the s3 Server.
     *
     * @param path      is a location of file.
     * @param bucket    is bucket name in which file is saved.
     * @param accessKey is a unique key which is a part of server client credential.
     * @param secretKey is a also unique key which is a part of server client credential.
     * @param region    is an area of a server that we are using as our storage.
     * @return setDownloadPath
     * @throws IOException this method might throw IOException
     */
    public String downloadFileFromS3(String path, String bucket, String accessKey, String secretKey, String region) throws IOException {
        AmazonS3 s3Client = null;
        s3Client = getCustomS3Client(accessKey, secretKey, region);
        //TODO:REFACTOR
        return null;
//        return setDownloadPath(path, bucket, s3Client);
    }

    /**
     * This method is used to get a list of files saved in a specified path.
     *
     * @param bucketName is a bucket name in which the file is saved.
     * @param folderKey  is the path in which file is stored.
     * @param bucket     is the folder on s3 where file is saved.
     * @param accessKey  is a unique key which is a part of server client credential.
     * @param secretKey  is a also unique key which is a part of server client credential.
     * @param region     is an area of a server that we are using as our storage.
     * @return keys
     * @throws Exception if any error occurs while execution of this method.
     */
    public List<String> getObjectslistFromFolder(String bucketName, String folderKey, String bucket, String accessKey, String secretKey, String region) throws Exception {
        AmazonS3 s3Client = null;
        s3Client = getCustomS3Client(accessKey, secretKey, region);
        ListObjectsRequest listObjectsRequest =
                new ListObjectsRequest()
                        .withBucketName(bucketName)
                        .withPrefix(folderKey + "/");

        List<String> keys = new ArrayList<>();

        ObjectListing objects = s3Client.listObjects(listObjectsRequest);
        for (; ; ) {
            List<S3ObjectSummary> summaries = objects.getObjectSummaries();
            if (summaries.size() < 1) {
                break;
            }
            summaries.forEach(s -> keys.add(s.getKey()));
            objects = s3Client.listNextBatchOfObjects(objects);
        }
        s3Client.shutdown();
        return keys;
    }

    /**
     * This method is used to get the size of file whish is going to be uploaded on s3.
     *
     * @param bucketName is the folder on s3 where file is saved.
     * @param accessKey  is a unique key which is a part of server client credential.
     * @param secretKey  is a also unique key which is a part of server client credential.
     * @param region     is an area of a server that we are using as our storage.
     * @param filePath   is the pah where file is uploaded on s3.
     * @return size of the file.
     * @throws Exception if any error occurs while execution of this method.
     */
    public Long sizeOfFile(String bucketName, String accessKey, String secretKey, String region, String filePath) throws Exception {
        AmazonS3 s3Client = null;
        s3Client = this.getCustomS3Client(accessKey, secretKey, region);
        GetObjectMetadataRequest metadataRequest = new GetObjectMetadataRequest(bucketName, filePath);
        final ObjectMetadata objectMetadata = s3Client.getObjectMetadata(metadataRequest);
        Long contentLength = objectMetadata.getContentLength();
        return contentLength;
    }
}

