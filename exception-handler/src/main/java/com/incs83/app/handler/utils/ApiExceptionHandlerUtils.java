/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.handler.utils;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * This class is to add some extra information in Logs when the exception will occur.
 * The extra information is added with the help a context called mapped diagnostic context.
 * <br><br>{@link #mdcLog(HttpServletRequest)} This method is used for inserting the additional information to the log by using the HttpServletRequest object in the ThreadContext.
 */
@Component
public class ApiExceptionHandlerUtils {

    /**
     * This method is used for inserting the additional information to the log by using the HttpServletRequest object in the ThreadContext.
     * @param httpServletRequest it is class type  object which consist of some data to be saved in database.
     *                           url it gives the url of request.
     *                           host it returns the fully qualified name of the client that sent the request.
     *                           query it is the query string that is contained in the request URL after the path.
     *                           user-agent it's for getting the address of the most recent client or proxy that sent the request.
     *                           method it returns the name of the HTTP method with which this request was made.
     * @throws Exception if any error occurs while execution of this method.
     */
    public static void mdcLog(HttpServletRequest httpServletRequest) throws Exception {
        String  queryString = "";
        if(Objects.nonNull(httpServletRequest.getQueryString()) && !httpServletRequest.getQueryString().isEmpty())
            queryString = httpServletRequest.getQueryString();
        ThreadContext.put("url",httpServletRequest.getRequestURL().toString());
        ThreadContext.put("host", httpServletRequest.getRemoteHost());
        ThreadContext.put("query", queryString);
        ThreadContext.put("user-agent", httpServletRequest.getHeader("User-Agent"));
        ThreadContext.put("method", httpServletRequest.getMethod());
    }
}
