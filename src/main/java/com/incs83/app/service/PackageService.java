/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.config.FaasConfig;
import com.incs83.app.mt.SqlCurrentTenantIdentifier;
import com.incs83.app.util.v1.CommonUtils;
import org.apache.http.entity.StringEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

import static com.incs83.app.constants.CommonConstants.*;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

/**
 * This class is used to perform operation like delete,update etc... for package.
 * <br><br>{@link #deletePackage(String packageName)} this method  is used to delete the package.
 * <br><br>{@link #createorUpdatePackage(String namespace, String packageName, String overwrite)} this method  is used to create or update the package.
 *
 */
@Service
public class PackageService {
    private static final Logger LOG = LogManager.getLogger();
    @Autowired
    private FaasConfig faasConfig;
    @Autowired
    private HttpServiceImpl2 httpService;

    /**
     * This method  is used to delete the package.
     * @param packageName it consist of project id.
     * @return response.
     * @throws Exception this method might throw exception
     */
    public HashMap<String, Object> deletePackage(String packageName) throws Exception {
        HashMap<String, Object> response = new HashMap<>();
        try {
            String respString = httpService.doDelete(null, faasConfig.getUrl() + OW_BASE_API + OW_PACKAGES_API.replace(OW_NAMESPACE, SqlCurrentTenantIdentifier._tenantName.get().toLowerCase()) + packageName,
                    null, CommonUtils.getUserName(), CommonUtils.getPassword(), false);
            response = new ObjectMapper().readValue(respString, HashMap.class);
        } catch (Exception e) {
            LOG.error("Error while invoking openWhisk api..", e);
        }
        return response;
    }

    /**
     * This method is used to create or update the package.
     * @param namespace it consist of tenantName.
     * @param packageName it consist of project id.
     * @param overwrite it consist of either true or false.
     * @return response.
     * @throws Exception this method might throw exception
     */
    public HashMap<String, Object> createorUpdatePackage(String namespace, String packageName, String overwrite) throws Exception {
        HashMap<String, Object> response;
        try {
            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put("overwrite", overwrite);
            HashMap<String, String> headerInfo = new HashMap<>();
            headerInfo.put(CONTENT_TYPE, APPLICATION_JSON);
            HashMap<String, Object> payload = new HashMap<>();
            payload.put("name", packageName);
            payload.put("namespace", namespace);
            String reqStr = new ObjectMapper().writeValueAsString(payload);
            StringEntity entity = new StringEntity(reqStr);

            String respString = httpService.doPut(entity, faasConfig.getUrl() + OW_BASE_API + OW_PACKAGES_API.replace(OW_NAMESPACE, namespace) + packageName,
                    headerInfo, queryParams, CommonUtils.getUserName(), CommonUtils.getPassword(), false);
            response = new ObjectMapper().readValue(respString, HashMap.class);
        } catch (Exception e) {
            LOG.error("Error while invoking openWhisk api...", e);
            throw e;
        }
        return response;
    }
}
