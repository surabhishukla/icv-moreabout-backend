/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incs83.app.handler.exceptions.AuthMethodNotSupportedException;
import com.incs83.app.handler.exceptions.InvalidJwtToken;
import com.incs83.app.handler.exceptions.RecordNotFoundException;
import com.incs83.app.handler.exceptions.ResourceNotAllowedException;
import com.incs83.app.handler.response.ResponseUtil;
import com.incs83.app.security.exceptions.AccountSuspendedException;
import com.incs83.app.security.exceptions.JwtExpiredTokenException;
import com.incs83.app.security.exceptions.LicenseExpiredException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * This class is used to handles the request when ever authentication attempt is failed.
 * <br><br>{@link #onAuthenticationFailure(HttpServletRequest,HttpServletResponse,AuthenticationException)} This method is called when an authentication attempt fails.
 */
@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private static final Logger LOG = LogManager.getLogger();
    private final ObjectMapper mapper;
    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    public AjaxAwareAuthenticationFailureHandler(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * This method is called when an authentication attempt fails.
     * @param request the request during which the authentication attempt occurred..
     * @param response it is the response.
     * @param e the exception which was thrown to reject the authentication request.
     * @throws IOException ServletException
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException e) throws IOException, ServletException {

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        if (e instanceof BadCredentialsException) {
            LOG.error("####Authentication Failed -- ", e);
            if (Objects.nonNull(e.getMessage()) && e.getMessage().startsWith("Authentication Failed. Invalid ") || e.getMessage().startsWith("Authentication Failed. Incorrect ")) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
            } else {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), "Sorry, something went wrong. We're working on it and get it fixed as soon as we can"));
            }
        } else if (e instanceof JwtExpiredTokenException) {
            LOG.error("####Token Expired -- ", e);
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), "Token Expired"));
        } else if (e instanceof AuthMethodNotSupportedException) {
            LOG.error("####Auth Method Not Supported Exception -- ", e);
            response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.METHOD_NOT_ALLOWED.value(), "Authentication method not Supported"));
        } else if (e instanceof com.incs83.app.handler.exceptions.InvalidContextException) {
            LOG.error("####Invalid Tenant name in url -- ", e);
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), "Context or Token is Missing"));
        } else if (e instanceof RecordNotFoundException) {
            LOG.error("####Invalid Entry -- ", e);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.NOT_FOUND.value(), "Record not found or invalid entry"));
        } else if (e instanceof UsernameNotFoundException) {
            LOG.error("####Invalid Entry -- ", e);
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), "Authentication Failed, Invalid Username"));
        } else if (e instanceof LicenseExpiredException) {
            LOG.error("####License Expired --- ", e);
            response.setStatus(HttpStatus.PAYMENT_REQUIRED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.PAYMENT_REQUIRED.value(), "License Expired"));
        } else if (e instanceof InvalidJwtToken && e.getMessage().equals("Please Provide Project ID")) {
            LOG.error("#### Project ID is NULL --- ", e);
            response.setStatus(HttpStatus.FORBIDDEN.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.FORBIDDEN.value(), e.getMessage()));
        } else if (e instanceof InvalidJwtToken) {
            LOG.error("#### Invalid Token --- ", e);
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
        } else if (e instanceof AuthenticationServiceException) {
            LOG.error("#### Invalid Inputs --- ", e);
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
        } else if (e instanceof ResourceNotAllowedException) {
            LOG.error("#### Not Authorized  --- ", e);
            response.setStatus(HttpStatus.FORBIDDEN.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.FORBIDDEN.value(), e.getMessage()));
        } else if ((e instanceof AccountSuspendedException)) {
            LOG.error("#######This account has been suspended#######", e);
            response.setStatus(HttpStatus.PAYMENT_REQUIRED.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.PAYMENT_REQUIRED.value(), "This account has been suspended"));
        } else {
            LOG.error("####Internal Server Error -- ", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            mapper.writeValue(response.getWriter(), responseUtil.exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Sorry, something went wrong. We're working on it and get it fixed as soon as we can"));
        }
    }
}
