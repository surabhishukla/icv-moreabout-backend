/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.util.v1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * This is a utility class for ClientInfo.
 * This class is used for showing various information related to the client.
 * <br><br>{@link #printClientInfo(HttpServletRequest)} it is used to print the various client information.
 * <br><br>{@link #getReferer(HttpServletRequest)} it is used to get the value of referer.
 * <br><br>{@link #getFullURL(HttpServletRequest)} it is used to get the value of Full URL.
 * <br><br>{@link #getClientIpAddr(HttpServletRequest)} it is used to get the value of IP Address.
 * <br><br>{@link #getClientOS(HttpServletRequest)} it is used to get the value of Operating System.
 * <br><br>{@link #getClientBrowser(HttpServletRequest)} it is used to get the value of Browser Name.
 * <br><br>{@link #getUserAgent(HttpServletRequest)} it is used to get the value of User Agent.
 */
@Component
public class ClientInfoUtils {
    private static final Logger LOG = LogManager.getLogger("org");

    /**
     * This method is used to print the various client information.
     *
     * @param request it is used to set the value of referer.
     *                it is used to set the value of Full URL.
     *                it is used to set the value of IP Address.
     *                it is used to set the value of Operating System.
     *                it is used to set the value of Browser Name.
     *                it is used to set the value of User Agent.
     */
    public void printClientInfo(HttpServletRequest request) {
        final String referer = getReferer(request);
        final String fullURL = getFullURL(request);
        final String clientIpAddr = getClientIpAddr(request);
        final String clientOS = getClientOS(request);
        final String clientBrowser = getClientBrowser(request);
        final String userAgent = getUserAgent(request);

        LOG.info("\n" +
                "User Agent \t" + userAgent + "\n" +
                "Operating System\t" + clientOS + "\n" +
                "Browser Name\t" + clientBrowser + "\n" +
                "IP Address\t" + clientIpAddr + "\n" +
                "Full URL\t" + fullURL + "\n" +
                "Referrer\t" + referer);
    }

    /**
     * This method is used to get the value of referer.
     *
     * @param request it is used to get the value of referer.
     * @return referer of String type
     */
    public String getReferer(HttpServletRequest request) {
        final String referer = request.getHeader("referer");
        return referer;
    }

    /**
     * This method is used to get the value of Full URL.
     *
     * @param request it is used to get the value of Full URL.
     * @return result of String type
     */
    public String getFullURL(HttpServletRequest request) {
        final StringBuffer requestURL = request.getRequestURL();
        final String queryString = request.getQueryString();

        final String result = queryString == null ? requestURL.toString() : requestURL.append('?')
                .append(queryString)
                .toString();

        return result;
    }

    /**
     * This method is used to get the value of IP Address.
     *
     * @param request it is used to get the value of IP Address.
     * @return ip of String type
     */
    public String getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip != null && !ip.isEmpty() && ip.contains(","))
            ip = ip.split(",")[0];
        return ip;
    }

    /**
     * This method is used to get the value of MAC Address.
     *
     * @param request it is used to get the value of MAC Address.
     * @return macAddress of String type
     */
    public String getClientMACAddress(HttpServletRequest request) {
        String macAddress = request.getHeader("X-Forwarded-MAC");
        return macAddress;
    }

    /**
     * This method is used to get the value of Operating System.
     *
     * @param request it is used to get the value of Operating System.
     * @return one of these("Windows" or "Mac" or "Unix" or "Android" or "IPhone" or "UnKnown, More-Info: ") which is of String type
     */
    public String getClientOS(HttpServletRequest request) {
        final String browserDetails = request.getHeader("User-Agent");

        //=================OS=======================
        final String lowerCaseBrowser = browserDetails.toLowerCase();
        if (lowerCaseBrowser.contains("windows")) {
            return "Windows";
        } else if (lowerCaseBrowser.contains("mac")) {
            return "Mac";
        } else if (lowerCaseBrowser.contains("x11")) {
            return "Unix";
        } else if (lowerCaseBrowser.contains("android")) {
            return "Android";
        } else if (lowerCaseBrowser.contains("iphone")) {
            return "IPhone";
        } else {
            return "UnKnown, More-Info: " + browserDetails;
        }
    }

    /**
     * This method is used to get the value of Browser Name.
     *
     * @param request it is used to get the value of Browser Name.
     * @return one of the multiple browsers which of String type
     */
    public String getClientBrowser(HttpServletRequest request) {
        final String browserDetails = request.getHeader("User-Agent");
        final String user = browserDetails.toLowerCase();

        String browser = "";

        //===============Browser===========================
        if (user.contains("msie")) {
            String substring = browserDetails.substring(browserDetails.indexOf("MSIE")).split(";")[0];
            browser = substring.split(" ")[0].replace("MSIE", "IE") + "-" + substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version")) {
            browser = (browserDetails.substring(browserDetails.indexOf("Safari")).split(" ")[0]).split(
                    "/")[0] + "-" + (browserDetails.substring(
                    browserDetails.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if (user.contains("opr") || user.contains("opera")) {
            if (user.contains("opera"))
                browser = (browserDetails.substring(browserDetails.indexOf("Opera")).split(" ")[0]).split(
                        "/")[0] + "-" + (browserDetails.substring(
                        browserDetails.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if (user.contains("opr"))
                browser = ((browserDetails.substring(browserDetails.indexOf("OPR")).split(" ")[0]).replace("/",
                        "-")).replace(
                        "OPR", "Opera");
        } else if (user.contains("chrome")) {
            browser = (browserDetails.substring(browserDetails.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1) || (user.indexOf(
                "mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf(
                "mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1)) {
            browser = "Netscape-?";

        } else if (user.contains("firefox")) {
            browser = (browserDetails.substring(browserDetails.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if (user.contains("rv")) {
            browser = "IE";
        } else {
            browser = "UnKnown, More-Info: " + browserDetails;
        }

        return browser;
    }

    /**
     * This method is used to get the value of User-Agent.
     *
     * @param request it is used to get the value of User-Agent.
     * @return header of String type
     */
    public String getUserAgent(HttpServletRequest request) {
        return request.getHeader("User-Agent");
    }

}