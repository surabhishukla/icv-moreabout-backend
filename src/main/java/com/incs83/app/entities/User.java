/* ***********************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

package com.incs83.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.incs83.app.security.auth.ajax.UserAuthorization;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity(name = "User")
@Table(name = "user")
public class User extends ParentEntity implements UserDetails, Serializable {

    private static final long serialVersionUID = -5723258342757016392L;

    @Id
    private String id;

    @Column(name = "password")
    @JsonIgnore
    private String password;

    @Column(name = "first_name", nullable = false)
    private String firstName = "";

    @Column(name = "last_name", nullable = false)
    private String lastName = "";


    @Column(name = "email", nullable = false)
    private String email = "";

    @Column(name = "company", nullable = false)
    private String company = "";

    @Column(name = "title", nullable = false)
    private String title = "";

    @Column(name = "mobile")
    private String mobile = "";


    @Column(name = "is_account_non_expired", columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    private boolean isAccountNonExpired;

    @Column(name = "is_account_non_locked", columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    private boolean isAccountNonLocked;

    @Column(name = "is_cred_non_expired", columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    private boolean isCredNonExpired;

    @Column(name = "is_enabled", columnDefinition = "tinyint(4) default '1'")
    @JsonIgnore
    private boolean isEnabled;

    @Column(name = "image_url")
    private String imageUrl;


    @JoinColumn(name = "user_id")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserRole> userRole;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    @JsonIgnore
    public Set<UserAuthorization> getAuthorities() {
        Set<UserAuthorization> authorities = new HashSet<>();
        Iterator<UserRole> roleList = this.getUserRole().iterator();
        while (roleList.hasNext()) {
            authorities.add(new UserAuthorization().setUser(this).setAuthority(roleList.next().getRole().getName()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        isAccountNonExpired = accountNonExpired;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return isCredNonExpired;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobile() {
        return mobile;
    }

    public User setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    @JsonIgnore
    public boolean isCredNonExpired() {
        return isCredNonExpired;
    }

    public void setCredNonExpired(boolean credNonExpired) {
        isCredNonExpired = credNonExpired;
    }

    public Set<UserRole> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", company='" + company + '\'' +
                ", title='" + title + '\'' +
                ", mobile='" + mobile + '\'' +
                ", isAccountNonExpired=" + isAccountNonExpired +
                ", isAccountNonLocked=" + isAccountNonLocked +
                ", isCredNonExpired=" + isCredNonExpired +
                ", isEnabled=" + isEnabled +
                ", userRole=" + userRole +
                '}';
    }
}
