/* **********************************************************************
 * 83incs CONFIDENTIAL
 **********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licensees (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licensees (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
package com.incs83.app.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 *  This class is used to set value of properties required for Mysql from consul server.
 *  This class consist of getter and setter methods.
 * <br><br>{@link #setHibernateConnectionUrl(String)} This method is used to set the value of hibernateConnectionUrl.
 * <br><br>{@link #setHibernateMtUrl(String)} This method is used to set the value of hibernateMtUrl.
 * <br><br>{@link #setHibernateConnectionUsername(String)} This method is used to set the value of hibernateConnectionUsername.
 * <br><br>{@link #setHibernateConnectionPassword(String)} This method is used to set the value of hibernateConnectionPassword.
 * <br><br>{@link #setHibernateHbm2ddlAuto(String)} This method is used to set the value of hibernateHbm2ddlAuto.
 * <br><br>{@link #setHibernateConnectionDriverClass(String)} This method is used to set the value of hibernateConnectionDriverClass.
 * <br><br>{@link #setHibernateDialect(String)} This method is used to set the value of hibernateDialect.
 * <br><br>{@link #setHibernateShowSql(String)} This method is used to set the value of hibernateShowSql.
 * <br><br>{@link #setHibernateFormatSql(String)} This method is used to set the value of hibernateFormatSql.
 * <br><br>{@link #setHibernateCreateMode(String)} This method is used to set the value of hibernateCreateMode.
 * <br><br>{@link #setHibernateMultiTenancy(String)} This method is used to set the value of hibernateMultiTenancy.
 * <br><br>{@link #setHibernateMtConnectionProvider(String)} This method is used to set the value of hibernateMtConnectionProvider.
 * <br><br>{@link #setHibernateMtTenantIdentifier(String)} This method is used to set the value of hibernateMtTenantIdentifier.
 * <br><br>{@link #setHibernateConnectionPoolClass(String)} This method is used to set the value of hibernateConnectionPoolClass.
 * <br><br>{@link #setHibernatePoolMinConnections(String)} This method is used to set the value of hibernatePoolMinConnections.
 * <br><br>{@link #setHibernatePoolMaxConnections(String)} This method is used to set the value of hibernatePoolMaxConnections.
 * <br><br>{@link #setHibernateGenerateMLTables(String)} This method is used to set the value of hibernateGenerateMLTables.
 * <br><br>{@link #setHibernatePackagesScan(String)} This method is used to set the value of hibernatePackagesScan.
 * <br><br>{@link #setHibernateAllowMerge(String)} This method is used to set the value of hibernateAllowMerge.
 */
@RefreshScope
@Configuration
@ConfigurationProperties
public class MySqlConsulService {
    @Value("${hibernate.connection.url}")
    private String hibernateConnectionUrl;
    @Value("${hibernate.mt_url}")
    private String hibernateMtUrl;
    @Value("${hibernate.connection.username}")
    private String hibernateConnectionUsername;
    @Value("${hibernate.connection.password}")
    private String hibernateConnectionPassword;
    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernateHbm2ddlAuto;
    @Value("${hibernate.connection.driver_class}")
    private String hibernateConnectionDriverClass;
    @Value("${hibernate.dialect}")
    private String hibernateDialect;
    @Value("${hibernate.hibernateShowSql}")
    private String hibernateShowSql;
    @Value("${hibernate.hibernateFormatSql}")
    private String hibernateFormatSql;
    @Value("${hibernate.hibernateCreateMode}")
    private String hibernateCreateMode;
    @Value("${hibernate.multiTenancy}")
    private String hibernateMultiTenancy;
    @Value("${hibernate.mtConnectionProvider}")
    private String hibernateMtConnectionProvider;
    @Value("${hibernate.mtTenantIdentifier}")
    private String hibernateMtTenantIdentifier;
    @Value("${hibernate.connectionPoolClass}")
    private String hibernateConnectionPoolClass;
    @Value("${hibernate.poolMinConnections}")
    private String hibernatePoolMinConnections;
    @Value("${hibernate.poolMaxConnections}")
    private String hibernatePoolMaxConnections;
    @Value("${hibernate.poolTimeout}")
    private String hibernatePoolTimeout;
    @Value("${hibernate.poolMaxStatements}")
    private String hibernatePoolMaxStatements;
    @Value("${hibernate.generateMLTables}")
    private String hibernateGenerateMLTables;

    public MySqlConsulService() {
    }

    @Value("${hibernate.packages.scan}")
    private String hibernatePackagesScan;
    @Value("${hibernate.allowMerge}")
    private String hibernateAllowMerge;

    public String getHibernateConnectionUrl() {
        return hibernateConnectionUrl;
    }

    /**
     * @param hibernateConnectionUrl it contains information such as where to search for the database, the name of the database to connect etc....
     */
    public void setHibernateConnectionUrl(String hibernateConnectionUrl) {
        this.hibernateConnectionUrl = hibernateConnectionUrl;
    }

    public String getHibernateMtUrl() {
        return hibernateMtUrl;
    }

    /**
     * @param hibernateMtUrl it contains information such as where to search for the database, the name of the database to connect etc....
     */
    public void setHibernateMtUrl(String hibernateMtUrl) {
        this.hibernateMtUrl = hibernateMtUrl;
    }

    public String getHibernateConnectionUsername() {
        return hibernateConnectionUsername;
    }

    /**
     * @param hibernateConnectionUsername it represents the database username.
     */
    public void setHibernateConnectionUsername(String hibernateConnectionUsername) {
        this.hibernateConnectionUsername = hibernateConnectionUsername;
    }

    public String getHibernateConnectionPassword() {
        return hibernateConnectionPassword;
    }

    /**
     * @param hibernateConnectionPassword it represents the database password.
     */
    public void setHibernateConnectionPassword(String hibernateConnectionPassword) {
        this.hibernateConnectionPassword = hibernateConnectionPassword;
    }

    public String getHibernateHbm2ddlAuto() {
        return hibernateHbm2ddlAuto;
    }

    /**
     * @param hibernateHbm2ddlAuto it automatically validates and exports DDL to the schema when the sessionFactory is created.
     */
    public void setHibernateHbm2ddlAuto(String hibernateHbm2ddlAuto) {
        this.hibernateHbm2ddlAuto = hibernateHbm2ddlAuto;
    }

    public String getHibernateConnectionDriverClass() {
        return hibernateConnectionDriverClass;
    }

    /**
     * @param hibernateConnectionDriverClass it represents the JDBC driver class.
     */
    public void setHibernateConnectionDriverClass(String hibernateConnectionDriverClass) {
        this.hibernateConnectionDriverClass = hibernateConnectionDriverClass;
    }

    public String getHibernateDialect() {
        return hibernateDialect;
    }

    /**
     * @param hibernateDialect it is used for connecting hibernate application with the database, which  requires to provide the configuration of SQL dialect.
     */
    public void setHibernateDialect(String hibernateDialect) {
        this.hibernateDialect = hibernateDialect;
    }

    public String getHibernateShowSql() {
        return hibernateShowSql;
    }

    /**
     * @param hibernateShowSql it is used to enable the logging of all the generated SQL statements to the console.
     */
    public void setHibernateShowSql(String hibernateShowSql) {
        this.hibernateShowSql = hibernateShowSql;
    }

    public String getHibernateFormatSql() {
        return hibernateFormatSql;
    }

    /**
     * @param hibernateFormatSql it is used to Format the generated SQL statement to make it more readable.
     */
    public void setHibernateFormatSql(String hibernateFormatSql) {
        this.hibernateFormatSql = hibernateFormatSql;
    }

    public String getHibernateCreateMode() {
        return hibernateCreateMode;
    }

    /**
     * @param hibernateCreateMode it is used to tell that which method is going to be used for performing operation on database such as CREATE,VALIDATE,UPDATE,CREATE-DROP.
     */
    public void setHibernateCreateMode(String hibernateCreateMode) {
        this.hibernateCreateMode = hibernateCreateMode;
    }

    public String getHibernateMultiTenancy() {
        return hibernateMultiTenancy;
    }

    /**
     * @param hibernateMultiTenancy it allows multiple tenants use a single resource or, a single database instance.
     */
    public void setHibernateMultiTenancy(String hibernateMultiTenancy) {
        this.hibernateMultiTenancy = hibernateMultiTenancy;
    }

    public String getHibernateMtConnectionProvider() {
        return hibernateMtConnectionProvider;
    }

    /**
     * @param hibernateMtConnectionProvider it tells that each database schema will have its own connectionProvider instance.
     */
    public void setHibernateMtConnectionProvider(String hibernateMtConnectionProvider) {
        this.hibernateMtConnectionProvider = hibernateMtConnectionProvider;
    }

    public String getHibernateMtTenantIdentifier() {
        return hibernateMtTenantIdentifier;
    }

    /**
     * @param hibernateMtTenantIdentifier it is used for identifying the tenant.
     */
    public void setHibernateMtTenantIdentifier(String hibernateMtTenantIdentifier) {
        this.hibernateMtTenantIdentifier = hibernateMtTenantIdentifier;
    }

    public String getHibernateConnectionPoolClass() {
        return hibernateConnectionPoolClass;
    }

    /**
     * @param hibernateConnectionPoolClass it is used to prevent from creating a connection each time when our application interact with database
     */
    public void setHibernateConnectionPoolClass(String hibernateConnectionPoolClass) {
        this.hibernateConnectionPoolClass = hibernateConnectionPoolClass;
    }

    public String getHibernatePoolMinConnections() {
        return hibernatePoolMinConnections;
    }

    /**
     * @param hibernatePoolMinConnections it tells about minimum number of pool connection to carried out.
     */
    public void setHibernatePoolMinConnections(String hibernatePoolMinConnections) {
        this.hibernatePoolMinConnections = hibernatePoolMinConnections;
    }

    public String getHibernatePoolMaxConnections() {
        return hibernatePoolMaxConnections;
    }

    /**
     * @param hibernatePoolMaxConnections it tells about minimum number of pool connection to carried out.
     */
    public void setHibernatePoolMaxConnections(String hibernatePoolMaxConnections) {
        this.hibernatePoolMaxConnections = hibernatePoolMaxConnections;
    }

    public String getHibernatePoolTimeout() {
        return hibernatePoolTimeout;
    }

    /**
     * @param hibernatePoolTimeout it tells about maximum number of pool connection to carried out.
     */
    public void setHibernatePoolTimeout(String hibernatePoolTimeout) {
        this.hibernatePoolTimeout = hibernatePoolTimeout;
    }

    public String getHibernatePoolMaxStatements() {
        return hibernatePoolMaxStatements;
    }

    /**
     * @param hibernatePoolMaxStatements it tells about maximum number of pool connection to carried out.
     */
    public void setHibernatePoolMaxStatements(String hibernatePoolMaxStatements) {
        this.hibernatePoolMaxStatements = hibernatePoolMaxStatements;
    }

    public String getHibernateGenerateMLTables() {
        return hibernateGenerateMLTables;
    }

    /**
     * @param hibernateGenerateMLTables it is used to generate table in database.
     */
    public void setHibernateGenerateMLTables(String hibernateGenerateMLTables) {
        this.hibernateGenerateMLTables = hibernateGenerateMLTables;
    }

    public String getHibernatePackagesScan() {
        return hibernatePackagesScan;
    }

    /**
     * @param hibernatePackagesScan it is used for defining the package to be scanned by hibernate.
     */
    public void setHibernatePackagesScan(String hibernatePackagesScan) {
        this.hibernatePackagesScan = hibernatePackagesScan;
    }

    public String getHibernateAllowMerge() {
        return hibernateAllowMerge;
    }

    /**
     * @param hibernateAllowMerge it offers a considerable flexibility to save the entities in a database as we don't need to attach the object to the session.
     */
    public void setHibernateAllowMerge(String hibernateAllowMerge) {
        this.hibernateAllowMerge = hibernateAllowMerge;
    }

}
